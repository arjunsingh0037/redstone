<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * GotoMeeting local lib functions
 *
 * @package    mod_gotomeeting
 * @copyright  2016 shambhu384@gmail.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/filelib.php');

require_once('autoload.php');

use GoToMeeting\Services\Meeting\MeetingService;
use GoToMeeting\Services\Webinar\RegistrantsService;
use GoToMeeting\Services\Webinar\CoOrganizerService;
use GoToMeeting\Services\Webinar\WebinarService;
use GoToMeeting\Services\Webinar\SessionService;
use GoToMeeting\Services\ServiceFactory;
use GoToMeeting\Services\Training\TrainingService;

use GoToMeeting\Models\Meeting;
use GoToMeeting\Models\Webinar;
use GoToMeeting\Models\Registrant;
use GoToMeeting\Models\Training;
use GoToMeeting\Models\CoOrganizers;

use GoToMeeting\Exception\AccessDeniedException;

use GuzzleHttp\Exception\ClientException;
/**
 * Abstract class form mod_service
 */
abstract class mod_service
{
    /**
     * For convenience we store the course object here as it is needed in other parts of code
     *
     * @var stdClass
     */
    protected $course;
    
    /**
     * For convenience we store the {meeting, webinar, training} object
     * @var stdClass
     */
    protected $instance;
    
    /**
     * For convenience we store the mod goto_meeting object
     * @var stdClass
     */
    protected $service;
    
    /**
     * For convenience we store the course_module object here as it is needed in other parts of code
     *
     * @var stdClass 
     */
    protected $cm;
    
    /**
     * implement to perform CRUD operation
     */
    abstract public function create($data);
    abstract public function update($data);
    abstract public function delete($data);

    /**
     * Implement to perform html view 
     */
    abstract public function render();
    
    /**
     * Default constructor
     */
    public function __construct($instance = null, $service = null, $course = null, $cm = null)
    {
        $this->instance = $instance;
        $this->service = $service;
        $this->course = $course;
        $this->cm = $cm;
    }

    /**
     * Find service status 
     * 
     * @return string
     */
    protected function service_status()
    {
        $service = ucfirst($this->instance->servicetype);
        
        // Service start time is grater then current time : not started
        if(time() <  $this->service->starttime) {
            $time1 = new DateTime(date("y-m-d H:m:s", $this->service->starttime));
            $time2 = new DateTime();
            $time2->setTimestamp(time());
            $interval =  $time2->diff($time1);
            $timediff =  $interval->format("%H hours %i minutes %s seconds");
            return html_writer::tag('p', get_string('notstarted', 'mod_gotomeeting',
                array('service'=>$service, 'time'=>$timediff)), ['class' => 'alert alert-success']); 
        }
        
        // Service end time  if less then current time : finish
        if(time() > $this->service->endtime) {
            return html_writer::tag('p', get_string('finished', 'mod_gotomeeting', $service), ['class' => 'alert alert-warning']); 
        }
        

        $insession = false;
        // Service start time & endtime is in between current time : check started or not from sercer
        if(time() > $this->service->starttime && time() < $this->service->endtime) {
            switch($service) {
                case 'Meeting':
                    $meetingservice = ServiceFactory::getInstance(MeetingService::class);
                    $meeting = $meetingservice->getMeeting($this->service->uniquemeetingid);
                    if($meeting->getStatus() == 'ACTIVE') {
                        $insession = true;    
                    }
                break;
                case  'Webinar':
                    $webinarservice = ServiceFactory::getInstance(WebinarService::class);
                    $webinarservice->getWebinar($this->service->webinarkey);
                    if($meeting->isSession()) {
                        $insession = true;
                    }
                break;
                case 'Training':
                    $trainingservice = ServiceFactory::getInstance(TrainingReportService::class);
                    $sessions = $trainingservice->getSessionsByTraining($this->service->trainingkey);
                    if(count($sessions) > 0) {
                        $insession = true;
                    }
                break;            
            }
        }
        if($insession) {
            $content = html_writer::tag('p',  get_string('insession', 'mod_gotomeeting', $service),['class' => 'alert alert-success']); 
        } else {
            $content = html_writer::tag('p',  get_string('yetnotstarted', 'mod_gotomeeting', $service), ['class' => 'alert alert-warning']);
        }
        return $content;
    }

    /**
     * Convert time with diff timezone
     *
     * @param string $time datetime
     * @param string $totimezone
     * @param string $fromtimezone 
     * @param string $format
     *
     * @return string
     */
    public function converToTz($time = "",$totimezone = '',$fromtimezone ='', $format = "Y-m-d\TH:i:s\Z")
    {
        $date = new DateTime($time, new DateTimeZone($fromtimezone));
        $date->setTimezone(new DateTimeZone($totimezone));
        return $date->format($format);
    }

    /**
     * Service duration
     *
     * @return string
     */
    public function duration()
    {
        $time1 = new DateTime(date("y-m-d H:m:s", $this->service->starttime)); // string date
        $time2 = new DateTime();
        $time2->setTimestamp($this->service->endtime);
        $interval =  $time2->diff($time1);
        return $interval->format("%H hr %i mins");
    }

    function timezone_list() 
    {
        static $timezones = null;

        if ($timezones === null) {
            $timezones = [];
            $offsets = [];
            $now = new DateTime();

            foreach (DateTimeZone::listIdentifiers() as $timezone) {
                $now->setTimezone(new DateTimeZone($timezone));
                $offsets[] = $offset = $now->getOffset();
                $timezones[$timezone] = $this->format_timezone_name($timezone).' (' . $this->format_GMT_offset($offset) . ') ';
            }

            array_multisort($offsets, $timezones);
        }
        return $timezones;
    }

    public function format_GMT_offset($offset) {
        $hours = intval($offset / 3600);
        $minutes = abs(intval($offset % 3600 / 60));
        return 'GMT' . ($offset ? sprintf('%+03d:%02d', $hours, $minutes) : '');
    }

    public function format_timezone_name($name) {
        $name = str_replace('/', ', ', $name);
        $name = str_replace('_', ' ', $name);
        $name = str_replace('St ', 'St. ', $name);
        return $name;
    }

    public function add_calender_event($meetingevent,$instanceevent) {
        global $USER,$CFG;
        require_once($CFG->dirroot."/calendar/lib.php");
        //print_r($meetingevent);
        $event = array();
        $event['userid'] = $USER->id;
        $event['name'] = $meetingevent->name;
        $event['description'] = $meetingevent->intro;
        $event['modulename'] = 'gotomeeting';
        $event['instance'] = $instanceevent;
        $event['subscriptionid'] = null;
        $event['uuid']= '';
        $event['format'] = 1;
        $event['timestart'] = $meetingevent->starttime;
        $event['repeats'] = 0;

        $eventobj = new calendar_event($event);
        $var = $eventobj->create($event);
    }
    public function update_calender_event($meetingevent) {
        global $USER,$CFG,$DB;
        require_once($CFG->dirroot."/calendar/lib.php");
        $eventid = $DB->get_record('event',array('instance'=>$meetingevent->instance));
        //print_r($meetingevent);
        $updateevent = array();
        $updateevent['id'] = $eventid->id;
        $updateevent['userid'] = $USER->id;
        $updateevent['name'] = $meetingevent->name;
        $updateevent['description'] = $meetingevent->intro;
        $updateevent['modulename'] = 'gotomeeting';
        $updateevent['subscriptionid'] = null;
        $updateevent['uuid']= '';
        $updateevent['format'] = 1;
        $updateevent['timestart'] = $meetingevent->starttime;
        $updateevent['repeats'] = 0;

        $var = $DB->update_record('event', $updateevent);
    }
}

/**
 * Meeting mod service
 * 
 * @desc provide features to curd operations on Meeting. 
 */
class meeting_mod_service  extends mod_service
{
    protected $table = 'meeting';
   
    /**
     * Create meeting in GotoMeeting server
     *
     * @param array
     * @return int newly created module id
     */
    public function create($data)
    {
        global $DB;
        $data->timecreated = time();
        $data->timemodified = $data->timecreated;
        $timezones = DateTimeZone::listIdentifiers();
        if(isset($timezones[$data->timezone])){
            $timezone = $timezones[$data->timezone];
        }

        // Make Api call to create meeting in GoToMeeting
        $meeting = new Meeting();
        $meeting->setConferenceCallInfo('text');
        $meeting->setCreateTime(new DateTime(date('Y-m-d\ H:i:s.u',$data->starttime)));
        $meeting->setStartTime(new DateTime(date('Y-m-d\ H:i:s.u',$data->starttime)));
        $meeting->setEndTime(new DateTime(date('Y-m-d\ H:i:s.u',$data->starttime)));
        $meeting->setMaxParticipants($data->maxparticipants);
        $meeting->setMeetingType($data->meetingtype);
        $meeting->setSubject($data->name);
        $meeting->setDuration($data->duration);
        $meeting->setDate(new DateTime(date('Y-m-d\ H:i:s.u',$data->starttime)));
        //print_object($meeting);
        // Get instance of MeetingService throught ServiceFactory
        $service = ServiceFactory::getInstance(MeetingService::class);
        // Make api call to create meeting
        $result = $service->createMeeting($meeting);
        //Insert into database
        $returnid = $DB->insert_record('gotomeeting', $data);
        $timezones = DateTimeZone::listIdentifiers();
        if(isset($timezones[$data->timezone])){
            $timezone = $timezones[$data->timezone];
        }

        //Add calendar event for this meeting  by Arjun Singh
        $cevent = $this->add_calender_event($data,$returnid,$result);
        //print_object($cevent);
        $insert = (object) array(
                                'instance' => $returnid,
                                'uniquemeetingid' => $result->getUniqueMeetingId(),
                                'meetingid' => $result->getMeetingId(),
                                'createtime' => time(),
                                'status' => 'INACTIVE',
                                'subject' => $result->getSubject(),
                                'endtime' => $result->getEndTime()->getTimestamp(),
                                'starttime' => $result->getStartTime()->getTimestamp(),
                                'timezone' => $timezone,
                                'conferencecallinfo' => '',
                                'passwordrequired' => false,
                                'meetingtype' => $result->getMeetingType(),
                                'maxparticipants' => $data->maxparticipants,
                                'numberofattendees' => '',
                                'organizerkey' => '',
                                'meetinginstancekey' => '',
                                'duration' => $result->getDuration(),
                                'date' => time(),
                                'numberofattendees' => $result->getNumberOfAttendees(),
                                'maxparticipants' => $result->getMaxParticipants(),
                                'conferencecallinfo' => $result->getConferenceCallInfo(),
                                'joinurl' => $result->getJoinUrl(),
                            );
        // insert service servicetype table
        $DB->insert_record('goto_meeting',$insert);
        return $returnid;  
    }

    /**
     * Update meeting in GotoMeeting server
     *
     * @param array
     * @return boolean
     */
    public function update($data)
    {
        global $DB;
        $data->timemodified = time();

        // Make Api call to create meeting in GoToMeeting
        $meeting = new Meeting();
        $meeting->setConferenceCallInfo('text');
        $meeting->setCreateTime(new DateTime(date('Y-m-d\ H:i:s.u',$data->starttime)));
        $meeting->setStartTime(new DateTime(date('Y-m-d\ H:i:s.u',$data->starttime)));
        $meeting->setEndTime(new DateTime(date('Y-m-d\ H:i:s.u',$data->starttime)));
        $meeting->setMaxParticipants($data->maxparticipants);
        $meeting->setMeetingType($data->meetingtype);
        $meeting->setSubject($data->name);
        $meeting->setDuration($data->duration);
        $meeting->setDate(new DateTime(date('Y-m-d\ H:i:s.u',$data->starttime)));
        
        // Get instance of MeetingService throught ServiceFactory & update meeting
        $service = ServiceFactory::getInstance(MeetingService::class);        
        $gotomeeting = $DB->get_record('goto_meeting',array('instance' => $data->instance), 'id,uniqueMeetingId,meetingId');
        $service->updateMeeting($gotomeeting->uniquemeetingid, $meeting);
        
        $timezones = DateTimeZone::listIdentifiers();
        if(isset($timezones[$data->timezone])){
            $timezone = $timezones[$data->timezone];
        }
 
        //Insert into database
        $data->id = $data->instance;
        $return = $DB->update_record('gotomeeting', $data);

        //Add calendar event for this meeting  by Arjun Singh
        $cevent = $this->update_calender_event($data);

        $update = (object) array(
                                'id'=> $gotomeeting->id,
                                'subject' => $meeting->getSubject(),
                                'starttime' => $meeting->getStartTime()->getTimestamp(),
                                'endtime' => $meeting->getEndTime()->getTimestamp(),
                                'timezone' => $timezone,
                                'conferencecallinfo' => '',
                                'passwordequired' => false,
                                'meetingtype' => $meeting->getMeetingType(),
                                'numberofattendees' => '',
                                'organizerkey' => '',
                                'meetinginstancekey' => '',
                                'duration' => $meeting->getDuration(),
                                'date' => time(),
                                'numberofattendees' => $meeting->getNumberOfAttendees(),
                                'maxparticipants' => $meeting->getMaxParticipants()
                            );
        // insert service servicetype table
        $DB->update_record('goto_meeting',$update);
        return $return;  
    }

    /**
     * Remove meeting from GotoMeeting server
     *
     * @param array
     */
    public function delete($id)
    {
        global $DB;
        $DB->delete_records('gotomeeting', array('id' => $id));
        $service = ServiceFactory::getInstance(MeetingService::class);        
        $meeting = $DB->get_record('goto_meeting',array('instance' => $id));
        $service->deleteMeeting($meeting->uniquemeetingid); 
        $DB->delete_records('goto_meeting', array('id' => $meeting->id));
        return true;
    }

    /**
     * Generate view for Meeting
     */
    public function render()
    {
        global $OUTPUT, $CFG,$DB, $USER;
        $head = html_writer::tag('h4', $this->service->subject.' <small>('.ucfirst($this->instance->servicetype).')</small>');
        //$head .= html_writer::tag('p', $this->service->description);
        $isteacher = has_capability('moodle/course:update', context_course::instance($this->course->id));
        $timezones = $this->timezone_list();
        if($isteacher) {

            // Render page for teacher
            $menu = '<ul class="nav nav-tabs">
                      <li class="active"><a data-toggle="tab" href="#home">'.get_string('home', 'mod_gotomeeting').'</a></li>
                      <li><a data-toggle="tab" href="#menu1">'.get_string('attendees', 'mod_gotomeeting').'</a></li>
                    </ul>';

            $service = ServiceFactory::getInstance(MeetingService::class);
            $meetingurl = $service->startMeeting($this->service->uniquemeetingid);
            $service = null;
            // default div
            $content = $head;
            $content .= html_writer::tag('p', userdate($this->service->starttime, '%A, %B %d %Y'));
            $content .= html_writer::tag('p',userdate($this->service->starttime,'%I:%M %p').' | '.$timezones[$this->service->timezone].'| '.$this->duration());
            $content .= '<br/>';
            $content .= $this->service_status();
            $content .= html_writer::tag('h4', get_string('joinmeeting', 'mod_gotomeeting'));
            //$content .= $OUTPUT->single_button($meetingurl, get_string('startasmoderator', 'mod_gotomeeting'));
            $content .= '<a id="bstart" href="#ifrm" onClick="showform()">
                            Start meeting as Moderator
                        </a>
                        <a id="bclose" style="float:right;display:none;position: relative;top: 17px;left: -3px;" href="#" onclick="closeWin()">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </a>';
            $content .="<div id='showframe'></div>";
            echo    "<script>
                    function showform(){
                        $('#bclose').show();
                        var ifrm = document.createElement('iframe');
                        ifrm.setAttribute('id', 'ifrm');
                        ifrm.setAttribute('width', '100%');
                        ifrm.setAttribute('height', '600px');
                        ifrm.setAttribute('style', 'padding:10px');
                        var el = document.getElementById('showframe');
                        el.parentNode.insertBefore(ifrm, el);
                        ifrm.setAttribute('src', '".$meetingurl."');
                    }

                    function closeWin()   
                    {
                        $('#bclose').hide();
                        var someIframe = window.parent.document.getElementById('ifrm');
                        someIframe.parentNode.removeChild(window.parent.document.getElementById('ifrm'));
                    }
                    </script>";

            $contents = html_writer::div($content, 'tab-pane fade in active', ['id' => 'home']);
            
            // Attendees
            $content = '';
            $content .= html_writer::tag('button',get_string('attendees', 'mod_gotomeeting'), 
            ['onclick'=>'return getMeetingAttendees("'.$this->service->uniquemeetingid.'")']); 
            $content .= html_writer::div('', '', ['id' => 'meetingattendees']);
            $contents .= html_writer::div($content, 'tab-pane fade ', ['id' => 'menu1']);

            $o = $menu.html_writer::div($contents, 'tab-content');     
        } else {
            $content = $this->service_status();
            $btn = '';

            $content = html_writer::tag('p', userdate($this->service->starttime, '%A, %B %d %Y'));
            $content .= html_writer::tag('p',userdate($this->service->starttime, '%I:%M %p').' | '.$timezones[$this->service->timezone].' | '.$this->duration());
            
            $content .= '<br/>';
            $content .= $this->service_status();
            $content .= html_writer::tag('h4', get_string('joinmeeting', 'mod_gotomeeting'));

            //$btn = $OUTPUT->single_button($this->service->joinurl, 'Join meeting as Participants'); 
            //$btn = $OUTPUT->single_button('test.php?url='.$this->service->joinurl); 
            //$btn = '<a href="test.php?url='.$this->service->joinurl.'">submit</a>';
            //$btn = "<button onclick='showform()'>submit</button>";
            $btn =  '<input name="submit" id="toiframe" type ="button" value ="Join as a participant" onClick="showform()" tabindex="10" />
                    <a id="bclose" style="float:right" href="#" onclick="closeWin()"><i class="fa fa-times" aria-hidden="true"></i></a>';
            $o = $head.$content.$btn;
            $o .="<div id='showframe'></div>";
            echo "<script>
            function showform(){
                var ifrm = document.createElement('iframe');
                ifrm.setAttribute('id', 'ifrm');
                ifrm.setAttribute('width', '100%');
                ifrm.setAttribute('height', '500px');
                var el = document.getElementById('showframe');
                el.parentNode.insertBefore(ifrm, el);
                ifrm.setAttribute('src', '".$this->service->joinurl."');
            }
            function closeWin()   
            {
                var someIframe = window.parent.document.getElementById('ifrm');
                someIframe.parentNode.removeChild(window.parent.document.getElementById('ifrm'));
            }
            
            </script>";
        }
        
        return $o;
    }
}

/**
 * Webinar mod service
 */
class webinar_mod_service extends mod_service {

    
    /**
     * Create webinar in GotoWebianr server
     *
     * @param array
     * @return int newly created module id
     */
    public function create($data)
    {
        global $DB;
        $data->timecreated = time();
        $data->timemodified = $data->timecreated;
        $timezones = DateTimeZone::listIdentifiers();
        $timezone = '';
        if(isset($timezones[$data->timezone])){
            $timezone = $timezones[$data->timezone];
        }
        // Make Api call to create meeting in GoToMeeting
        $webinar = new Webinar();
        $webinar->setSubject($data->name);
        $webinar->setDescription(strip_tags($data->intro));
        $webinar->setTimes(array(
            'startTime' => $this->converToTz(date('Y-m-d h:i:s', $data->starttime),'UTC',$timezone),
            'endTime' => $this->converToTz(date('Y-m-d h:i:s', $data->endtime),'UTC',$timezone)
            ));
        $webinar->setTimeZone($timezone);
        $service = ServiceFactory::getInstance(WebinarService::class);
        $response = $service->createWebinar($webinar);

        // insert database
        try {
            $transaction = $DB->start_delegated_transaction();
            $returnid = $DB->insert_record('gotomeeting', $data);
            $insert = new stdClass();
            $insert->instance = $returnid;
            $insert->subject = $data->name;
            $insert->description = $data->intro;
            $insert->starttime = $data->starttime;
            $insert->endtime = $data->endtime;
            $insert->insession = 0;
            $insert->organizerkey = $service->getClient()->getAuth()->getOrganizerKey();

            $insert->webinarkey = $response["webinarKey"];
            $insert->timezone = $timezone;
            $insert->registrationurl = '';
            $insert->webinarid = '';
            $insert->timecreated = time();
            $insert->timemodified ='';
            $DB->insert_record('goto_webinar', $insert);
            $transaction->allow_commit();
        } catch (moodle_exception $e) {
            $transaction->rollback($e);
        }
        return $returnid;
    }
    

    /**
     * Update webinar in GotoWebinar server
     *
     * @param array
     * @return boolean
     */
    public function update($data)
    {
        global $DB;
        $data->id = $data->instance;
        $data->timemodified = time();
        $timezones = DateTimeZone::listIdentifiers();
        $timezone = '';
        if(isset($timezones[$data->timezone])){
            $timezone = $timezones[$data->timezone];
        }
        $webinartbl = $DB->get_record('goto_webinar', array('instance' => $data->instance), 'id, webinarKey');
        
        // Make Api call to create meeting in GoToMeeting
        $webinar = new Webinar();
        $webinar->setSubject($data->name);
        $webinar->setDescription(strip_tags($data->intro));
        $webinar->setTimes(array(
            'startTime' => $this->converToTz(date('Y-m-d h:i:s', $data->starttime),'UTC',$timezone),
            'endTime' => $this->converToTz(date('Y-m-d h:i:s', $data->endtime),'UTC',$timezone)
            ));
        $webinar->setTimeZone($timezone);
        $service = ServiceFactory::getInstance(WebinarService::class);
        $service->updateWebinar($webinartbl->webinarkey, $webinar);

        //Update database info
        try {
            $transaction = $DB->start_delegated_transaction();
            // Update module table
            $module = new stdClass();
            $module->id = $data->instance;
            $module->name = $data->name;
            $module->intro = $data->intro;
            $result = $DB->update_record('gotomeeting', $module);

            // Update webinar table
            $webinartbl->instance = $data->id;
            $webinartbl->subject = $data->name;
            $webinartbl->description = $data->intro;
            $webinartbl->starttime = $data->starttime;
            $webinartbl->endtime = $data->endtime;
            $webinartbl->timezone = $timezone;
            $webinartbl->timemodified = time();
            $DB->update_record('goto_webinar', $webinartbl);
            $transaction->allow_commit();
        } catch (moodle_exception $e) {
            $transaction->rollback($e);
        }
        return $result;
    }

    public function delete($id)
    {
        global $DB;
        $DB->delete_records('gotomeeting', array('id' => $id));
        $webinar = $DB->get_record('goto_webinar', array('instance' => $id));
        $service = ServiceFactory::getInstance(WebinarService::class);
        $service->cancelWebinar($webinar->webinarkey);
        $DB->delete_records('goto_webinar', array('id' => $webinar->id));
        return true;
    }
     /**
     * Generate view for Webinar
     */
    public function render()
    {
        global $OUTPUT, $CFG, $DB, $USER;
        //$img = html_writer::img('pix/icon.png', 'alt', array('style' => 'margin:3px;'));
        $head = html_writer::tag('h4', $this->service->subject.' <small>('.ucfirst($this->instance->servicetype).')</small>');
        //$head .= html_writer::tag('p', $this->service->description);

        $timezones = $this->timezone_list();
        $isteacher = has_capability('moodle/course:update', context_course::instance($this->course->id));
        if($isteacher) {

            // Render page for teacher
            $menu = '<ul class="nav nav-tabs">
                      <li class="active"><a data-toggle="tab" href="#home">'.get_string('home', 'mod_gotomeeting').'</a></li>
                      <li><a data-toggle="tab" href="#menu1">'.get_string('coorganizers', 'mod_gotomeeting').'</a></li>
                      <li><a data-toggle="tab" href="#menu2">'.get_string('registrant', 'mod_gotomeeting').'</a></li>
                      <li><a data-toggle="tab" href="#menu3">'.get_string('attendees', 'mod_gotomeeting').'</a></li>
                    </ul>';

            $service = ServiceFactory::getInstance(WebinarService::class);
            $response = $service->getWebinar($this->service->webinarkey);
            $service = null;
            // default div
            $content = $head;
            $content .= html_writer::tag('p', userdate($this->service->starttime, '%A, %B %d %Y'));
            $content .= html_writer::tag('p',userdate($this->service->starttime,'%I:%M %p').' | '.$timezones[$this->service->timezone].' | '.$this->duration());
            $content .= '<br/>';

	    $content .= $this->service_status();
            // default
            $content .= html_writer::tag('h4', get_string('joinwebinar', 'mod_gotomeeting'));
            $coorg = $DB->get_record('goto_coorganizers',['userid' => $USER->id, 'servicekey' => $this->service->webinarkey]);
            if($coorg) {
                $content .= $OUTPUT->single_button($coorg->joinlink, get_string('startasmoderatorinwebinar', 'mod_gotomeeting'));
            } else {
                $content .= html_writer::tag('button', get_string('addcoorganizer', 'mod_gotomeeting'),
                array('onclick' => 'return addCoorganizerInWebinar("'.$this->service->webinarkey.'")')); 
                $content .= html_writer::div('','', array('id' => 'ajax-loader'));
            }
            
            $contents = html_writer::div($content, 'tab-pane fade in active', ['id' => 'home']);
            
            // Co-Organizers div
            $content = '';
            $content .= html_writer::tag('button', get_string('coorganizers', 'mod_gotomeeting'), 
            ['onclick'=>'return getCoorganizerList("'.$this->service->webinarkey.'")']);
            $content .= html_writer::div('', '', ['id' => 'coorganizersHistory']);
            $contents .= html_writer::div($content, 'tab-pane fade ', ['id' => 'menu1']);
            
            // Participants
            $content = '';
            $content .= html_writer::tag('button',get_string('registrant', 'mod_gotomeeting'), 
            ['onclick'=>'return getWebinarRegistrant("'.$this->service->webinarkey.'")']); 
            $content .= html_writer::div('', '', ['id' => 'registrant']);
            $contents .= html_writer::div($content, 'tab-pane fade ', ['id' => 'menu2']);
            
             // Attendees
            $content = '';
            $content .= html_writer::tag('button',get_string('attendees', 'mod_gotomeeting'), 
            ['onclick'=>'return getAttendeesForAllWebinarSessions("'.$this->service->webinarkey.'")']); 
            $content .= html_writer::div('', '', ['id' => 'participantsHistory']);
            $contents .= html_writer::div($content, 'tab-pane fade ', ['id' => 'menu3']);


            $o = $menu.html_writer::div($contents, 'tab-content');     
        } else {
            $content = $this->service_status();
            $content = html_writer::tag('p', userdate($this->service->starttime, '%A, %B %d %Y'));
            $content .= html_writer::tag('p',userdate($this->service->starttime, '%I:%M %p').' | '.$timezones[$this->service->timezone].' | '.$this->duration());
            //$btn = $OUTPUT->single_button($reg->joinurl, 'Join meeting as Participants'); 

            $isregistered = $DB->get_record('goto_registrant',['userid' => $USER->id, 'webinarkey' => $this->service->webinarkey]);
            if($isregistered) {
                $btn = $OUTPUT->single_button($isregistered->joinurl, "Join webinar as Participants"); 
            } else {
                $btn = html_writer::tag('button', get_string('registerwebinar', 'mod_gotomeeting'), 
                array('onclick' => 'return regiserAsParticipantsInWebinar("'.$this->service->webinarkey.'")')); 
                $btn .= html_writer::div('','', array('id' => 'ajax-loader'));
            }
               
            $content .= '<br/>';
            $content .= $this->service_status();
            $content .= html_writer::tag('h4', get_string('joinwebinar', 'mod_gotomeeting'));

            $o = $head.$content.$btn;

        }
        return $o;
    }
}

/**
 * Training mod service
 */
class training_mod_service extends mod_service
{
     /**
     * Create training in GotoWebianr server
     *
     * @param array
     * @return int newly created module id
     */
    public function create($data)
    {
        global $DB;
        $data->timecreated = time();
        $data->timemodified = $data->timecreated;
        $timezones = DateTimeZone::listIdentifiers();
        $timezone = '';
        if(isset($timezones[$data->timezone])){
            $timezone = $timezones[$data->timezone];
        }

        // Make Api call to create meeting in GoToMeeting
        $training = new Training();
        $training->setName($data->name); 
        $training->setDescription(strip_tags($data->intro)); 
        $training->setTimeZone($timezone); 
        $training->setTimes(array(array("startDate" => $this->converToTz(date('Y-m-d h:i:s', $data->starttime),'UTC',$timezone), 
                                        "endDate" => $this->converToTz(date('Y-m-d h:i:s', $data->endtime),'UTC',$timezone)))); 
        $training->setRegistrationSettings(array(
                                            'disableConfirmationEmail' => (boolean) $data->disableConfirmationEmail,
                                            'disableWebRegistration' => (boolean) $data->disableWebRegistration
                                        ));
        try {
            $service = ServiceFactory::getInstance(TrainingService::class);
            $organizer = $service->getClient()->getAuth()->getOrganizerKey();
            $training->setOrganizers(array((int)$organizer));
            // Create training
            $service->createTraining($training);
            
            // Find training info
            $trainings = $service->getTrainings();
            $newtraining = null;
            foreach($trainings as $training) {
                if($training->getName() == $data->name){
                    $newtraining = $training;
                }
            }

        } catch (ClientException $ex) {
            $error = json_decode($ex->getResponse()->getBody(true), true);
            if($error['errorCode'] == 'AccessDenied') {
                throw new AccessDeniedException();
            }
        } catch (exception $ex) {
            $error = json_decode($ex->getResponse()->getBody(true), true);
            throw new \moodle_exception($error);
        }
        
        /*
        |------------------------------------
        | If training created on server then
        | training info and persist into DB
        |------------------------------------
        */
        if(is_null($newtraining)) {
            
            // Send Exception to user cann't create training
            throw new moodle_exception(get_string('servicenotavailable', 'mod_gotomeeting'));
        } else {
            $returnid = $DB->insert_record('gotomeeting', $data);
            $insert = new stdClass();
            $insert->instance = $returnid;
            $insert->name = $data->name;
            $insert->description = $data->intro;
            $insert->timezone = $timezone;
            $insert->starttime  = $data->starttime;
            $insert->endtime  = $data->endtime;
            $insert->registrationsettings = serialize($training->getRegistrationSettings());
            $insert->organizers = serialize(array());
            $insert->timecreated = time();
            $insert->trainingkey = $newtraining->getTrainingKey(); 
            $insert->trainingid = $newtraining->getTrainingId(); 
            $DB->insert_record('goto_training', $insert);
            return $returnid;  
        }
    }

    /**
     * Update training in GotoTraning server
     *
     * @param array
     * @return int newly created module id
     */
    public function update($data)
    {
        global $DB;
        $data->timemodified = time();
        $timezones = DateTimeZone::listIdentifiers();
        $timezone = '';
        if(isset($timezones[$data->timezone])){
            $timezone = $timezones[$data->timezone];
        }

        // Make Api call to update training in GoToMeeting
        $training = new Training();
        $service = ServiceFactory::getInstance(TrainingService::class);
        
        $oldtraining = $DB->get_record('gotomeeting', array('id' =>$data->instance));
        $oldservice = $DB->get_record('goto_training', array('instance' => $oldtraining->id));
        /*
         |--------------------------------
         | Update name & desc if changed
         |--------------------------------
         */
        if($data->name != $oldtraining->name || $data->intro != $oldtraining->intro) {
            $training->setName($data->name); 
            $training->setDescription(strip_tags($data->intro)); 
            $service->updateTrainingNameAndDescription($oldservice->trainingkey, $training);
        }
        
        /*
         |-------------------------------
         | Update times if changed
         |-------------------------------
         */
        
        if($data->starttime != $oldservice->starttime || $data->endtime != $oldservice->endtime) {
            $training->setTimeZone('GMT');
            $training->setTimes(array(array("startDate" => $this->converToTz(date('Y-m-d h:i:s', $data->starttime),'UTC',$timezone), 
                                        "endDate" => $this->converToTz(date('Y-m-d h:i:s', $data->endtime),'UTC',$timezone)))); 
            
            $service->updateTrainingTimes($oldservice->trainingkey, $training);
        } 
        
        /*
         |-----------------------------
         | Web settings
         |---------------------------
         */
        //$registersetting = unserialize($oldservice->registrationsettings);

        $training->setRegistrationSettings(array(
                                            'disableConfirmationEmail' => (boolean) $data->disableConfirmationEmail,
                                            'disableWebRegistration' => (boolean) $data->disableWebRegistration
                                        ));

        $service->updateTrainingRegistrationSettings($oldservice->trainingkey, $training);

        $url = $service->startTraining($oldservice->trainingkey);
        
        //$response = $service->updateTraining($training);
        $update = new stdClass();
        $update->id = $data->instance;
        $update->name = $data->name;
        $update->intro = $data->intro;
        $update->grade = $data->grade;
        $update->intro = $data->intro;
        $returnid = $DB->update_record('gotomeeting', $update);

        $row = $DB->get_record('goto_training', array('instance' => $data->instance));
        $insert = new stdClass();
        $insert->id = $row->id;
        $insert->name = $data->name;
        $insert->description = $data->intro;
        $insert->timezone = $timezone;
        $insert->starturl = $url['hostURL'];
        $insert->starttime  = $data->starttime;
        $insert->endtime  = $data->endtime;
        $insert->registrationsettings = serialize(array('disableConfirmationEmail' => $data->disableConfirmationEmail,
        'disableWebRegistration' => $data->disableWebRegistration));
        $insert->organizers = serialize(array());
        $insert->timemodified = time();
        $DB->update_record('goto_training', $insert);
        return $data->instance;  
    }

     /**
     * Remove training from GotoTraning server
     *
     * @param array
     * @return boolean
     */
    public function delete($id)
    {
        global $DB;
        $DB->delete_records('gotomeeting', array('id' => $id));
        $service = ServiceFactory::getInstance(TrainingService::class);
        $training = $DB->get_record('goto_training',array('instance' => $id));
        $service->deleteTraining($training->trainingkey);
        $DB->delete_records('goto_training', array('id' => $training->id));
        return true;
    }


     /**
     * Generate view for Training
     */
    public function render() {
        global $OUTPUT, $CFG, $DB, $USER;
        $head = html_writer::tag('h4', $this->service->name.' <small>('.ucfirst($this->instance->servicetype).')</small>');
        $timezones = $this->timezone_list();
        $isteacher = has_capability('moodle/course:update', context_course::instance($this->course->id));
        if($isteacher) {

            // Render page for teacher
            $menu = '<ul class="nav nav-tabs">
                      <li class="active"><a data-toggle="tab" href="#home">'.get_string('home', 'mod_gotomeeting').'</a></li>
                      <li><a data-toggle="tab" href="#menu1">'.get_string('organizers', 'mod_gotomeeting').'</a></li>
                      <li><a data-toggle="tab" href="#menu2">'.get_string('registrations', 'mod_gotomeeting').'</a></li>
                      <li><a data-toggle="tab" href="#menu3">'.get_string('recordings', 'mod_gotomeeting').'</a></li>
                      <li><a data-toggle="tab" href="#menu4">'.get_string('attendees', 'mod_gotomeeting').'</a></li>
                    </ul>';

            // default div
            $content = $head;
            $content .= html_writer::tag('p', userdate($this->service->starttime, '%A, %B %d %Y'));
            $content .= html_writer::tag('p',userdate($this->service->starttime,'%I:%M %p').' | '.$timezones[$this->service->timezone].' | '.$this->duration());
            $content .= '<br/>';

            $content .= $this->service_status();

            $content .= html_writer::tag('h4', get_string('jointraining', 'mod_gotomeeting'));
            $content .= $OUTPUT->single_button($this->service->starturl, get_string('starttraining', 'mod_gotomeeting'));

            unset($service);
            $contents = html_writer::div($content, 'tab-pane fade in active', ['id' => 'home']);
            
            // Co-Organizers div
            $content = '';
            $content .= html_writer::tag('button', get_string('organizers', 'mod_gotomeeting'), 
            ['onclick'=>'return getTrainingOrganizers("'.$this->service->trainingkey.'")']);
            $content .= html_writer::div('', '', ['id' => 'trainingOrganizerList']);
            $contents .= html_writer::div($content, 'tab-pane fade ', ['id' => 'menu1']);
            
            // Participants
            $content = '';
            $content .= html_writer::tag('button',get_string('registrations', 'mod_gotomeeting'),
            ['onclick'=>'return getTrainingRegistrations("'.$this->service->trainingkey.'")']);
            
            $content .= html_writer::div('', '', ['id' => 'trainingRegistrationList']);
            $contents .= html_writer::div($content, 'tab-pane fade ', ['id' => 'menu2']);
            
            // Recordings
            $content = '';
            $content .= html_writer::tag('button',get_string('recordings', 'mod_gotomeeting'), 
            ['onclick'=>'return getTrainingRecordings("'.$this->service->trainingkey.'")']); 
            $content .= html_writer::div('', '', ['id' => 'trainingRecordings']);
            $contents .= html_writer::div($content, 'tab-pane fade ', ['id' => 'menu3']);
            
            // Attendees
            $content = '';
            $content .= html_writer::tag('button', get_string('attendees', 'mod_gotomeeting'), 
            ['onclick'=>'return getTrainingAttendees("'.$this->service->trainingkey.'")']); 
            $content .= html_writer::div('', '', ['id' => 'trainingAttendees']);
            $contents .= html_writer::div($content, 'tab-pane fade ', ['id' => 'menu4']);

            $o = $menu.html_writer::div($contents, 'tab-content');     
        } else {
            /*
             |------------------
             | Student view
             |------------------
             */
            $btn = '';

            $content = html_writer::tag('p', userdate($this->service->starttime, '%A, %B %d %Y'));
            $content .= html_writer::tag('p',userdate($this->service->starttime, '%I:%M %p').' | '.$timezones[$this->service->timezone].' | '.$this->duration());
            
            $isregistered = $DB->get_record('goto_registrations',['userid' => $USER->id, 'trainingkey' => $this->service->trainingkey]);
            if($isregistered) {
                $btn = $OUTPUT->single_button($isregistered->joinurl, 'Join training as Participants'); 
            } else {
                $btn = html_writer::tag('button', get_string('registertraining', 'mod_gotomeeting'),
                array('onclick' => 'return regiserAsParticipantsInTraining("'.$this->service->trainingkey.'")')); 
                $btn .= html_writer::div('','', array('id' => 'ajax-stuff'));
            }
               
            $content .= '<br/>';
            $content .= $this->service_status();
            $content .= html_writer::tag('h4', get_string('jointraining', 'mod_gotomeeting'));

            $o = $head.$content.$btn;

        }
        return $o;
    }
}
