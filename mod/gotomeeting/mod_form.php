<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The main gotomeeting configuration form
 *
 * It uses the standard core Moodle formslib. For more info about them, please
 * visit: http://docs.moodle.org/en/Development:lib/formslib.php
 *
 * @package    mod_gotomeeting
 * @copyright  2015 Your Name
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/course/moodleform_mod.php');

$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/mod/gotomeeting/scripts.js'));
/**
 * Module instance settings form
 *
 * @package    mod_gotomeeting
 * @copyright  2015 Your Name
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_gotomeeting_mod_form extends moodleform_mod {
     private $updatetraininig = false;
    /**
     * Defines forms elements
     */
    public function definition() {
        global $DB,$CFG, $OUTPUT,$COURSE;

        $mform = $this->_form;
        $modservices = array('' => '-- Select service --' ,'meeting' => 'Meeting', 'webinar' => 'Webinar', 'training' => 'Training');
        $update = optional_param('update', false, PARAM_INT);
        $servicetype = optional_param('servicetype' , '', PARAM_RAW);
        /*
         |---------------------------------------
         | Generate Url for select input if 
         | update is false
         |---------------------------------------
         */
        if($update) {
            $params = array(
                'update' => optional_param('update','', PARAM_TEXT),
                'return' => optional_param('return','', PARAM_TEXT),
                'sr' => optional_param('sr','', PARAM_TEXT)
                );
            $url = $CFG->wwwroot.'/course/modedit.php?update='.$params['update'].'&return='.$params['return'].'&sr='.$params['sr'];
            $modinfo = $DB->get_record('course_modules', array('id' =>$update));
            $instance = $DB->get_record('gotomeeting', array('id' => $modinfo->instance));
            $servicetype = $instance->servicetype;
            $service = $DB->get_record('goto_'.$instance->servicetype, array('instance' => $instance->id));
        } else {
            $params = array(
                'add' => 'gotomeeting', 
                'update' => optional_param('update','', PARAM_TEXT),
                'type' => optional_param('type','', PARAM_TEXT),
                'course' => optional_param('course','', PARAM_INT), 
                'section' => optional_param('section','', PARAM_INT), 
                'sr' => optional_param('sr','', PARAM_INT)
                ); 
            $url = $CFG->wwwroot.'/course/modedit.php?add=gotomeeting&type='.$params['type'].'&course='.
            $params['course'].'&section='.$params['section'].'&sr='.$params['sr'];
        }


        $mform->addElement('header', 'general', get_string('general', 'form'));

        // Select input to find out service
        $serviceselect =$mform->addElement('select', 'servicetype', get_string('servicetype', 'mod_gotomeeting'), $modservices, 
            array('onchange' => 'window.location = "'.$url.'&servicetype="+this.value'));
        $mform->addRule('servicetype', get_string('error'), 'required', '', 'server', false, false);
        $serviceselect->setSelected($servicetype);
        
        
        // Adding the standard "name" field.
        $mform->addElement('text', 'name', get_string('gotomeetingname', 'gotomeeting'));
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEANHTML);
        }
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');
        $mform->addHelpButton('name', 'gotomeetingname', 'gotomeeting');

        // Adding the standard "intro" and "introformat" fields.
        if ($CFG->branch >= 29) {
            $this->standard_intro_elements();
        } else {
            $this->add_intro_editor();
        }
        /*
         |----------------------------
         | Custom fields
         |----------------------------
         */
        $timezones =  DateTimeZone::listIdentifiers();     
        $mform->setType('meetingtype', PARAM_RAW);
        $mform->addElement('date_time_selector', 'starttime', get_string('from'));
        
        $mform->addElement('date_time_selector', 'endtime', get_string('to'));
        //array_search($service->timezone, $timezones))
        $timezoneselect = $mform->addElement('select', 'timezone', get_string('timezone'), $timezones);
        
        
        if(isset($service)) {
            $timezoneselect->setSelected(array_search($service->timezone, $timezones));
            $mform->setDefault('starttime',$service->starttime);
            $mform->setDefault('endtime', $service->endtime);
        }

        /*
         |-------------------------
         | Extra field for Meeting
         |-------------------------
         */
        if($servicetype == "meeting") {
            $meetingtypes = array('immediate' => 'Immediate', 'scheduled' => 'Scheduled', 'recurring' =>'Recurring');
            $mform->addElement('select', 'meetingtype', get_string('meetingtype', 'mod_gotomeeting'), $meetingtypes);

            $mform->addElement('text', 'maxparticipants', get_string('maxparticipants', 'gotomeeting'));
            $mform->setType('maxparticipants', PARAM_INT);
                    $mform->addRule('maxparticipants', get_string('error'), 'required', '', 'server', false, false);

            $mform->addElement('text', 'duration', get_string('duration', 'gotomeeting'));
            $mform->setType('duration', PARAM_INT);
                    $mform->addRule('duration', get_string('error'), 'required', '', 'server', false, false);
            if(isset($service)) {
                $mform->setDefault('meetingtype', $service->meetingtype);
                $mform->setDefault('maxparticipants', $service->maxparticipants);
                $mform->setDefault('duration', $service->duration);
            }

        }
        
        /*
         |------------------------------
         | Extra fields for Training
         |------------------------------
         */
        if($servicetype == "training") {
            $mform->addElement('selectyesno', 'disableConfirmationEmail', get_string('disableconfirmationemail', 'gotomeeting'));
            $mform->setType('disableConfirmationEmail', PARAM_INT);
            $mform->addElement('selectyesno', 'disableWebRegistration', get_string('disablewebregistration', 'gotomeeting'));
            $mform->setType('disableWebRegistration', PARAM_INT);
            $mform->setDefault('disableConfirmationEmail', 1);
            $mform->setDefault('disableWebRegistration', 1);
        }

        $this->standard_grading_coursemodule_elements();
        $this->standard_coursemodule_elements();
        $this->add_action_buttons();
    }

    public function get_defaults($modid)
    {
        global $DB;
        //$result = $DB->get_record();

    
    }

    // form verification
    function validation($data, $files) {
        global $COURSE, $DB, $CFG;
        $errors = parent::validation($data, $files);
        if(isset($data['starttime']) && isset($data['endtime'])) {

            $starttime = new DateTime();
            $starttime->setTimestamp($data['starttime']);

            $curtime = new DateTime();
            $curtime->setTimestamp(time());
            $diff =  $curtime->diff($starttime);
            if(optional_param('update',false, PARAM_TEXT)) {
                if($data['servicetype'] == 'training') { 
                    if($diff->days == 0) {
                    $errors['endtime'] = get_string('error_earlytime', 'mod_gotomeeting', ucfirst($data['servicetype']));
                    }
                }
            }

            if($data['starttime'] > $data['endtime']) {
                $errors['starttime'] = get_string('error_starttime', 'mod_gotomeeting');
            }
            
            $endtime = new DateTime();
            $endtime->setTimestamp($data['endtime']);
            $interval =  $endtime->diff($starttime);
            $hours = $interval->h;
            $hours = $hours + ($interval->days * 24);
            if($hours > 24) {
                $errors['endtime'] = get_string('error_maxtime', 'mod_gotomeeting');
            }
        }
        return $errors;
    }
}

//git log --pretty=format:"%h%x09%an%x09%ad%x09%s"
