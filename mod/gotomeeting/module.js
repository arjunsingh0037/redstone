
function getAttendeesForAllWebinarSessions(webinar)
{
	var xhttp = new XMLHttpRequest();
	document.getElementById("participantsHistory").innerHTML = '<center><img src="pix/general-loader.gif"/><p>Loading ...</p></center>';
	xhttp.onreadystatechange = function() {
	    if (xhttp.readyState == 4 && xhttp.status == 200) {
	      	var jsondata = JSON.parse(xhttp.responseText);
            if(jsondata.status) {
	      	    var head = '<table id="Participant-tbl" class="table table-striped"><thead><tr><th>Participant name</th>'+
                '<th>Email name</th><th>Time <small>(In sec)</small></th><th class="header c2">Join Time</th><th class="header c3">Leave Time</th></tr></thead>';
                var content = '<tbody>';
                var subkey = 0;
		 	    for (var key in jsondata) {
                    content += '<tr><td>' + jsondata[key].firstName +' '+jsondata[key].lastName + '</td>' + 
                    '<td>' + jsondata[key].email + '</td>';
                    var totalSec = jsondata[key].attendanceTimeInSeconds;
					var hours = parseInt( totalSec / 3600 ) % 24;
					var minutes = parseInt( totalSec / 60 ) % 60;
					var seconds = totalSec % 60;
					var result = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);

                    content += '<td>' + result + '</td>'+
 					'<td>' + jsondata[key].attendance[subkey].joinTime + '</td>'+
                    '<td>' + jsondata[key].attendance[subkey].leaveTime + '</td>' + '</tr>';
                }
                var footer = '</tbody></table>'
                document.getElementById("participantsHistory").innerHTML = head+content+footer;
                $("#Participant-tbl").DataTable({
                    dom: "Bfrtip",
                    buttons: [
                        "copy", "csv", "excel", "pdf", "print"
                    ]
                });
            } else {
                error('participantsHistory', jsondata.error);
            }
	    }
	};
	xhttp.open("GET", "ajax.php?callback=getAttendeesForAllWebinarSessions&webinar="+webinar, true);
	xhttp.send();
}
function getMeetingAttendees(meetingid)
{
	var xhttp = new XMLHttpRequest();
    loader('meetingattendees');
	xhttp.onreadystatechange = function() {
	    if (xhttp.readyState == 4 && xhttp.status == 200) {
	      	var json = JSON.parse(xhttp.responseText);
            if(json.status) {
	      	    var head = '<table id="Participant-tbl" class="table table-striped"><thead><tr><th>Name</th>'+
                '<th>Email</th></th><th>Join Time</th><th>Leave Time</th></tr></thead>';
                var content = '<tbody>';
		 	    for (var key in json.attendees) {
                    content += '<tr><td>' + json.attendees[key].attendeename +'</td>' + 
 					'<td>' + json.attendees[key].attendeeemail + '</td>'+
                    '<td>' + json.attendees[key].jointime + '</td>'+ 
                    '<td>' + json.attendees[key].leavetime + '</td>'+ '</tr>';
                }
                var footer = '</tbody></table>'
                document.getElementById('meetingattendees').innerHTML = head+content+footer;
                $("#Participant-tbl").DataTable({
                    dom: "Bfrtip",
                    buttons: [
                        "copy", "csv", "excel", "pdf", "print"
                    ]
                });
            } else {
                error('meetingattendees', json.error);
            }
	    }
	};
	xhttp.open("GET", "ajax.php?callback=getMeetingAttendees&meetingid="+meetingid, true);
	xhttp.send();
}



function getWebinarRegistrant(webinar)
{
	var xhttp = new XMLHttpRequest();
    loader('registrant');
	xhttp.onreadystatechange = function() {
	    if (xhttp.readyState == 4 && xhttp.status == 200) {
	      	var jsondata = JSON.parse(xhttp.responseText);
            if(jsondata.status) {
	      	    var head = '<table id="Participant-tbl" class="table table-striped"><thead><th>Name</th><th>Email</th></tr></thead>';
                var content = '<tbody>';
                var subkey = 0;
                var registrants = jsondata.registrants;
		 	    for (var key in registrants) {
                    content += '<tr><td>' + registrants[key].firstName +' '+registrants[key].lastName + '</td>' 
                    + '<td>' + registrants[key].email + '</td>' 
                    + '</tr>';
                }
                var footer = '</tbody></table>';
                document.getElementById("registrant").innerHTML = head+content+footer;
                $("#Participant-tbl").DataTable({
                    dom: "Bfrtip",
                    buttons: [
                        "copy", "csv", "excel", "pdf", "print"
                    ]
                });
            } else {

            }
	    }
	};
	xhttp.open("GET", "ajax.php?callback=getWebinarRegistrant&webinar="+webinar, true);
	xhttp.send();
}


function getCoorganizerList(webinar)
{
	var xhttp = new XMLHttpRequest();
	document.getElementById("coorganizersHistory").innerHTML = '<center><img src="pix/general-loader.gif"/><p>Loading ...</p></center>';
	xhttp.onreadystatechange = function() {
	    if (xhttp.readyState == 4 && xhttp.status == 200) {
	      	var jsondata = JSON.parse(xhttp.responseText);
            if(jsondata) {
	      	    var head = '<table id="CoOrganizer-tbl" class="table table-striped"><thead><tr><th>Co-Organizer name</th><th>Email</th></tr></thead>';
                var content = '<tbody>';
                var subkey = 0;
                var organizer = jsondata.coorganizers;
		 	    for (var key in organizer) {
                    content += '<tr><td>' + organizer[key].givenName+ '</td>' + 
                    '<td>' + organizer[key].email + '</td></tr>';
                }
                var footer = '</tbody></table>'
                document.getElementById("coorganizersHistory").innerHTML = head+content+footer;
                $("#CoOrganizer-tbl").DataTable({
                    dom: "Bfrtip",
                    buttons: [
                        "copy", "csv", "excel", "pdf", "print"
                    ]
                });
            } else {

            }
	    }
	};
	xhttp.open("GET", "ajax.php?callback=getWebinarCoorganizers&webinar="+webinar, true);
	xhttp.send();
}

function getTrainingOrganizers(training)
{
	var xhttp = new XMLHttpRequest();
    loader('trainingOrganizerList');
	xhttp.onreadystatechange = function() {
	    if (xhttp.readyState == 4 && xhttp.status == 200) {
	      	var jsondata = JSON.parse(xhttp.responseText);
            if(jsondata.status) {
	      	    var head = '<table id="CoOrganizer-tbl" class="table table-striped"><thead><tr><th>Name</th><th>Surname</th><th>Email</th></tr></thead>';
                var content = '<tbody>';
                var subkey = 0;
		 	    for (var key in jsondata.organizer) {
                    content += '<tr><td>' + jsondata.organizer[key].givenName+ '</td>' +'<td>' + jsondata.organizer[key].surname + '</td>'+
                    '<td>' + jsondata.organizer[key].email + '</td></tr>';
                }
                var footer = '</tbody></table>';
                document.getElementById("trainingOrganizerList").innerHTML = head+content+footer;
                $("#CoOrganizer-tbl").DataTable({
                    dom: "Bfrtip",
                    buttons: [
                        "copy", "csv", "excel", "pdf", "print"
                    ]
                });
            } else {
                 error(jsondata.error, 'trainingOrganizerList');
            }
        }
	};
	xhttp.open("GET", "ajax.php?callback=getTrainingOrganizers&training="+training, true);
	xhttp.send();
}



function regiserAsParticipantsInWebinar(webinar)
{
    var xhttp = new XMLHttpRequest();
    loader('ajax-loader');
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            var json = JSON.parse(xhttp.responseText);
            if(json.status){
                window.location.reload();
            } else {
                error('ajax-loader', json.error);
            }
        }
    };
    xhttp.open("GET", "ajax.php?callback=regiserAsParticipantsInWebinar&webinar="+webinar, true);
    xhttp.send();
}



function addCoorganizerInWebinar(webinar) {
    var xhttp = new XMLHttpRequest();
    document.getElementById("ajax-loader").innerHTML = '<center><img src="pix/general-loader.gif"/><p>Loading ...</p></center>';
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            var json = JSON.parse(xhttp.responseText);
            if(json.status){
                window.location.reload();
            } else {
                document.getElementById("ajax-loader").innerHTML = json.error;
            }
        }
    };
    xhttp.open("GET", "ajax.php?callback=addCoorganizerInWebinar&webinar="+webinar, true);
    xhttp.send();
}

function registerAsOrganizerInTraining(trainingkey) {
    var xhttp = new XMLHttpRequest();
    loader('ajax-loader');
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            var json = JSON.parse(xhttp.responseText);
            if(json.status){
                window.location.reload();
            } else {
                error('ajax-loader', json.error);
            }
        }
    };
    xhttp.open("GET", "ajax.php?callback=registerAsOrganizerInTraining&webinar="+trainingkey, true);
    xhttp.send();
}

function regiserAsParticipantsInTraining(training)
{
    var xhttp = new XMLHttpRequest();
    loader('ajax-stuff');
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            var json = JSON.parse(xhttp.responseText);
            if(json.status){
                window.location.reload();
            } else {
                error('ajax-stuff', json.error);
            }
        }
    };
    xhttp.open("GET", "ajax.php?callback=regiserAsParticipantsInTraining&training="+training, true);
    xhttp.send();
}

function getTrainingRegistrations(trainingkey)
{
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "ajax.php?callback=getTrainingRegistrations&training="+trainingkey, true);
    xhttp.send();      
    loader('trainingRegistrationList');
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            var data = JSON.parse(xhttp.responseText);
            if(data.status) {
	      	    var head = '<table id="Registration-tbl" class="table table-striped"><thead><tr><th>Name</th>'+
                '<th>Email</th><th>Status</th></tr></thead>';
                var content = '<tbody>';
                var subkey = 0;
		 	    for (var key in data.registration) {
                    content += '<tr><td>' + data.registration[key].givenname +'</td><td>' + 
                    data.registration[key].email + '</td><td>'+
                    data.registration[key].status + '</td></tr>';
                }
                var footer = '</tbody></table>'
                document.getElementById("trainingRegistrationList").innerHTML = head+content+footer;
                $("#Registration-tbl").DataTable({
                    dom: "Bfrtip",
                    buttons: [
                        "copy", "csv", "excel", "pdf", "print"
                    ]
                });
            } else {
		error('trainingRegistrationList', data.error);	
	    }
        }
    };

}

function getTrainingAttendees(trainingkey)
{
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "ajax.php?callback=getTrainingAttendees&training="+trainingkey, true);
    xhttp.send();      
    loader('trainingAttendees');
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            var data = JSON.parse(xhttp.responseText);
            if(data.status) {
	      	    var head = '<table id="Registration-tbl" class="table table-striped"><thead><tr><th>Name</th>'+
                '<th>Email</th><th>Status</th></tr></thead>';
                var content = '<tbody>';
                var subkey = 0;
		 	    for (var key in data.registration) {
                    content += '<tr><td>' + data.registration[key].givenname +'</td><td>' + 
                    data.registration[key].email + '</td><td>'+
                    data.registration[key].status + '</td></tr>';
                }
                var footer = '</tbody></table>'
                document.getElementById("trainingAttendees").innerHTML = head+content+footer;
                $("#Registration-tbl").DataTable({
                    dom: "Bfrtip",
                    buttons: [
                        "copy", "csv", "excel", "pdf", "print"
                    ]
                });
            } else {
                error('trainingAttendees', data.error);
            }
        }
    };
}



function getTrainingRecordings(trainingkey)
{
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "ajax.php?callback=getTrainingRecording&training="+trainingkey, true);
    xhttp.send();
    loader('trainingRecordings');
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            var json = JSON.parse(xhttp.responseText);
            if(json.status) {
	      	    var head = '<table id="recordings-tbl" class="table table-striped"><thead><tr><th>Name</th>'+
                       '<th>Start Date</th><th>End date</th><th>Download</th></tr></thead>';
                var content = '<tbody>';
                var subkey = 0;
                var recording =  json['recording'];
		 	    for (var key in recording) {
                    content += '<tr><td>' + recording[key].name+'</td>' + 
                    '<td>' + recording[key].startdate+'</td>'+
                    '<td>' + recording[key].enddate+'</td>'+
                    '<td><a href="'+recording[key].downloadurl+'" class="btn btn-primary">Download</a></td></tr>';
                }
                var footer = '</tbody></table>'
                document.getElementById("trainingRecordings").innerHTML = head+content+footer;
                $("#recordings-tbl").DataTable({
                    dom: "Bfrtip",
                    buttons: [
                        "copy", "csv", "excel", "pdf", "print"
                    ]
                });
            } else {
                error('trainingRecordings', json.error);
            }
        }
    };
}

function getAttendanceDetails(trainingkey)
{
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "ajax.php?callback=getTrainingAttendanceDetails&training="+trainingkey, true);
    xhttp.send();
    loader('ajax-loader');
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            var json = JSON.parse(xhttp.responseText);
	      	var head = '<table id="recordings-tbl" class="table table-striped"><thead><tr><th>Name</th>'+
                       '<th>Description</th><th>Start Date</th><th>End date</th><th>Download</th></tr></thead>';
            var content = '<tbody>';
            var subkey = 0;
		 	for (var key in json) {
                    content += '<tr><td>' + json[key].givenName+'</td>' + 
                    '<td>' + json[key].email+'</td>'+
                    '<td>' + json[key].timeInSession+'</td>'+
                    '<td>' + json[key].inSessionTimes[0]['joinTime']+'</td>'+
                    '<td>' + json[key].inSessionTimes[0]['leaveTime']+'</td>'+
                    '<td>' + json[key].inSessionTimes[0]['timeInPartOfSession']+'</td>'+
                    '</tr>';
            }
            var footer = '</tbody></table>';
            document.getElementById("attendanceDetails").innerHTML = head+content+footer;
        }
    };
}

function getMeetingAttendanceDetails(meetingid)
{
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "ajax.php?callback=getAttendeesByMeeting&meetingid="+meetingid, true);
    xhttp.send();
    loader('meetingAttendanceDetails');
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            var json = JSON.parse(xhttp.responseText);
	      	var head = '<table id="recordings-tbl" class="table table-striped"><thead><tr><th>Name</th>'+
                       '<th>Description</th><th>Start Date</th><th>End date</th><th>Download</th></tr></thead>';
            var content = '<tbody>';
            var subkey = 0;
		 	for (var key in json) {
                    content += '<tr><td>' + json[key].givenName+'</td>' + 
                    '<td>' + json[key].email+'</td>'+
                    '<td>' + json[key].timeInSession+'</td>'+
                    '<td>' + json[key].inSessionTimes[0]['joinTime']+'</td>'+
                    '<td>' + json[key].inSessionTimes[0]['leaveTime']+'</td>'+
                    '<td>' + json[key].inSessionTimes[0]['timeInPartOfSession']+'</td>'+
                    '</tr>';
            }
            var footer = '</tbody></table>';
            document.getElementById("meetingAttendanceDetails").innerHTML = head+content+footer;
        }
    };
}



function error(div, message) {
    document.getElementById(div).innerHTML = "<div class='alert alert-warning'><a href='#' class='close' "+
    "data-dismiss='alert' aria-label='close'>&times;</a>"+message+"</div>";
}

function info(div, message) {
    document.getElementById(div).innerHTML = "<div class='alert alert-warning'><a href='#' class='close' "+
    "data-dismiss='alert' aria-label='close'>&times;</a>"+meesage+"</div>";
}

function loader(div) {
    document.getElementById(div).innerHTML = '<center><img src="pix/general-loader.gif"/><p>Loading ...</p></center>';
}
