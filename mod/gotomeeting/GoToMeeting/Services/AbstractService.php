<?php
/**
 * Abstract service implementation.
 * @package GoToMeeting\Services
 */

namespace GoToMeeting\Services;

/**
 * Abstract service implementation. Additional services should extend this class.
 *
 * @package GoToMeeting\Services
 */
class AbstractService
{
    /**
     * @var string root URL for authorizing requests
     */
    protected $endpoint = '';

    /**
     * @var GoToMeeting\Client
     */
    protected $client;
    
    /**
     * {GTM, GTW} specify type
     * @var type 
     */
    protected $type;

    /**
     * Default constructor.
     *
     * @param GoToMeeting\Client $client
     */
    public function __construct($client)
    {
        if(!is_null($client->getAuth())){
            $client->setType(static::getType()); 
        }
        $this->client = $client;       
    }

    public function getClient()
    {
        return $this->client;
    }

}
