<?php
/**
 * Service configured to authenticate with the API.
 * @package GoToMeeting\Services
 */

namespace GoToMeeting\Services;

use GoToMeeting\Models\Auth;

/**
 * Class AuthService
 *
 * @package GoToMeeting\Services
 */
class AuthService extends AbstractService
{
    protected $endpoint = 'oauth';

    /**
     * Authenticate with the server and retrieve an auth token.
     *
     * Currently implements Citrix's {@link https://developer.citrixonline.com/page/direct-login "Direct Login"} method.
     *
     * @param string $userId
     * @param string $password
     * @return GoToMeeting\Models\Auth
     */
    public function authenticate($userId, $password)
    {
        $url = "{$this->endpoint}/access_token";
        $query = array(
            'grant_type' => 'password',
            'user_id' => $userId,
            'password' => $password,
            'client_id' =>$this->client->getApiKey()
            );
        $jsonBody = $this->client->sendRequest('GET', $url, $query, true);
        return new Auth($jsonBody);
    }
}
