<?php
/**
 * Abstract service implementation.
 * @package GoToMeeting\Services
 */

namespace GoToMeeting\Services\Meeting;

use GoToMeeting\Services\AbstractService;
/**
 * Abstract service implementation. Additional services should extend this class.
 *
 * @package GoToMeeting\Services
 */
class AbstractMeetingService extends AbstractService
{
    public static function getType() {
        return 'G2M';
    }
}
