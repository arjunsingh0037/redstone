<?php
/**
 * Abstract service implementation.
 * @package GoToMeeting\Services
 */

namespace GoToMeeting\Services\Webinar;

use GoToMeeting\Models\Registrant;
/**
 * Abstract service implementation. Additional services should extend this class.
 *
 * @package GoToMeeting\Services
 */
class RegistrantsService extends AbstractWebinarService
{
    /**
     * @var string root URL for authorizing requests
     */
    protected $endpoint = 'organizers/{{organizerKey}}/webinars/{{webinarKey}}/registrants';

    /**
     * @var \GoToMeeting\Client
     */
    protected $client;

    /**
     * Default constructor.
     *
     * @param \GoToMeeting\Client $client
     */
    public function __construct($client)
    {
        parent::__construct($client);
        $this->endpoint = str_replace('{{organizerKey}}', $this->client->getAuth()->getOrganizerKey(),$this->endpoint);
    }


    public function getRegistrants($webinarkey) {
        $apiurl = str_replace('{{webinarKey}}', $webinarkey, $this->endpoint);
        $response = $this->client->sendRequest('GET', $apiurl);
        $registrants = array();
        foreach($response as $registrant) {
            $registrants[] = new Registrant($registrant);
        }
        return $registrants;
    }
    
    public function getRegistrant($webinarKey, $registrantKey) {
        $apiurl = str_replace('{{webinarKey}}', $webinarKey, $this->endpoint);
        $apiurl .= '/'.$registrantKey;
        $response = $this->client->sendRequest('GET', $apiurl);
        return $response;
    }

    /**
     * Register an attendee for a scheduled webinar. 
     * The response contains the registrantKey and join URL for the registrant.
     *
     * @param GoToMeeting\Models\Registrant $registrant
     * @param int $webinarKey
     * @param string $accept
     */
    public function createRegistrant($webinarKey, $registrant, $accept = '') {
        $apiurl = str_replace('{{webinarKey}}', $webinarKey, $this->endpoint);
        $response = $this->client->sendRequest('POST', $apiurl, null, false , $registrant->toArrayForApi());
        return new Registrant($response);
    }
    
    /**
     * Removes a webinar registrant from current registrations for the specified webinar. The webinar must be a scheduled, future webinar.
     *
     * @param int $webinarKey
     * @param int $registrantKey
     */
    public function deleteRegistrat($webinarKey, $registrantKey) {
         $apiurl = str_replace('{{webinarKey}}', $webinarKey, $this->endpoint);
         $apiurl .= '/'.$registrantKey;
         $response = $this->client->sendRequest('DELETE', $apiurl);
         return $response;
    }
    
    public function getRegistrationFields($param) {
        $apiurl = str_replace('{{webinarKey}}', $webinarKey, $this->endpoint);
        $apiurl .= '/fields';
        $response = $this->client->sendRequest('GET', $apiurl);
        return $response;
    }
}
