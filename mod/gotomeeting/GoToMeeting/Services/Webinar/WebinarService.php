<?php
/**
 * Abstract service implementation.
 * @package GoToMeeting\Services
 */

namespace GoToMeeting\Services\Webinar;

use GoToMeeting\Models\Webinar;
/**
 * Abstract service implementation. Additional services should extend this class.
 *
 * @package GoToMeeting\Services
 */
class WebinarService extends AbstractWebinarService
{
    
    /**
     * @var string root URL for authorizing requests
     */
    protected $endpoint = 'organizers/{{organizerKey}}/webinars';

    /**
     * @var \GoToMeeting\Client
     */
    protected $client;
    
    /**
     * Default constructor
     *
     * @param GoToMeeting\Client $cleint
     */
    public function __construct($client)
    {
        parent::__construct($client);
        $this->endpoint = str_replace('{{organizerKey}}', $this->client->getAuth()->getOrganizerKey(),$this->endpoint);
    }

    /**
     * Returns details for completed webinars for the specified organizer 
     * and completed webinars of other organizers where the specified 
     * organizer is a co-organizer.
     *
     * @param string fromTime 
     * @param string toTime 
     * @return array
     */
    public function  getHistoricalWebinars($fromTime, $toTime)
    {
        $this->endpoint = str_replace('webinars', 'historicalWebinars', $this->endpoint);
        $response = $this->client->sendRequest('GET', "{$this->endpoint}", $query);
        return $response;
    }
    
    /**
     * Returns webinars scheduled for the future for the specified organizer and
     * webinars of other organizers where the specified organizer is a co-organizer.
     *
     * @return array
     */
    public function getUpcomingWebinars()
    {
        $this->endpoint = str_replace('webinars', 'upcomingWebinars', $this->endpoint);
        $response = $this->client->sendRequest('GET', "{$this->endpoint}");
        return $response;
    }
    
    /**
     * Returns webinars scheduled for the future for a specified organizer.
     *
     * @return array
     */
    public function getAllWebinars()
    {
        $response = $this->client->sendRequest('GET', "{$this->endpoint}");
        return $response;
    }
    
    /**
     * Creates a single session webinar. The call requires a webinar subject and description, 
     * and a start and end time. The response provides a numeric webinarKey in string format for the new webinar.
     * Once a webinar has been created with this method, you can accept registrations.
     *
     * @param GoToMeeting\Models\Webinar $webinar
     * @return array
     */
    public function createWebinar(Webinar $webinar) 
    {
        $this->endpoint = str_replace('{{organizerKey}}', $this->client->getAuth()->getOrganizerKey(), $this->endpoint);
        $response = $this->client->sendRequest('POST', "{$this->endpoint}", null, false , $webinar->toArrayForApi());
        return $response;
    }
    
    /**
     * Cancels a specific webinar. By default, cancellation notice emails to registrants 
     * will not be sent. To have the cancellation notice emails sent to registrants 
     * (as if the webinar was cancelled through the G2W website), include the parameter
     * sendCancellationEmails=true in the request parameters. When the cancellation emails 
     * are sent, the default generated message is used in the cancellation email body
     *
     * @return array 
     */
    public function cancelWebinar($webinarKey) {
        $apiurl = $this->endpoint.'/'.$webinarKey;
        $response = $this->client->sendRequest('DELETE', $apiurl);
        return $response;
    }

    /**
     * Retrieve information on a specific webinar. If the type of the webinar is "sequence",
     * a sequence of future times will be provided. Webinars of type "series" are
     * treated the same as normal webinars - each session in the webinar series has 
     * a different webinarKey. If an organizer cancels a webinar, 
     * then a request to get that webinar would return a "404 Not Found" error.
     *
     * @param int $webinarKey
     * @return array
     */
    public function getWebinar($webinarKey) {
        $apiurl = $this->endpoint.'/'.$webinarKey;
        $response = $this->client->sendRequest('GET', $apiurl);
        return new Webinar($response);
    }
    
    /**
     * Updates a webinar. The call requires at least one of the parameters in the request body.
     *
     * @param $webinarKey
     * @param GoToMeeting\Models\Webinar $webinar
     */
    public function updateWebinar($webinarKey, $webinar) {
        $this->endpoint .= '/'.$webinarKey;
        $response = $this->client->sendRequest('PUT', "{$this->endpoint}", null, false , $webinar->toArrayForApi());
        return $response;
    }
    
    /**
     * Returns all attendees for all sessions of the specified webinar.
     * 
     * @param int $webinarKey
     * @return array
     */
    public function getAttendeesForAllWebinarSessions($webinarKey) {
        $this->endpoint .= '/'.$webinarKey.'/attendees';
        $response = $this->client->sendRequest('GET', "{$this->endpoint}");
        return $response;
    }

    /**
     * Updates the audio/conferencing settings for a specific webinar
     * 
     * @param int $webinarKey
     */
    public function getAudioInformation($webinarKey) {
        $this->endpoint .= '/'.$webinarKey.'/audio';
        $response = $this->client->sendRequest('GET', "{$this->endpoint}");
        return $response;
    }

    /**
     * Updates the audio/conferencing settings for a specific webinar
     *
     * @param int $webinarKey
     * @param int $audioinfo
     *
     */
    public function updateAudioInformation($webinarKey, $audioinfo) {
        $this->endpoint .= '/'.$webinarKey.'/audio';
        $response = $this->client->sendRequest('POST', "{$this->endpoint}", null, false , $audioinfo);
        return $response;
    }

    /**
     * Retrieves the meeting times for a webinar.
     *
     * @param int 
     */

    public function getWebinarMeetingTimes($param) {
        $this->endpoint .= '/'.$webinarKey.'/meetingtimes';
        $response = $this->client->sendRequest('GET', "{$this->endpoint}");
        return $response;
    }
    
    /**
     *
     */
    public function getWebinarPanelists($param) {
        $this->endpoint .= '/'.$webinarKey.'/panelists';
        $response = $this->client->sendRequest('GET', "{$this->endpoint}");
        return $response;
    }
    
    /**
     * Create panelists for a specified webinar
     *
     * @param int $webinarKey
     * @param array $panelists
     */
    public function createPanelists($webinarKey, $panelists) {
        $this->endpoint .= '/'.$webinarKey.'/panelists';
        $response = $this->client->sendRequest('GET', "{$this->endpoint}");
        return $response;
    }

    /**
     * Gets performance details for all sessions of a specific webinar.
     *
     * @param 
     */
    public function getPerformanceForAllWebinarSessions($param) {
        $this->endpoint .= '/'.$webinarKey.'/performance';
        $response = $this->client->sendRequest('GET', "{$this->endpoint}");
        return $response;
    }
}
