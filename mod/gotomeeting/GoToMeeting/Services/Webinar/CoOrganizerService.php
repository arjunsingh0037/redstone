<?php
/**
 * CoOrganizer service implementation.
 * @package GoToMeeting\Services
 */

namespace GoToMeeting\Services\Webinar;

use GoToMeeting\Models\CoOrganizers;
/**
 * Abstract service implementation. Additional services should extend this class.
 *
 * @package GoToMeeting\Services
 */
class CoOrganizerService extends AbstractWebinarService
{
    /**
     * @var string root URL for authorizing requests
     */
    protected $endpoint = '/organizers/{{organizerKey}}/webinars/{{webinarKey}}/coorganizers';

    /**
     * @var GoToMeeting\Client
     */
    protected $client;

    /**
     * Default constructor.
     *
     * @param GoToMeeting\Client $client
     */
    public function __construct($client)
    {
        parent::__construct($client);
        $this->client = $client;
        $this->endpoint = str_replace('{{organizerKey}}', $this->client->getAuth()->getOrganizerKey(), $this->endpoint);
    }

    /**
     * Returns the co-organizers for the specified webinar. 
     * The original organizer who created the webinar is filtered out of the list. 
     * If the webinar has no co-organizers, an empty array is returned. 
     * Co-organizers that do not have a GoToWebinar account are returned as external co-organizers.
     * 
     * @param sting $webinarKey 
     * @return mixed
     */
    public function getCoOrganizers($webinarKey) {
        $apiurl = str_replace('{{webinarKey}}', $webinarKey, $this->endpoint);
        $response = $this->client->sendRequest('GET', $apiurl);
        return $response;
    }
    
    /**
     * Creates co-organizers for the specified webinar. This call requires the admin role.
     * For co-organizers that have a GoToWebinar account you have to set the parameter "external" to "false". 
     * In this case you have to pass the parameter "organizerKey" only. For co-organizers that have no GoToWebinar account
     * you have to set the parameter "external" to "true". 
     * In this case you have to pass the parameters "givenName" and "email".
     * Since there is no parameter for "surname" you should pass first and last name to the parameter "givenName".
     *
     * @param GoToMeeting\Models\CoOrganiers $coorganizer
     * @return void { HTTP Status Code 201 if created }
     */
    public function createCoOrganizers(CoOrganizers $coorganizer, $webinarKey) {
        $apiurl = str_replace('{{webinarKey}}', $webinarKey, $this->endpoint);
	        
$response = $this->client->sendRequest('POST', $apiurl, null, false, $coorganizer->toArrayForApi());
	$result = new CoOrganizers($response[0]);
	return $result;
    }
    
    /**
     * Deletes an internal co-organizer specified by the coorganizerKey (memberKey).
     *
     * @param long $webinarKey
     * @param long $coorganizerKey
     *
     * @return void { HTTP Status Code 204 No Content (Co-organizer was deleted)}
     */
    public function deleteCoOrganizer($webinarKey, $coorganizerKey) {
        $apiurl = str_replace('{{webinarKey}}', $webinarKey, $this->endpoint);
        $apiurl .= '/'.$coorganizerKey;
        $response = $this->client->sendRequest('DELETE', $apiurl);
        return $response;
    }
    
    /**
     * Resends an invitation email to the specified co-organizer
     *
     * @param long $webinarKey
     * @param long $coorganizerKey
     *
     * @return void { HTTP Status Code 204 No Content (Invitation email was sent)}
     */
    public function resendInvitation($webinarKey, $coorganizerKey) {
        $apiurl = str_replace('{{webinarKey}}', $webinarKey, $this->endpoint);
        $apiurl .= '/'.$coorganizerKey.'/resendInvitation';
        $response = $this->client->sendRequest('POST', $apiurl, null, false, array());
        return $response;
    }
}
