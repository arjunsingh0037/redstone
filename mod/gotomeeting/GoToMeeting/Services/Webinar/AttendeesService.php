<?php

namespace GoToMeeting\Services\Webinar;

/**
 * Abstract service implementation. Additional services should extend this class.
 *
 * @package GoToMeeting\Services
 */
class AttendeesService extends AbsractWebinarService
{
    /**
     * @var string root URL for authorizing requests
     */
    protected $endpoint = '/organizers/{organizerKey}/webinars/{webinarKey}/sessions/{sessionKey}/attendees';

    /**
     * @var GoToMeeting\Client
     */
    protected $client;

    //Get registrants
    
    public function getSessionAttendees($param)
    {
        $path = '/organizers/{organizerKey}/webinars/{webinarKey}/sessions/{sessionKey}/attendees  ';
    }
    
    public function getAttendeeQuestions($param)
    {
        $path = '/organizers/{organizerKey}/webinars/{webinarKey}/sessions/{sessionKey}/attendees/{registrantKey}/questions   ';
    }

    /**
     *
     *
     */
    public function getattendee($param)
    {
      $path ='/organizers/{organizerKey}/webinars/{webinarKey}/sessions/{sessionKey}/attendees/{registrantKey}';
    }
    
    public function getAttendeePollAnswers($param)
    {
      $str = '/organizers/{organizerKey}/webinars/{webinarKey}/sessions/{sessionKey}/attendees/{registrantKey}/polls ';  
    }
    
    public function getAttendeeSurveyAnswers($param)
    {
        $path = '/organizers/{organizerKey}/webinars/{webinarKey}/sessions/{sessionKey}/attendees/{registrantKey}/surveys';
    }
}
