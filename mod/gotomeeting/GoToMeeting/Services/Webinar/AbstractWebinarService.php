<?php
/**
 * Abstract service implementation.
 * @package GoToMeeting\Services
 */

namespace GoToMeeting\Services\Webinar;

use GoToMeeting\Services\AbstractService;

/**
 * Abstract service implementation. Additional services should extend this class.
 *
 * @package GoToMeeting\Services
 */
class AbstractWebinarService extends AbstractService
{
    public static function getType()
    {
        return 'G2W';
    }
}
