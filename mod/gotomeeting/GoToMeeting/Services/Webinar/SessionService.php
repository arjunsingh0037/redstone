<?php
/**
 * Abstract service implementation.
 * @package GoToMeeting\Services
 */

namespace GoToMeeting\Services\Webinar;

/**
 * Abstract service implementation. Additional services should extend this class.
 *
 * @package GoToMeeting\Services
 */
class SessionService extends AbstractWebinarService
{
    /**
     * @var string root URL for authorizing requests
     */
    protected $endpoint = 'organizers/{{organizerKey}}/webinars/{{webinarKey}}/sessions';

    /**
     * Default constructor.
     *
     * @param GoToMeeting\Client $client
     */
    public function __construct($client)
    {
         parent::__construct($client);
        $this->endpoint = str_replace('{{organizerKey}}', $this->client->getAuth()->getOrganizerKey(),$this->endpoint);
    }

    /**
     * Retrieve all completed sessions of all the webinars of a given organizer.
     * 
     * @param string 
     * @param string
     * @return array
     */
    public function getOrganizerSessions($formtime, $totime)
    {
        $apiurl = "organizers/{{organizerKey}}/sessions";
        $this->endpoint = str_replace('{{organizerKey}}', $this->client->getAuth()->getOrganizerKey(), $apiurl);
        $response = $this->client->sendRequest('GET', "{$this->endpoint}", array('fromTime' => $formtime, 'toTime' => $totime));
        return $response;
    }

    /**
     * Retrieves details for all past sessions of a specific webinar.
     *
     * @param int
     * @return array
     */
    public function getWebinarSessions($webinarkey)
    {
        $this->endpoint = str_replace('{{webinarKey}}', $webinarkey, $this->endpoint);
        $response = $this->client->sendRequest('GET', "{$this->endpoint}");
        return $response;
    }

    /**
     * Retrieves attendance details for a specific webinar session that has ended. 
     * If attendees attended the  session ('registrantsAttended'), specific attendance details, 
     * such as attendenceTime for a registrant, will also be retrieved.
     * 
     * @param int 
     * @param int
     * @return array
     */
    public function getWebinarSession($webinarkey, $sessionskey)
    {
        $this->endpoint = str_replace('{{webinarKey}}', $webinarkey, $this->endpoint);
        $this->endpoint .= '/'.$sessionskey;
        $response = $this->client->sendRequest('GET', "{$this->endpoint}");
        return $response;
    }

    /**
     * Get performance details for a session.
     *
     * @param int 
     * @param int
     * @return array
     */
    public function getSessionPerformance($webinarkey, $sessionskey)
    {
        $this->endpoint = str_replace('{{webinarKey}}', $webinarkey, $this->endpoint);
        $this->endpoint .= '/'.$sessionskey.'/performance';
        $response = $this->client->sendRequest('GET', "{$this->endpoint}");
        return $response;
    }

    /**
     * Retrieve all collated attendee questions and answers for polls from a specific webinar session.
     *
     * @param int 
     * @param int
     * @return array
     */
    public function getSessionPolls($webinarkey, $sessionskey)
    {
        $this->endpoint = str_replace('{{webinarKey}}', $webinarkey, $this->endpoint);
        $this->endpoint .= '/'.$sessionskey.'/polls';
        $response = $this->client->sendRequest('GET', "{$this->endpoint}");
        return $response;
    }

    /**
     * Retrieve questions and answers for a past webinar session.
     *
     * @param int 
     * @param int
     * @return array
     */
    public function getSessionQuestions($webinarkey, $sessionskey)
    {
        $this->endpoint = str_replace('{{webinarKey}}', $webinarkey, $this->endpoint);
        $this->endpoint .= '/'.$sessionskey.'/questions';
        $response = $this->client->sendRequest('GET', "{$this->endpoint}");
        return $response;
    }

    /**
     * Retrieve surveys for a past webinar session.
     *
     * @return array
     */
    public function getSessionSurveys($webinarkey, $sessionskey)
    {  
        $this->endpoint = str_replace('{{webinarKey}}', $webinarkey, $this->endpoint);
        $this->endpoint .= '/'.$sessionskey.'/surveys';
        $response = $this->client->sendRequest('GET', "{$this->endpoint}");
        return $response;
    }
}
