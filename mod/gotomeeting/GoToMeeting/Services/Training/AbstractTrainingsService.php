<?php

namespace GoToMeeting\Services\Training;

use GoToMeeting\Services\AbstractService;

class AbstractTrainingsService extends AbstractService
{
	public function getType()
	{
		return 'G2T';
	}
}
