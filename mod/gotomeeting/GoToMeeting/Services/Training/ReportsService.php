<?php

namespace GoToMeeting\Services\Training;

/**
 * Operations available for training reports.
 */
class ReportsService
{
	/**
	 * @var string
	 */
	protected $endpoint = "reports/organizers/{{organizerKey}}/sessions";

	/**
	 * This call returns all session details over a given date range for a given organizer.
	 * A session is a completed training event.
	 *  
	 * @var
	 */
	public function getSessionsbyDateRange()
	{
		# code...
	}

	/**
	 * This call retrieves a list of registrants from a specific completed training session. The response 
	 * includes the registrants' email addresses, and if they attended, it includes the duration of each 
	 * period of their attendance in minutes, and the times at which they joined and left.
	 * If a registrant does not attend, they appear at the bottom of the listing with timeInSession = 0.
	 *
	 * @return
	 */
	public function getAttendanceDetails()
	{
		# code...
	}

	/**
	 * This call returns session details for a given training. A session is a completed training event.
	 * Each training may contain one or more sessions.
	 *
	 * @return 
	 */
	public function getSessionsByTraining()
	{
		# code...
	}
}
