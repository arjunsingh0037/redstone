<?php

namespace GoToMeeting\Services\Training;

use GoToMeeting\Models\Registration;

/**
 * Operations available for registrants of a given training.
 */

class RegistrationService extends AbstractTrainingsService
{
    protected $endpoint = "organizers/{{organizerKey}}/trainings/{{trainingKey}}/registrants";
   
    public function __construct($client)
    {
        parent::__construct($client);
        $this->endpoint = str_replace("{{organizerKey}}", $this->client->getAuth()->getOrganizerKey(), $this->endpoint);
    }
    
    /**
     * Retrieves details on all registrants for a specific training. Registrants can be:
     * WAITING - registrant registered and is awaiting approval (where organizer has required approval)
     * APPROVED - registrant registered and is approved
     * DENIED - registrant registered and was not approved.
     *
     * IMPORTANT: The registrant data caches are typically updated immediately and the data will be returned in the response.
     * However, the update can take as long as two hours.
     *
     * @return
     */
    public function getTrainingRegistrants($trainingKey)
    {
        $this->endpoint = str_replace("{{trainingKey}}", $trainingKey, $this->endpoint);
        $response = $this->client->sendRequest('GET', $this->endpoint);
        $registrations = [];
        foreach($response as $row) {
            $registrations[] = new Registration($row);
        }
        return $registrations;
    }

    /**
     * Registers one person, identified by a unique email address, for a training. Approval is automatic unless payment or approval is required.
     * The response contains the Confirmation page URL and Join URL for the registrant.
     * NOTE: If some registrants do not receive a confirmation email, the emails could be getting blocked by their email
     * server due to spam filtering or a grey-listing setting.
     *
     * @return
     */
    public function registerForTraining(Registration $registration)
    {
        $this->endpoint = str_replace("{{trainingKey}}", $registration->getTrainingKey(), $this->endpoint);
        $response = $this->client->sendRequest('POST', $this->endpoint, null, false, $registration->toArrayForApi());
        return new Registration($response);
    }

    /**
     * This call cancels a registration in a scheduled training for a specific registrant. If the registrant has paid for the training, 
     * a cancellation cannot be completed with this method; it must be completed on the Citrix external admin site. 
     * No notification is sent to the registrant or the organizer by default. The registrant can re-register if needed.
     *
     * @return 
     */
    public function cancelRegistration($trainingKey)
    {
        $this->endpoint = str_replace("{{trainingKey}}", $trainingKey, $this->endpoint);
        $response = $this->client->sendRequest('DELETE', $this->endpoint);
    }
    /**
     * Retrieves details for specific registrant in a specific training. Registrants can be: 
     * WAITING - registrant registered and is awaiting approval (where organizer has required approval)
     * APPROVED - registrant registered and is approved
     * DENIED - registrant registered and was not approved.
     *
     * IMPORTANT: The registrant data caches are typically updated immediately and the data will be returned in the response.
     * However, the update can take as long as two hours.
     *
     * @return
     */
    public function getRegistrant($trainingKey, $registrationKey)
    {
        $this->endpoint = str_replace("{{trainingKey}}", $trainingKey, $this->endpoint);
        $this->endpoint .= '/'.$registrationKey;
        $response = $this->client->sendRequest('GET', $this->endpoint);
        return new Registration($response);
    }
}
