<?php

namespace GoToMeeting\Services\Training;

use GoToMeeting\Client;
use GoToMeeting\Models\Training;

class TrainingService extends AbstractTrainingsService
{
	protected $endpoint ="organizers/{{organizerKey}}/trainings";

    public function __construct(Client $client) {
        parent::__construct($client);
       	$this->endpoint = str_replace("{{organizerKey}}", $this->client->getAuth()->getOrganizerKey(), $this->endpoint);
    }

	/**
	 * This call retrieves information on all scheduled trainings for a given organizer. 
	 * The trainings are returned in the * order in which they were created.
	 *
	 * @return array
	 */
	public function getTrainings()
	{
		$response = $this->client->sendRequest('GET', $this->endpoint);
        $trainings = array();
        foreach($response as $training) {
            $trainings[] = new Training($training);
        }
        return $trainings;
	}

	/**
	 * Schedules a training of one or more sessions. 
	 * 
     * @param Training 
     * @return array
	 */
	public function createTraining(Training $training)
	{
		$response = $this->client->sendRequest('POST',"{$this->endpoint}", null, false, $training->toArrayForApi());
        return $response;
	}

	public function  deleteTraining($trainingkey)
	{
        $this->endpoint .= "/{$tainingkey}";
		return $this->client->sendRequest('DELETE', $this->endpoint);
	}

	public function getTraining($trainingkey)
	{
        $this->endpoint .= "/{$trainingkey}";
		$response = $this->client->sendRequest('GET', $this->endpoint);
        return $response;
	}
    
    /**
     * Request to update a scheduled training name and description.
     * 

     */
    public function getManagementURLforTraining($trainingkey)
    {
        $this->endpoint .= "/{$tainingkey}/manageUrl";
		$response = $this->client->sendRequest('GET', $this->endpoint);
        return $response;
    }
	
    /**
     * Request to update a scheduled training name and description.
     *
     * @param
     * @return
     */
    public function updateTrainingNameAndDescription($trainingkey, $training) {
        $apiurl = $this->endpoint."/$trainingkey/nameDescription";
		$response = $this->client->sendRequest('PUT', $apiurl, null, false, $training->toArrayForApiNameAndDesc());
        return $response;
    }
	
    /**
     * Retrieves organizer details for a specific training. This is only applicable to multi-user accounts with sharing enabled (co-organizers).
     * 
     * @param Training
     * @return array of Organizer
     */
    public function getTrainingOrganizers($trainingkey)
	{
        $apiurl = $this->endpoint."/{$trainingkey}/organizers";
		$response = $this->client->sendRequest('GET', $apiurl);
        return $response;
	}

    /**
     * Replaces the co-organizers for a specific training. The scheduling organizer cannot be unassigned.
     * Organizers will be notified via email if the notifyOrganizers parameter is set to true. 
     * Replaced organizers are not notified. This method is only applicable to multi-user accounts with sharing enabled (co-organizers).
     *
     * @param Training
     * @return array
     */
	public function updateTrainingOrganizers($updateorganizer)
	{
	    $apiurl = $this->endpoint."/{$tainingkey}/nameDescription";
		$response = $this->client->sendRequest('PUT', $apiurl, null, false, $updateorganizer);
        return $response;
    }
	
    /**
     * An API request to automatically enable or disable web registrations and confirmation emails to registrants.
     * 
     * @param 
     * @return void
     */
    public function updateTrainingRegistrationSettings($trainingkey, Training $training)
	{
		$apiurl = $this->endpoint ."/{$trainingkey}/registrationSettings";
		$response = $this->client->sendRequest('PUT', $apiurl, null, false, $training->toArrayForApiUpdateSetting());
        return $response;
	}
    
	/**
     *
     **/
    public function getStartUrl($trainingkey)
	{
        $apiurl = $this->endpoint."/{$trainingkey}/startUrl";
		$response = $this->client->sendRequest('GET', $apiurl);
        return $response;
    }

    /**
     * A request to update a scheduled training's start and end times.
     * If the request contains 'notifyTrainers = true' and 'notifyRegistrants = true', both organizers and registrants are notified. 
     * The response provides the number of notified trainers and registrants.
     *
     * @param Training
     * @return array
     */
	public function updateTrainingTimes($trainingkey, Training $training)
	{
		$apiurl = $this->endpoint."/{$trainingkey}/times";
        //echo json_encode($training->toArrayForApiUpdateTrainingTimes());
		$response = $this->client->sendRequest('PUT', $apiurl, null, false, $training->toArrayForApiUpdateTrainingTimes());
        return $response;
	}
	
    /**
     * Returns a URL that can be used to start a training. When this URL is opened in a web browser,
     * the GoToTraining client will be downloaded and launched and the training will start.
     * A login of the organizer is not required.
     * 
     * @param int
     * @return array
     */
    public function  startTraining($trainingkey)
	{
		$apiurl = str_replace('{{trainingKey}}',$trainingkey, "/trainings/{{trainingKey}}/start"); 
        $response = $this->client->sendRequest('GET', $apiurl);
        return $response;
	}

}
