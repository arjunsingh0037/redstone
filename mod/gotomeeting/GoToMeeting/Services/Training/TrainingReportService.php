<?php

namespace GoToMeeting\Services\Training;

use GoToMeeting\Client;
use GoToMeeting\Models\Training;
use GoToMeeting\Models\TrainingSession;

class TrainingReportService extends AbstractTrainingsService
{
	protected $endpoint ="/reports/organizers/{{organizerKey}}/";

    public function __construct(Client $client) {
        parent::__construct($client);
       	$this->endpoint = str_replace("{{organizerKey}}", $this->client->getAuth()->getOrganizerKey(), $this->endpoint);
    }

    /**
     * This call returns session details for a given training. A session is a completed training event. 
     * Each training may contain one or more sessions
     *
     * @param int
     * @return array of TrainingSession
     */
    public function getSessionsByTraining($trainingkey)
    {
        $this->endpoint .= 'trainings/'.$trainingkey;
        $response = $this->client->sendRequest('GET', $this->endpoint);
        $trainingsession = array();
        foreach($response as $session) {
            $trainingsession = new TrainingSession($session);
        }
        return $trainingsession;
    }
    
    /**
     * This call retrieves a list of registrants from a specific completed training session.
     * The response includes the registrants' email addresses, and if they attended, it includes 
     * the duration of each period of their attendance in minutes, and the times at which they joined 
     * and left. If a registrant does not attend, they appear at the bottom of the listing with timeInSession = 0.
     *
     * @param int
     */
    public function getAttendanceDetails($sessionkey)
    {
        $this->endpoint .= str_replace('{{sessionKey}}', $sessionkey, 'sessions/{{sessionKey}}/attendees');
        $attendances = $this->client->sendRequest('GET', $this->endpoint);
        return $attendances;
    }

}
