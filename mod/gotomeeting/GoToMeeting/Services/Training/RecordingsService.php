<?php

namespace GoToMeeting\Services\Training;

use GoToMeeting\Models\Recording;
/**
 * Operations available for online recordings.
 */
class RecordingsService extends AbstractTrainingsService
{
    protected $endpoint = "trainings/{{trainingKey}}/recordings";
    
    /**
     * This call retrieves information on all online recordings for a given training. 
     * If there are none, it returns an empty list.
     * 
     * @return array
     */
    public function getOnlineRecordingsforTraining($trainingkey)
    {
        $this->endpoint = str_replace('{{trainingKey}}', $trainingkey, $this->endpoint);
        $response = $this->client->sendRequest('GET', $this->endpoint);
        $recordings = [];
        foreach($response['recordingList'] as $recording) {
            $recordings[] = new Recording($recording);
        }
        return $recordings;
    }
    /**
     * This call provides the download for the given recording by returning a 
     * 302 redirect to the original file.
     *
     * @return array
     */
    public function getDownloadforOnlineRecordings()
    {
        $this->client->sendRequest('GET', $this->endpoint);
    }
}
