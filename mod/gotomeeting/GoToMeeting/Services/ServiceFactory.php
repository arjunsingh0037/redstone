<?php

namespace GotoMeeting\Services;

use GoToMeeting\Client;
use GoToMeeting\Services\AuthService;
use GuzzleHttp\Exception\ClientException;
use GoToMeeting\Exception\AuthenticationFailedException;

class ServiceFactory
{
    /**
     * @var AbstractService
     */
    private static $instance = null;
    /**
     * Restrict object creation using new operator
     */
    private function __construct(){}

    /**
     * Prevent cloning
     */
    public function __clone()
    {
        trigger_error("Do not clone object instead call ServiceFactory::getInstance(Service::class)", E_USER_ERROR);
    }

    /**
     * Returns instance of passed service class name
     * 
     * @param string Service classes
     * @return object
     */
    public static function getInstance($class)
    {
        if(is_null(static::$instance))
        {
            self::instance($class);
        }

        if(!static::$instance instanceof $class)
        {
            self::instance($class);
        }

        return static::$instance;
    }

    private static function instance($class)
    {
        $config = self::load_config($class);
        $client = new Client($config['apikey']);
        $service = new AuthService($client);
        try {
            $auth = $service->authenticate($config['email'], $config['password']);
            $client->setAuth($auth);
        } catch (ClientException $ex) {
            throw new AuthenticationFailedException();
        }
        static::$instance = new $class($client);
    }

    private static function load_config($class)
    {
        global $DB;
        $service = explode("\\", $class);
        if(isset($service[2]) && 
            $service[2] == 'Webinar' ||
            $service[2] == 'Meeting' ||
            $service[2] == 'Training') {
            $type = strtolower($service[2]);
        } else {
            throw new \exception('Service not found :' . $class);
        }
        $config['apikey'] = $DB->get_field('config_plugins', 'value',
            array('plugin' => 'gotomeeting', 'name' => $type.'_key'));

        $config['email'] = $DB->get_field('config_plugins', 'value',
            array('plugin' => 'gotomeeting', 'name' => $type.'_email'));

        $config['password'] = $DB->get_field('config_plugins', 'value',
            array('plugin' => 'gotomeeting', 'name' => $type.'_password'));
        return $config;
    }
}
