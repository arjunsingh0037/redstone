<?php

namespace GoToMeeting\Models;

use JsonSerializable;

class Webinar implements JsonSerializable
{
    /**
     * Subject of the Webinar
     *
     * @var string
     */
    private $subject;
    
    /**
     * Description of Webinar
     * 
     * @var string
     */
    private $description;

    /**
     * Times of webinar
     *
     * @var array 
     */
    private $times;
    
    /**
     * Timezone 
     *
     * @var string
     */
    private $timeZone;
    
    /**
     * Timezone 
     *
     * @var string
     */
    private $inSession;


    /**
     * Default constructor. parse provided response from JOSN
     *
     * @param array $response
     *
     */
    public function __construct($response = array())
    {
        $this->parseFromJson($response);    
    }

    public function jsonSerialize() {
        return get_object_vars($this);        
    }

    public function getSubject(){
        return $this->subject;
    }
    
    public function getDescription(){
        return $this->description;
    }
    
    public function getTimes(){
        return $this->times;
    }

    public function getTimeZone(){
        return $this->timeZone;
    }
    
    public function getIsSession() {
        return $this->isSession;
    }

    public function setSubject($subject){
        $this->subject = $subject;
    }

    public function setDescription($description){
        $this->description = $description;
    }
    
    public function setTimes($times){
        $this->times = $times;
    }
    
    public function setIsSession($isSession) {
        $this->isSession = $isSession;
    }

    public function setTimeZone($timeZone){
        $this->timeZone = $timeZone;
    }
   
    
    public function toArrayForApi()
    {
        $webinarArray = array();
        $webinarArray['subject'] = $this->subject;
        $webinarArray['description'] = $this->description;
        $webinarArray['times'] = array($this->times);
        $webinarArray['timeZone'] = $this->timeZone;
        return $webinarArray;
    }

    public function parseFromJson($response)
    {
        if(isset($response['subject']))
            $this->subject = $response['subject'];

        if(isset($response['description']))
            $this->description = $response['description'];
        
         if(isset($response['times']))
            $this->times = $response['times'];
        
        if(isset($response['timeZone']))
            $this->timeZone = $response['timeZone'];
        
        if(isset($response['isSession']))
            $this->isSession = $response['isSession'];

    }

   
}
