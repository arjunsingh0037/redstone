<?php

namespace GoToMeeting\Models;

class Training implements \JsonSerializable
{	
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $timeZone;

    /**
     * Contains { 'startDate' => '' , 'endDate' => ''} 
     *
     * @var array 
     */
    private $times;  

    /**
     * { disableConfirmationEmail: true,disableWebRegistration: true }
     *
     * @var array
     */
    private $registrationSettings;

    /**
     * @var array
     */
    private $organizers;

    /**
     * @var int
     */
     private $trainingKey;

    public function __construct($response = array())
    {
        $this->parseFromJson($response);
    }
    /**
     * Returns training name
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Returns training key
     * 
     * @return string
     */
    public function getTrainingKey()
    {
        return $this->trainingKey;
    }

    public function getTrainingId()
    {
        return $this->trainingId;
    }

    /**
     * Returns training description
     * 
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * Returns training description
     * 
     * @return string
     */
    public function getTimeZone()
    {
        return $this->timeZone;
    }
    
    /**
     * Returns times
     * 
     * @return string
     */
    public function getTimes()
    {
        return $this->times;
    }

    /**
     * Returns RegistrationSettings in array
     * [ 
     *   "disableConfirmationEmail": true,
     *   "disableWebRegistration": true
     * ]
     * 
     * @return array
     */
    public function getRegistrationSettings()
    {
        return $this->registrationSettings;
    }
    
    /**
     * Returns list of organizers
     * 
     * @return array
     */
    public function getOrganizers()
    {
        return $this->organizers;
    }
    
    /**
     * Set training name
     * 
     * @param string
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    
    /**
     * Set training description
     * 
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
    
    /**
     * Set training description
     * 
     * @param string
     */
    public function setTimeZone($timeZone)
    {
        $this->timeZone = $timeZone;
    }
    
    public function setTimes(array $times)
    {
        $this->times = $times;
    }

    public function setRegistrationSettings($registrationSettings)
    {
        $this->registrationSettings = $registrationSettings;
    }

    public function setOrganizers($organizers)
    {
        $this->organizers = $organizers;
    }
    
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
    
    public function parseFromJson($response = array())
    {
        if(isset($response["name"]))
            $this->name = $response["name"];
        
        if(isset($response["description"]))
            $this->description = $response["description"];
        
        if(isset($response["timeZone"]))
            $this->timeZone = $response["timeZone"];
        
        if(isset($response["registrationSettings"]))
            $this->registrationSettings = $response["registrationSettings"];
        
        if(isset($response["organizers"]))
            $this->organizers = $response["organizers"];
        
        if(isset($response["trainingKey"]))
            $this->trainingKey = $response["trainingKey"];
        
        if(isset($response["trainingId"]))
            $this->trainingId = $response["trainingId"];

    }
    
    public function toArrayForApi() {
        $training = array();
        $training["name"] = $this->name; 
        $training["description"] = $this->description;
        $training["timeZone"] = $this->timeZone;
        $training["times"] = $this->times;
        $training["registrationSettings"] = $this->registrationSettings;
        $training["organizers"] = $this->organizers;
        
        return $training;
    }
    
    public function toArrayForTrainingUpdate()
    {
        $training = array();
        $training["timeZone"] = $this->timeZone;
        $training["times"] = $this->times;
        $training["notifyRegistrants"] = $this->notifyRegistrants;
        $training["notifyTrainers"] = $this->notifyTrainers;
        return $training;
    }

    public function toArrayForApiNameAndDesc() 
    {
        $training = array();
        $training["name"] = $this->getName();
        $training["description"] = $this->getDescription();
        return $training;
    }

    public function toArrayForApiUpdateTrainingTimes()
    {
        $training = array();
        $training['timeZone'] = $this->timeZone;
        $training['times'] = $this->times;
        $training['notifyRegistrants'] = true;
        $training['notifyTrainers'] = true;
        return $training;
    }

    public function toArrayForApiUpdateSetting()
    {
        return $this->getRegistrationSettings();
    }
}
