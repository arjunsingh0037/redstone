<?php

namespace GoToMeeting\Models;

class CoOrganizers implements \JsonSerializable {

    /**
     * @var boolean
     */
    private $external;

    /**
     * @var string 
     */
    private $organizerKey;

    /**
     * @var string
     */
    private $givenName;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $joinLink;

    /**
     * @var int
     */
    private $memberKey;

    /**
     * @var int
     */
    private $webinarKey;


    /**
     * @var registrantKey
     */
    private $registrantKey;

    public function __construct($response = array()) {
        $this->parseFromJson($response);
    }

    public function getExternal() {
        return $this->external;
    }

    public function getRegistrantKey(){
        return $this->registrantKey;
    }
    public function getOrganizerKey() {
        return $this->organizerKey;
    }

    public function getGivenName() {
        return $this->givenName;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getJoinLink() {
        return $this->joinLink;
    }

    public function getMemberKey() {
        return $this->memberKey;
    }

    public function getWebinarKey() {
        return $this->webinarKey;
    }

    public function setExternal($external) {
        $this->external = $external;
    }

    public function setOrganizerKey($organizerKey) {
        $this->organizerKey = $organizerKey;
    }

    public function setGivenName($givenName) {
        $this->givenName = $givenName;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setJoinLink($joinLink) {
        $this->joinLink = $joinLink;
    }

    public function setMemberKey($memberKey) {
        $this->memberKey = $memberKey;
    }
    
    public function setWebinarKey($webinarKey) {
        $this->webinarKey = $webinarKey;
    }
    
    public function setRegistrantKey($registrantKey) {
        $this->registrantKey = $registrantKey;
    }


    public function jsonSerialize() {
        return get_object_vars($this);
    }

    public function vars()
    {
        return get_object_vars($this);
    }

    public function toArrayForApi() {
        $coorganizer = array();
        $coorganizer['external'] = $this->external;
        $coorganizer['organizerKey'] = $this->organizerKey;
        $coorganizer['givenName'] = $this->givenName;
        $coorganizer['email'] = $this->email;
        return array($coorganizer);
    }

    public function parseFromJson($response) {
        
        if(isset($response['external']))
            $this->external = $response['external'];

        if(isset($response['organizerKey']))
            $this->organizerKey = $response['organizerKey'];

        if(isset($response['givenName']))
            $this->givenName = $response['givenName'];

        if(isset($response['email']))
            $this->email = $response['email'];

        if(isset($response['joinLink']))
            $this->joinLink = $response['joinLink'];

        if(isset($response['memberKey']))
            $this->memberKey = $response['memberKey'];
        
        if(isset($response['registrantKey']))
            $this->registrantKey = $response['registrantKey'];
    }

}
