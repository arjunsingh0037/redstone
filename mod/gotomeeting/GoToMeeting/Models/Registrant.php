<?php

namespace GoToMeeting\Models;

class Registrant implements \JsonSerializable{

    private $firstName;
    private $lastName;
    private $email;
    private $source;
    private $address;
    private $city;
    private $state;
    private $zipCode;
    private $country;
    private $phone;
    private $status;
    private $timeZone;
    private $organization;
    private $jobTitle;
    private $registrantKey;
    private $registrationDate;
    private $joinUrl; 
    private $questionsAndComments;
    private $industry;
    private $numberOfEmployees;
    private $purchasingTimeFrame;
    private $purchasingRole;
    private $responses = array();

    public function getFirstName() {
        return $this->firstName;
    }

    public function getLastName() {
        return $this->lastName;
    }
    
    public function getRegistrantKey() {
        return $this->registrantKey;
    }
    
    public function getJoinUrl() {
        return $this->joinUrl;
    }

    public function getEmail() {
        return $this->email;
    }
    public function getTimeZone() {
        return $this->timeZone;
    }

    public function getSource() {
        return $this->source;
    }

    public function getAddress() {
        return $this->address;
    }

    public function getState() {
        return $this->state;
    }
    
    public function getStatus() {
        return $this->status;
    }
    
    public function getRegistrationDate() {
        return $this->registrationDate;
    }

    public function getZipCode() {
        return $this->zipCode;
    }

    public function getCountry() {
        return $this->country;
    }

    public function getOrganization() {
        return $this->organization;
    }

    public function getCity() {
        return $this->city;
    }

    public function getPhone() {
        return $this->phone;
    }

    public function getJobTitle() {
        return $this->jobTitle;
    }

    public function getQuestionsAndComments() {
        return $this->questionsAndComments;
    }

    public function getIndustry() {
        return $this->industry;
    }

    public function getNumberOfEmployees() {
        return $this->numberOfEmployees;
    }

    public function getPurchasingTimeFrame() {
        return $this->purchasingTimeFrame;
    }

    public function getPurchasingRole() {
        return $this->purchasingRole;
    }

    public function getResponses() {
        return $this->responses;
    }

    public function __construct($response = array()) {
        $this->parseFromJson($response);
    }

    public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }

    public function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    public function setEmail($email) {
        $this->email = $email;
    }
    public function setRegistrationDate($registrationDate) {
        $this->setRegistrationDate = $registrationDate;
    }
 
    public function setJoinUrl($joinUrl) {
        $this->joinUrl = $joinUrl;
    }

  public function setRegistrantKey($registrantKey) {
        $this->registrantKey = $registrantKey;
    }


    public function setAddress($address) {
        $this->address = $address;
    }

    public function setSource($surce) {
        $this->source = $surce;
    }

    public function setState($state) {
        $this->state = $state;
    }

    public function setZipCode($zipCode) {
        $this->zipCode = $zipCode;
    }

    public function setPhone($phone) {
        $this->phone = $phone;
    }
    
    public function setStatus($status) {
        $this->status = $status;
    }

    public function setJobTitle($jobTitle) {
        $this->jobTitle = $jobTitle;
    }

    public function setQuestionsAndComments($questionsAndComments) {
        $this->questionsAndComments = $questionsAndComments;
    }

    public function setCountry($country) {
        $this->country = $country;
    }

    public function setIndustry($industry) {
        $this->industry = $industry;
    }

    public function setNumberOfEmployees($numberOfEmployees) {
        $this->numberOfEmployees = $numberOfEmployees;
    }

    public function setPurchasingTimeFrame($purchasingTimeFrame) {
        $this->purchasingTimeFrame = $purchasingTimeFrame;
    }

    public function setPurchasingRole($purchasingRole) {
        $this->purchasingRole = $purchasingRole;
    }

    public function setResponses($response) {
        $this->responses[] = $response;
    }

    public function setOrganization($organization) {
        $this->organization = $organization;
    }

    public function setCity($city) {
        $this->city = $city;
    }
    public function setTimeZone($timeZone) {
        $this->timeZone = $timeZone;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
    public function parseFromJson($response)
    {
        if(isset($response['registrantKey']))
            $this->setRegistrantKey($response['registrantKey']);
        if(isset($response['joinUrl']))
            $this->setJoinUrl($response['joinUrl']);
        if(isset($response['firstName']))
            $this->setFirstName($response['firstName']);
        if(isset($response['lastName']))
            $this->setLastName($response['lastName']);
        if(isset($response['email']))
            $this->setEmail($response['email']);
        if(isset($response['status']))
            $this->setStatus($response['status']);
        if(isset($response['registrationDate']))
            $this->setRegistrationDate($response['registrationDate']);
        if(isset($response['timeZone']))
            $this->setTimeZone($response['timeZone']);
    }

    public function toArrayForApi()
    {
        $registrant = array();
        $registrant['firstName'] = $this->firstName;
        $registrant['lastName'] = $this->lastName;
        $registrant['email'] = $this->email;
        $registrant['source'] = $this->source;
        $registrant['address'] = $this->address;
        $registrant['city'] = $this->city; 
        $registrant['state'] = $this->state;
        $registrant['zipCode'] = $this->zipCode; 
        $registrant['country'] = $this->country;
        $registrant['phone'] = $this->phone;
        $registrant['organization'] = $this->organization;
        $registrant['jobTitle'] = $this->jobTitle;
        $registrant['questionsAndComments'] = $this->questionsAndComments; 
        $registrant['industry'] = $this->industry; 
        $registrant['numberOfEmployees'] = $this->numberOfEmployees; 
        $registrant['purchasingTimeFrame'] = $this->purchasingTimeFrame;
        $registrant['purchasingRole'] = $this->purchasingRole;
        $registrant['responses'] = $this->responses;
        return $registrant;
    }
}
