<?php
/**
 * Representation of an attendee in the API.
 * @package GoToMeeting\Models
 */

namespace GoToMeeting\Models;

use DateTime;
/**
 * Class Attendee
 *
 * @package GoToMeeting\Models
 */
class Attendee implements \JsonSerializable
{
    /**
     * @var string
     */
    private $firstName;
    
    private $joinTime;

    private $leaveTime;

    private $attendeeEmail;

    private $attendeeName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $groupName;

    /**
     * Constructor for an attendee.
     *
     * @param array $response optional response body data to populate model
     */
    public function __construct($response = array())
    {
        $this->parseFromJson($response);
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
     /**
     * @return string
     */
    public function getAttendeeName()
    {
        return $this->attendeeName;
    }
    
    /**
     * @return string
     */
    public function getAttendeeEmail()
    {
        return $this->attendeeEmail;
    }
    /**
     * @return string
     */
    public function getJoinTime()
    {
        return $this->joinTime;
    }
  /**
     * @return string
     */
    public function getLeaveTime()
    {
        return $this->leaveTime;
    }
    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }
    
    public function setAttendeeEmail($attendeeEmail)
    {
        $this->attendeeEmail = $attendeeEmail;
    }
    
    public function setAttendeeName($attendeeName){
    
        $this->attendeeName = $attendeeName;
    }
    
    public function setLeaveTime($leaveTime){
    
        $this->leaveTime = $leaveTime;
    }
    
    public function setJoinTime($joinTime){
        $this->joinTime = $joinTime;
    }


    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getGroupName()
    {
        return $this->groupName;
    }

    /**
     * @param string $groupName
     */
    public function setGroupName($groupName)
    {
        $this->groupName = $groupName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * Parse each known property into the model from an array of values.
     *
     * @param array $response
     */
    public function parseFromJson($response)
    {
        if (isset($response['firstName'])) {
            $this->setFirstName($response['firstName']);
        }
        if (isset($response['lastName'])) {
            $this->setLastName($response['lastName']);
        }
        if (isset($response['email'])) {
            $this->setEmail($response['email']);
        }
        if (isset($response['groupName'])) {
            $this->setGroupName($response['groupName']);
        }
        if (isset($response['leaveTime'])) {
            $this->setLeaveTime(new DateTime($response['leaveTime']));
        }
        if (isset($response['joinTime'])) {
            $this->setJoinTime(new DateTime($response['joinTime']));
        }
        if (isset($response['attendeeName'])) {
            $this->setAttendeeName($response['attendeeName']);
        }
        if (isset($response['attendeeEmail'])) {
            $this->setAttendeeEmail($response['attendeeEmail']);
        }
    }

    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}

