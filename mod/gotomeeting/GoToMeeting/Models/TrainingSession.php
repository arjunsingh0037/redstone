<?php
/**
 * Representation of an organizer in the API.
 * @package GoToMeeting\Models
 */

namespace GoToMeeting\Models;

/**
 * Class Organizer
 * @package GoToMeeting\Models
 */
class TrainingSession implements \JsonSerializable
{
    /**
     * @var array
     */
    private $sessionKey;

    /**
     * @var string
     */
    private $trainingName;

    /**
     * @var string
     */
    private $sessionStartTime;

    /**
     * @var string
     */
    private $sessionEndTime;
    
    /**
     * @var int
     */
    private $attendanceCount;
    
    /**
     * @var int
     */
    private $duration;
    
    /**
     * @var array of organizers
     */
    private $organizers;
    

    /**
     * Default constructor. Parse provided response from JSON.
     *
     * @param array $response optional parameter to pass in initial values (as if from a JSON response)
     */
    public function __construct($response = array())
    {
        $this->parseFromJson($response);
    }

    /**
     * @return string
     */
    public function getSessionKey()
    {
        return $this->sessionKey;
    }

    /**
     * @param string $sessionKey
     */
    public function setSessionKey($sessionKey)
    {
        $this->sessionKey = $sessionKey;
    }

    /**
     * @return string
     */
    public function getTrainingName()
    {
        return $this->trainingName;
    }

    /**
     * @param string $firstName
     */
    public function setTrainingName($trainingName)
    {
        $this->trainingName = $trainingName;
    }

    /**
     * @return int
     */
    public function getSessionStartTime()
    {
        return $this->sessionStartTime;
    }

    /**
     * @param int $groupKey
     */
    public function setSessionStartTime($sessionStartTime)
    {
        $this->sessionStartTime = $sessionStartTime;
    }

    /**
     * @return string
     */
    public function getSessionEndTime()
    {
        return $this->sessionEndTime;
    }

    /**
     * @param string $sessionEndTime
     */
    public function setSessionEndTime($sessionEndTime)
    {
        $this->sessionEndTime = $sessionEndTime;
    }

    /**
     * @return string
     */
    public function getAttendanceCount()
    {
        return $this->attendanceCount;
    }

    /**
     * @param int $attendanceCount
     */
    public function setAttendanceCount($attendanceCount)
    {
        $this->attendanceCount = $attendanceCount;
    }

    /**
     * @return int
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    /**
     * @return string
     */
    public function getOrganizers()
    {
        return $this->organizers;
    }

    /**
     * @param string $organizerKey
     */
    public function setOrganizers($organizers)
    {
        $this->organizers = $organizers;
    }

    /**
     * Parse each known property into the model from an array of values.
     *
     * @param array $response values from JSON representation of object
     */
    public function parseFromJson($response)
    {
        if (isset($response['sessionKey'])) {
            $this->setSessionKey($response['sessionKey']);
        }
        if (isset($response['firstName'])) {
            $this->setTrainingName($response['trainingName']);
        }
        if (isset($response['sessionStartTime'])) {
            $this->setSessionStartTime($response['sessionStartTime']);
        }
        if (isset($response['sessionEndTime'])) {
            $this->setSessionEndTime($response['sessionEndTime']);
        }
        if (isset($response['attendanceCount'])) {
            $this->setAttendanceCount($response['attendanceCount']);
        }
        if (isset($response['duration'])) {
            $this->setDuration($response['duration']);
        }
        if (isset($response['organizers'])) {
            $listoforgainzers = array();
            foreach($response['organizers'] as $organizer) {
                $listoforganizers[] = new CoOrganizers($organizer);
            }
            $this->setOrganizers($listoforganizers);
        }
    }

    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public function get_vars()
    {
        return get_object_vars($this);
    }
}
