<?php

namespace GoToMeeting\Models;

class Registration implements \JsonSerializable{

    private $givenName;
    private $surname;
    private $email;
    private $userid;
    private $status;
    private $joinUrl;
    private $confirmationUrl;
    private $registrantKey;
    private $trainingKey;
    private $registrationDate;

    public function getGivenName() {
        return $this->givenName;
    }

    public function getSurname() {
        return $this->surname;
    }

    public function getEmail() {
        return $this->email;
    }
    
    public function getUserid() {
        return $this->userid;
    }

    public function setUserid($userid){
        $this->userid = $userid;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getTrainingKey()
    {
        return $this->trainingKey;
    }


    public function getRegistrationDate()
    {
        $this->registrationDate;
    }

    public function getJoinUrl() {
        return $this->joinUrl;
    }
    
    public function getConfirmationUrl() {
        return $this->confirmationUrl;
    }
    
    public function getRegistrantKey() {
        return $this->registrantKey;
    }

    public function __construct($response = array()) {
        $this->parseFromJson($response);
    }

    public function setGivenName($givenName) {
        $this->givenName = $givenName;
    }

    public function setSurname($surname) {
        $this->surname = $surname;
    }

    public function setEmail($email) {
        $this->email = $email;
    }
   
    public function setStatus($status) {
        $this->status = $status;
    }

    public function setRegistrationDate($registrationDate) {
        $this->registrationDate = $registrationDate;
    }

    public function setJoinUrl($joinUrl) {
        $this->joinUrl = $joinUrl;
    }
    
    public function setTrainingKey($trainingkey) {
        $this->trainingKey = $trainingkey;
    }
    
    public function setRegistrantKey($registrantKey) {
        $this->registrantKey = $registrantKey;
    }
    
    public function setConfirmationUrl($confirmUrl) {
        $this->confirmationUrl = $confirmUrl;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
    public function parseFromJson($response)
    {
        if(isset($response['givenName']))
           $this->setGivenName($response['givenName']);
        
        if(isset($response['surName']))
           $this->setSurname($response['surname']);
        
        if(isset($response['email']))
           $this->setEmail($response['email']);
        
        if(isset($response['status']))
           $this->setStatus($response['status']);

        if(isset($response['joinUrl']))
           $this->setJoinUrl($response['joinUrl']);
        
        if(isset($response['registrationDate'])) {
            if (is_int($response['registrationDate'])) {
                $registrationDate = new \DateTime('now', new \DateTimeZone('UTC'));
                $startTime->setTimestamp($response['registrationDate'] / 1000);
            } else {
                $registrationDate = new \DateTime($response['registrationDate']);
            }
            $this->setRegistrationDate($registrationDate->getTimestamp());
        }
        
        if(isset($response['confirmationUrl']))
           $this->setConfirmationUrl($response['confirmationUrl']);

        if(isset($response['registrantKey']))
           $this->setRegistrantKey($response['registrantKey']);
        
        if(isset($response['trainingKey']))
           $this->setTrainingKey($response['trainingKey']);

    }

    public function toArrayForApi()
    {
        $registrant = array();
        $registrant['givenName'] = $this->givenName;
        $registrant['surname'] = $this->surname;
        $registrant['email'] = $this->email;
        return $registrant;
    }

    public function get_vars()
    {
        return get_object_vars($this);
    }
}
