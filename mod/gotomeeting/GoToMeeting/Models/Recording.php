<?php

namespace GoToMeeting\Models;

class Recording implements \JsonSerializable, \IteratorAggregate
{
    private $recordingId;
    private $name;
    private $description;
    private $registrationUrl;
    private $downloadUrl;
    private $startDate;
    private $endDate;
    private $trainingKey;
    
    public function __construct($response = array())
    {
        $this->parseFromJson($response);
    }

    public function getRecordingId()
    {
        return $this->recordingId;
    }
    
    public function getName()
    {
        return $this->name;
    }
   
    public function getDescription()
    {
        return $this->description;
    }
    
    public function getRegistrationUrl()
    {
        return $this->registrationUrl;
    }
    
    public function getDownloadUrl()
    {
        return $this->downloadUrl;
    }
    
    public function getStartDate()
    {
        return $this->startDate;
    }
    
    public function getEndDate()
    {
        return $this->endDate;
    }
    
    public function getTrainingKey()
    {
        return $this->trainingKey;
    }


    public function setRecordingId($recordingId)
    {
        $this->recordingId = $recordingId;
    }
    
    public function setName($name)
    {
        $this->name = $name;
    }
    
    public function setDescription($description)
    {
        $this->description = $description;
    }
    
    public function setRegistrationUrl($url)
    {
        $this->registrationUrl = $url;
    }
    
    public function setDownloadUrl($downloadUrl)
    {
        $this->downloadUrl =$downloadUrl;
    }
    
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate; 
    }

    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;    
    }
    
    public function setTrainingKey($trainingKey)
    {
        $this->trainingKey = $trainingKey;
    }

    public function parseFromJson($response)
    {
        if(isset($response['recordingId']))
            $this->setRecordingId((int)$response['recordingId']);

        if(isset($response['name']))
            $this->setName($response['name']);
        
        if(isset($response['description']))
            $this->setDescription($response['description']);
        
        if(isset($response['registrationUrl']))
            $this->setRegistrationUrl($response['registrationUrl']);

        if(isset($response['downloadUrl']))
            $this->setDownloadUrl($response['downloadUrl']);
        
        if(isset($response['startDate'])) {
            if (is_int($response['startDate'])) {
                $startDate = new \DateTime('now', new \DateTimeZone('UTC'));
                $startDate->setTimestamp($response['startDate'] / 1000);
            } else {
                $startDate = new \DateTime($response['startDate']);
            }
            $this->setStartDate($startDate);
        }

        if(isset($response['trainingKey']))
            $this->setTrainingKey($response['trainingKey']);

        if (isset($response['endDate'])) {
            if (is_int($response['endDate'])) {
                $endTime = new \DateTime('now', new \DateTimeZone('UTC'));
                $endTime->setTimestamp($response['endDate'] / 1000);
            } else {
                $endTime = new \DateTime($response['endDate']);
            }
            $this->setEndDate($endTime);
        }
    }        
    
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public function getIterator() {
        return new \ArrayIterator($this);
    }
    
    public function get_vars()
    {
        return get_object_vars($this);
    }
}
