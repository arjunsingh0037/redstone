<?php

namespace GoToMeeting\Exception;

class AccessDeniedException extends \moodle_exception {

    public function __construct() {
        parent::__construct('Could not access services, please check account info!!');
    }
}
