<?php

namespace GoToMeeting\Exception;

class InvalidRequestException extends \moodle_exception {

    public function __construct() {
        parent::__construct('Your request could not be processed because it is invalid');
    }
}
