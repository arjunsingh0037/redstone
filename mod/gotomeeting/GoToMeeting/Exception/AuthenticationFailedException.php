<?php

namespace GoToMeeting\Exception;

class AuthenticationFailedException extends \moodle_exception {

    public function __construct() {
        parent::__construct('Unable to access account, invalid Username/Password !!');
    }
}
