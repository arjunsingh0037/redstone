<?php


require 'vendor/autoload.php';

spl_autoload_register(function ($relative_class) {
    $base_dir = __DIR__.'/';
    $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
    if (file_exists($file)) {
        require $file;
    }
});

/**
 * Citrix Date format class
 */
class ApiDateFormat extends DateTime
{
    /**
     * Default constructor
     *
     * @param int timestamp
     * @param string timezone
     */
    public function __construct($timestamp, $timezone)
    {
        parent::__construct('', new DateTimeZone($timezone));
        $this->setTimestamp($timestamp);
    }

    /**
     * Method return date format in string
     *
     * @return string
     */
    public function __toString()
    {
        return $this->format('Y-m-d\ H:i:s.u');
    }
}