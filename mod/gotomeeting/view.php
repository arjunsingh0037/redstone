<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints a particular instance of gotomeeting
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_gotomeeting
 * @copyright  2015 Your Name
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// Replace gotomeeting with the name of your module and remove this line.

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');

$id = optional_param('id', 0, PARAM_INT); // Course_module ID, or
$n  = optional_param('n', 0, PARAM_INT);  // ... gotomeeting instance ID 
if ($id) {
    $cm         = get_coursemodule_from_id('gotomeeting', $id, 0, false, MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $gotomeeting  = $DB->get_record('gotomeeting', array('id' => $cm->instance), '*', MUST_EXIST);
    $type =  $DB->get_record('goto_'.$gotomeeting->servicetype, array('instance' => $cm->instance), '*', MUST_EXIST);
} else if ($n) {
    $gotomeeting  = $DB->get_record('gotomeeting', array('id' => $n), '*', MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $gotomeeting->course), '*', MUST_EXIST);
    $cm         = get_coursemodule_from_instance('gotomeeting', $gotomeeting->id, $course->id, false, MUST_EXIST);
} else {
    error('You must specify a course_module ID or an instance ID');
}

require_login($course, true, $cm);

$event = \mod_gotomeeting\event\course_module_viewed::create(array(
    'objectid' => $PAGE->cm->instance,
    'context' => $PAGE->context,
));
$event->add_record_snapshot('course', $PAGE->course);
$event->add_record_snapshot($PAGE->cm->modname, $gotomeeting);
$event->trigger();

// Print the page header.

$PAGE->set_url('/mod/gotomeeting/view.php', array('id' => $cm->id));
$PAGE->set_title(format_string($gotomeeting->name));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->requires->jquery();
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/mod/gotomeeting/module.js'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/mod/gotomeeting/js/jquery.dataTables.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/mod/gotomeeting/js/dataTables.fixedColumns.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/mod/gotomeeting/js/dataTables.fixedHeader.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/mod/gotomeeting/js/buttons.html5.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/mod/gotomeeting/js/buttons.flash.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/mod/gotomeeting/js/buttons.print.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/mod/gotomeeting/js/dataTables.buttons.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/mod/gotomeeting/js/dataTables.responsive.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/mod/gotomeeting/js/jszip.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/mod/gotomeeting/js/pdfmake.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/mod/gotomeeting/js/vfs_fonts.js'), true);
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/mod/gotomeeting/js/jquery.dataTables.min.css'));
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/mod/gotomeeting/js/buttons.dataTables.min.css'));
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/mod/gotomeeting/js/fixedHeader.dataTables.min.css'));
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/mod/gotomeeting/js/responsive.dataTables.min.css'));
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/mod/gotomeeting/styles.css'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/mod/gotomeeting/scripts.js'));
/*
 * Other things you may want to set - remove if not needed.
 * $PAGE->set_cacheable(false);
 * $PAGE->set_focuscontrol('some-html-id');
 * $PAGE->add_body_class('gotomeeting-'.$somevar);
 */

// Output starts here.
echo $OUTPUT->header();


// Replace the following lines with you own code.
//echo $OUTPUT->heading($type->subject, '3');

$class = $gotomeeting->servicetype.'_mod_service';
$modservice  =  new $class($gotomeeting, $type, $course, $cm);

echo $modservice->render();
echo $OUTPUT->footer();
