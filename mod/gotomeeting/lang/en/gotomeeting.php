<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * English strings for gotomeeting
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_gotomeeting
 * @copyright  2015 Your Name
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['modulename'] = 'GotoMeeting';
$string['modulenameplural'] = 'gotomeetings';
$string['modulename_help'] = 'Use the gotomeeting module for... | The gotomeeting module allows...';
$string['gotomeetingfieldset'] = 'Custom example fieldset';
$string['gotomeetingname'] = 'Subject';
$string['gotomeetingname_help'] = 'This is the content of the help tooltip associated with the gotomeetingname field. Markdown syntax is supported.';
$string['gotomeeting'] = 'GotoMeeting';
$string['pluginadministration'] = 'gotomeeting administration';
$string['pluginname'] = 'GotoMeeting';
$string['sessionmoderator'] = 'Start meeting as Moderator';
$string['sessionvisitor'] = 'Start meeting as Visitor';

$string['maxparticipants'] = 'Max participants';
$string['duration'] = 'Duration';
$string['meetingtype'] = 'Meeting type';

$string['meeting_key'] = 'Meeting key';
$string['webinar_key'] = 'Webinar key';
$string['training_key'] = 'Training key';
$string['useremail'] = 'Account email';
$string['userpassword'] = 'Account password';

$string['startasmoderator'] = 'Start meeting as Moderator';
$string['startasmoderatorinwebinar'] = 'Start webinar as Moderator';
$string['startasaprticipant'] = 'Start meeting as Visitor';
$string['joinwebinar'] = 'Join GotoWebinar';
$string['joinmeeting'] = 'Join GotoMeeting';
$string['jointraining'] = 'Join GoToTraining';
$string['coorganizers'] = 'Co-Organizers';
$string['coorganizer'] = 'Co-Organizer';
$string['organizers'] = 'Organizers';
$string['home'] = 'Home';
$string['participants'] = 'Participants';
$string['addcoorganizer'] = 'Add CoOrganizers in this webinar';
$string['registerwebinar'] = 'Register webinar as Participant';
$string['registermeeting'] = 'Start meeting as Moderator';
$string['registertraining'] = 'Register for GotoTraining';
$string['registrations'] = 'Registrations';
$string[''] = 'Start meeting as Moderator';
$string['disableconfirmationemail'] = 'Disable Confirmation email';
$string['disablewebregistration'] = 'Disable web registration';
$string['recordings'] = 'Recordings';
$string['attendees'] = 'Attendees';
$string['starttraining'] = 'Start training';
$string['error_starttime'] = 'To date time is less then from date time';
$string['error_maxtime'] = 'From time must within 24 hours';
$string['error_earlytime'] = '{$a} must change after 24 hours';
$string['coorganizeralreadyexists'] = 'Coorganizer already exists';
$string['registrant'] = 'Registrant';
$string['noattendeeinsession'] = 'No attendees in this webinar';
$string['meetingtype'] = 'Meeting type';
$string['norecordingsfound'] = 'No recording found';
$string['noattendeefoundinmeeting'] = 'No Attendee found in meeting';
$string['notstarted'] = '{$a->service} start within <strong>{$a->time}</strong> ';
$string['finished'] = '{$a} finished';
$string['insession'] = '{$a} in Session';
$string['yetnotstarted'] = '{$a} yet not started';
$string['notrainingregistration'] = 'Training has no registration';
$string['servicenotavailable'] = 'Service not available, please contact site administrator';
$string['notattendfoundintraining'] = 'No attendee present in training';
$string['servicetype'] = 'Service type';


/*
 * Dummy text
 */

