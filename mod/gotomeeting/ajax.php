<?php

require dirname(dirname(dirname(__FILE__))).'/config.php';

include_once 'autoload.php';
use GoToMeeting\Models\CoOrganizers;
use GoToMeeting\Models\Registrant;
use GoToMeeting\Models\Registration;
use GoToMeeting\Services\ServiceFactory;
use GoToMeeting\Services\Webinar\WebinarService;
use GoToMeeting\Services\Meeting\MeetingService;
use GoToMeeting\Services\Training\TrainingService;
use GoToMeeting\Services\Training\TrainingReportService;
use GoToMeeting\Services\Training\RegistrationService;
use GoToMeeting\Services\Training\RecordingsService;
use GoToMeeting\Services\Webinar\RegistrantsService;
use GoToMeeting\Services\Webinar\CoOrganizerService;
use GoToMeeting\Exception\InvalidRequestException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
$params = array();
require_login();
foreach ($_REQUEST as $field => $value) {
	$params[$field] = $value;
}
unset($params['callback']);
$result = array();
try {
	if(isset($_REQUEST['callback'])) {
		// callable method name
		$callback = $_REQUEST['callback'];
		if(!function_exists($callback)) {
			throw new Exception("Undefined method access.", 1);	
		}
	
	}

	$callback = $_REQUEST['callback'];
	$result = call_user_func_array($callback, $params);
} catch (GuzzleHttp\Exception\ClientException $e) {
	//throw $e;
	$result['status'] = false;
	$response = $e->getResponse();
    $error = json_decode($response->getBody()->getContents(), true);
	$result['error'] =  $error['description'];
} catch (ConnectException $ex) {
    //throw $ex;
	$result['status'] = false;
    $result['error'] = 'Could not coneect Citrix api server';
} catch (exception $e) {
	$result['status'] = false;
	$result['error'] = $e->getMessage();
    //throw $e;
}
echo json_encode($result);
die();


/*
 |------------------------------------------
 | Webinar functions
 |------------------------------------------
 */

function addCoorganizerInWebinar($webinarkey)
{
	global $USER, $DB;
    $flag = $DB->record_exists('goto_coorganizers', array('email' => $USER->email, 'servicekey' => $webinarkey));
    if($flag) {
        throw new moodle_exception(get_string('coorganizeralreadyexists', 'mod_gotomeeting'));
    }
	$service = ServiceFactory::getInstance(CoOrganizerService::class);
	// instantiate CoOrganizer
	$coorganizer = new CoOrganizers();
	$coorganizer->setExternal(true);
	$coorganizer->setOrganizerKey($service->getClient()->getAuth()->getOrganizerKey());
	$coorganizer->setGivenName($USER->firstname.' '.$USER->lastname);
	$coorganizer->setEmail($USER->email);
	$coorganizer->setWebinarKey($webinarkey);
	$result = $service->createCoOrganizers($coorganizer, $webinarkey);
	
	$insert = new stdClass();
	$insert->givenname = $USER->firstname.' '.$USER->lastname;
	$insert->external = 1;
	$insert->surname = '';
	$insert->email = $USER->email;
    $insert->servicekey = $webinarkey;
    $insert->memberkey = $result->getMemberKey();
    $insert->registrantkey = $result->getRegistrantKey();
    $insert->joinlink = $result->getJoinLink();
	$insert->userid = $USER->id;
	$insert->timecreated = time();
	$DB->insert_record('goto_coorganizers', $insert);
	return array('status' => true);
}

function getWebinarCoorganizers($webinarkey)
{
	$service = ServiceFactory::getInstance(CoOrganizerService::class);
	$coorganizers = $service->getCoOrganizers($webinarkey);
    if(count($coorganizers) > 0) {
        return array('status' => true, 'coorganizers' => $coorganizers);
    } else {
        return array('status' => false, 'message' => '');
    }
}

function getWebinarRegistrant($webinarkey)
{
	$service = ServiceFactory::getInstance(RegistrantsService::class);
	$coorganizers = $service->getRegistrants($webinarkey);
    if(count($coorganizers) > 0) {
        return array('status' => true, 'registrants' => $coorganizers);
    } else {
        return array('status' => false, 'error' => '');
    }
}


function getAttendeesForAllWebinarSessions($webinarkey)
{
    $service = ServiceFactory::getinstance(webinarservice::class);
	$sessions = $service->getattendeesforallwebinarsessions($webinarkey);
    if(count($sessions) > 0) {
        return array('status' => true, 'attendees' => $sessions);
    } else {
        return array('status' => false, 'error' => get_string('noattendeeinsession', 'mod_gotomeeting'));
    }
}




function regiserAsParticipantsInWebinar($webinarkey)
{
	global $USER,$DB;
	$flag = $DB->record_exists('goto_registrant', array('userid' => $USER->id, 'webinarkey' => $webinarkey));
    if($flag) {
        throw new moodle_exception(get_string('registrantalreadyexists', 'mod_gotomeeting'));
    }


	$service = ServiceFactory::getInstance(RegistrantsService::class);
	$registraint = new Registrant();
	$registraint->setFirstName($USER->firstname);
	$registraint->setLastname($USER->lastname);
	$registraint->setEmail($USER->email);
	$reg = $service->createRegistrant($webinarkey, $registraint);
	// Insert into database
	$insert = new stdClass();
	$insert->webinarkey = $webinarkey;
	$insert->registrantkey = $reg->getRegistrantKey();
	$insert->status = 'APPROVED';
	$insert->joinurl = $reg->getJoinUrl();
	$insert->userid = $USER->id;
	$insert->timecreated = time();

	$DB->insert_record('goto_registrant', $insert);
	return array("status" => true);
}

/*
 |------------------------------
 | Meeting functions
 |------------------------------
 */
function getAttendeesByMeeting($meetingid) {
    $service = ServiceFactory::getInstance(MeetingService::class);
	$attendees = $service->getAttendeesByMeeting($meetingid);
    if(count($attendees) > 0) {
        return array("status" => true, 'attendees' => $attendess);
    }

    return array("status" => false, "message" => '');
}

/*
 |-----------------------------
 | Webinar funcitons
 |-----------------------------
 */

function regiserAsParticipantsInTraining($trainingkey)
{
	global $USER,$DB;
    
    $service = ServiceFactory::getInstance(RegistrationService::class);
	$register = new Registration();
	$register->setGivenName($USER->firstname);
	$register->setSurname($USER->lastname);
	$register->setEmail($USER->email);
    $register->setTrainingKey($trainingkey);
	try { 
        // Call Training server
        $registration = $service->registerForTraining($register);

        // Confirm account
        ob_start();
        $ch = curl_init($registration->getConfirmationUrl());
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        curl_close($ch);
        ob_clean();
        $registration->setGivenName($USER->firstname);
	    $registration->setSurname($USER->lastname);
	    $registration->setEmail($USER->email);
	    $registration->setStatus('APPROVED');
	    $registration->setUserid($USER->id);
	    $registration->setRegistrationDate(time());
        $registration->setTrainingKey($trainingkey);
        // Save data
        $DB->insert_record('goto_registrations', $registration->get_vars());
        return array("status" => true);
    } catch(ClientException $ex) {
        //throw new InvalidRequestException(); 
        $error = json_decode($ex->getResponse()->getBody(true), true);
        throw new moodle_exception($error['description']);
    }
}
/*
 |-------------------------------------
 | We cann't add organizer in training
 |-------------------------------------
 */
function registerAsOrganizerInTraining($trainingkey)
{
    global $USER,$DB;
	// Find organizers present in training
    $coorganizers = $DB->get_records('goto_coorganizers', array('serviceKey' => $trainingkey), 'memberkey');
    $organizer = array();
    foreach($coorganizers as $org) {
        $organizer[] = $org->memberkey;
    }
    // Add new organizer for this user if not exists
    $userorg = $DB->get_record('goto_coorganizers', array(  'userid' => $USER->id));
    if(!$userorg) {
        $service = ServiceFactory::getInstance(CoOrganizerService::class);
        //$service->createOrganizer
    }
	$service = ServiceFactory::getInstance(TrainingService::class);
    $updateorganizer = array('organizers' => $organizer, 'notifyOrganizers'=> true);
    $service->updateTrainingOrganizers($updateorganizer);
}

function getRegistrationsOfTraining()
{
    $service = ServiceFactory::getInstance(RegistrationsService::class);
	return $service->getattendeesforallwebinarsessions($webinarkey);
}


function getTrainingAttendanceDetails($trainingkey)
{
	$service = ServiceFactory::getInstance(RegistrationService::class);
	$registraint = new Registrations();
	$registraint->setGivenName($USER->firstname);
	$registraint->setSurname($USER->lastname);
	$registraint->setEmail($USER->email);
	$response = $service->createRegistrations($training, $registraint);
	// Insert into database
	$response['trainingkey'] = $webinarkey;
	$response['userid'] = $USER->id;
	$response['timecreated'] = time();

	$DB->insert_record('goto_registrations', (object) $response);
    return '';
}
function getMeetingAttendees($meetingid)
{
    global $DB;

    $attendees = $DB->get_records('goto_meeting_attendees', array('meetingid' => $meetingid));
    if(!$attendees) {
        try {
            $service = ServiceFactory::getInstance(MeetingService::class);
	        $attendees = $service->getAttendeesByMeeting($meetingid);
        } catch (ClientException $e) {
            throw new exception(get_string('noattendeefoundinmeeting', 'mod_gotomeeting'));
        }
        // Persist into database
        if(count($attendees) > 0) {
            $dataobject = array();
            foreach($attendees as $attendee) {
                $std = new stdClass();
                $std->meetingid = $meetingid;
                $std->attendeename = $attendee->getAttendeeName();
                $std->attendeeemail = $attendee->getAttendeeEmail();
                $std->jointime = $attendee->getJoinTime()->getTimestamp();
                $std->leavetime = $attendee->getLeaveTime()->getTimestamp();
                $dataobject[] = $std;
            }
            try {
                $transaction = $DB->start_delegated_transaction();
                $DB->insert_records('goto_meeting_attendees', $dataobject);
                $transaction->allow_commit();
                $attendees = $DB->get_records('goto_meeting_attendees', array('meetingid' => $meetingid));
            } catch(Exception $e) {
                $transaction->rollback($e);
                return $e;
            }
        }
    }
    
    if(count($attendees) > 0) {
         array_map(function($array) {
            $array->jointime = userdate($array->jointime, get_string('strftimedatetimeshort'));
            $array->leavetime = userdate($array->leavetime, get_string('strftimedatetimeshort'));
        }, $attendees);
        
        $response = array(
            "status" => true,
            'attendees' => $attendees
        );
    } else {
        $response = array(
            "status" => false,
            'error' => get_string('noattendeefoundinmeeting', 'mod_gotomeeting')
        );
    }

    return $response;
}

function getTrainingRecording($trainingkey)
{
    global $DB;

    $recordings = $DB->get_records('goto_recording', array('trainingkey' => $trainingkey));
    if(!$recordings) {
        $service = ServiceFactory::getInstance(RecordingsService::class);
	    $recordings = $service->getOnlineRecordingsforTraining($trainingkey);

        // Persist into database
        if(count($recordings) > 0) {
            $dataobject = array();
            foreach($recordings as $recording) {
                $std = new stdClass();
		$std->recordingid = $recording->getRecordingId();
		$std->name = $recording->getName();
		$std->description = $recording->getDescription();
		$std->registrationurl = $recording->getRegistrationUrl();
		$std->downloadurl = $recording->getDownloadUrl();
		$std->startdate = $recording->getStartDate()->getTimestamp();
		$std->enddate = $recording->getEndDate()->getTimestamp();
                $std->trainingkey = $trainingkey;
		$std->timecreated = time();
                $dataobject[] = $std;
            }
            try {
                $transaction = $DB->start_delegated_transaction();
                $DB->insert_records('goto_recording', $dataobject);
		$recordings = $dataobject;
                $transaction->allow_commit();
            } catch(Exception $e) {
                $transaction->rollback($e);
                return $e;
            }
        }
    }
    
    if(count($recordings) > 0) {
        array_map(function($array) {
            $array->startdate = userdate($array->startdate, get_string('strftimedatetimeshort'));
            $array->enddate = userdate($array->enddate, get_string('strftimedatetimeshort'));
        }, $recordings);
        
        
        $response = array(
            'status' => true,
            'recording' => $recordings
        );
    } else {
        $response = array(
            'status' => false,
            'error' => get_string('norecordingsfound', 'mod_gotomeeting')
        );
    }

    return $response;
}
function getTrainingAttendees($trainingkey)
{
    global $DB;

    $attendees = $DB->get_records('goto_training_attendees', array('trainingkey' => $trainingkey));
    if(!$attendees) {
        $service = ServiceFactory::getInstance(TrainingReportService::class);
	    $sessions = $service->getSessionsByTraining($trainingkey);
        // Persist into database
        if(count($sessions) > 0) {
            // Make call ti find attendee
            $results = array();
            foreach($sessions as $session) {
               $results[] = $service->getAttendanceDetails($session->getSessionKey());
            }
            $attendees =array();
            if(count($results)) {
                foreach($results as $result) {
                    $insert->givenname = $result['givenName'];
                    $insert->surname = $result['surname'];
                    $insert->email = $result['email'];
                    $insert->timeinsession = $result['timeInSession'];
                    $insert->timecreated = time();
                    $attendees[] = $insert;
                }
                try {
                    $transaction = $DB->start_delegated_transaction();
                    $DB->insert_records('goto_training_attendees', $dataobject);
                    $transaction->allow_commit();
                } catch(Exception $e) {
                    $transaction->rollback($e);
                    return $e;
                }
            }
        }
    }
    
    if(count($attendees ) > 0) {
        $response = array(
            "status" => true,
            'recording' => $attendees
        );
    } else {
        $response = array(
            "status" => false,
            'error' => get_string('notattendfoundintraining', 'mod_gotomeeting')
        );
    }

    return $response;
}



function getTrainingOrganizers($trainingid)
{
    $service = ServiceFactory::getInstance(TrainingService::class);
	$organizer = $service->getTrainingOrganizers($trainingid);
    return array("status" => true, 'organizer' => $organizer);
}

function getTrainingRegistrations($trainingkey)
{
    global $DB; 
        $service = ServiceFactory::getInstance(RegistrationService::class);
        $registrations = $service->getTrainingRegistrants($trainingkey);
    
    if(count($registrations) > 0) {
        $response = array(
            "status" => true,
            'registration' => $registrations
        );
    } else {
        $response = array(
            "status" => false,
            'error' => get_string('notrainingregistration', 'mod_gotomeeting')
        );
    }

    return $response;
}

function coorganizer_in_training($trainigkey)
{
	global $USER, $DB;
	return array('status' => true);
}
