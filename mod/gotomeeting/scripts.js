
function generate_form(service)
{
	var content;
	if(service == "meeting") {
	 	content = '<div id="fitem_bigbluebuttonbn_participant_selection" class="fitem fitem_fselect">'+
                    '<div class="fitemtitle">'+
                        '<label for="bigbluebuttonbn_participant_selectiontype">Meeting Type</label>'+
                    '</div>'+
                    '<div class="felement fselect">'+
                        '<select id="bigbluebuttonbn_participant_selection_type" onchange="disabledate()">'+
                            '<option value="immediate">Immediate</option>'+
                            '<option value="scheduled" selected="selected">Scheduled</option>'+
                            '<option value="recurring">Recurring</option>'+
                        '</select>'+
                    '</div>'+
                '</div>'+
                '<div id="fitem_id_duration" class="fitem fitem_ftext ">'+
                    '<div class="fitemtitle"><label for="id_duration">Duration </label></div>'+
                        '<div class="felement ftext">'+
                            '<input name="duration" type="text" id="id_duration"/>'+
                        '</div>'+
                    '</div>'+
                    '<div id="fitem_id_maxparticipants" class="fitem fitem_ftext ">'+
                        '<div class="fitemtitle">'+
                            '<label for="id_maxparticipants">Max participants </label>'+
                        '</div>'+
                    '<div class="felement ftext">'+
                    '<input name="maxparticipants" type="text" id="id_maxparticipants">'+
                    '</div>'+
                '</div>';
    } else if(service == "webinar") {
        content = '';

    } else if(service == "training") {
       	content = '';
    }
    document.getElementById("generate_form").innerHTML = content;
}

