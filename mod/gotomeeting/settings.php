<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * gotomeeting module admin settings and defaults
 *
 * @package    mod_gotomeeting
 * @copyright  2009 Petr Skoda  {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {

	
    //--- Meeting setting -----------------------------------------------------------------------------------

 	$settings->add(new admin_setting_heading('gotomeeting/meeting', 'Goto Meeting', ''));
	$settings->add(new admin_setting_configtext('gotomeeting/meeting_email', get_string('useremail', 'gotomeeting'), '', '', PARAM_TEXT));
	$settings->add(new admin_setting_configpasswordunmask('gotomeeting/meeting_password', 
		get_string('userpassword', 'gotomeeting'), ' ', '', PARAM_TEXT));
    $settings->add(new admin_setting_configpasswordunmask('gotomeeting/meeting_key',
    	get_string('meeting_key', 'gotomeeting'),' ', '',PARAM_RAW));
    
    //--- Weinar setting -----------------------------------------------------------------------------------
 	$settings->add(new admin_setting_heading('gotomeeting/webinar', 'Goto Webinar', ''));

	$settings->add(new admin_setting_configtext('gotomeeting/webinar_email', get_string('useremail', 'gotomeeting'), '', '', PARAM_TEXT));
	$settings->add(new admin_setting_configpasswordunmask('gotomeeting/webinar_password', 
		get_string('userpassword', 'gotomeeting'), ' ', '', PARAM_TEXT));
    $settings->add(new admin_setting_configpasswordunmask('gotomeeting/webinar_key',
    	get_string('webinar_key', 'gotomeeting'), ' ', '', PARAM_RAW));
   
    //--- Training setting -----------------------------------------------------------------------------------
 	$settings->add(new admin_setting_heading('gotomeeting/training', 'Goto Training', ''));

	$settings->add(new admin_setting_configtext('gotomeeting/training_email', get_string('useremail', 'gotomeeting'), '', '', PARAM_TEXT));
	$settings->add(new admin_setting_configpasswordunmask('gotomeeting/training_password', 
		get_string('userpassword', 'gotomeeting'), ' ', '', PARAM_TEXT));
     $settings->add(new admin_setting_configpasswordunmask('gotomeeting/training_key',
     	get_string('training_key', 'gotomeeting'), ' ', '', PARAM_RAW));
}