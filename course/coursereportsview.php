<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Course reports view with users enrolled in it
 *
 * @copyright 1999 Martin Dougiamas  http://dougiamas.com
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package course
 */

require_once("../config.php");
require_once($CFG->dirroot. '/course/lib.php');
require_once($CFG->libdir. '/coursecatlib.php');
require_once($CFG->dirroot. '/enrol/locallib.php');
$site = get_site();
$PAGE->set_url('/course/index.php');
$PAGE->set_context(context_system::instance());
$PAGE->set_title(get_string('coursereports','admin'));
$PAGE->set_heading(get_string('coursereports','admin'));
$PAGE->set_pagelayout('admin');
$PAGE->navbar->add('Course reports');

$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/course/js/jquery.dataTables.min.css'), true);
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/course/js/buttons.dataTables.min.css'), true);	
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/course/js/jquery-1.12.3.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/course/js/jquery-1.12.0.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/course/js/jquery.dataTables.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/course/js/dataTables.fixedColumns.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/course/js/buttons.html5.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/course/js/buttons.flash.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/course/js/buttons.print.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/course/js/dataTables.buttons.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/course/js/jszip.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/course/js/pdfmake.min.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/course/js/vfs_fonts.js'), true);
require_login();

$content = '';

$table = new html_table();

$table->id = "example";
$table->attributes = array('class' => 'table table-striped table-bordered table-hover');
$table->head = array('Sl.no', 'Course Name', 'Category Name', 'User Name', 'Start Date','Expiry Date');

$courses = get_courses();
unset($courses[1]);
$catlist = coursecat::make_categories_list();

$userenrolid = array();
$usercourses = array();
$users = $DB->get_records('user',array());
foreach ($users as $key => $user) {
		static $i = 1;
		$userenrolid = $DB->get_records('user_enrolments',array('userid'=>$user->id),'id,enrolid,timestart,timeend');
		foreach ($userenrolid as $key => $enrolid) {
		 	$usercourses = $DB->get_record('enrol',array('id'=>$enrolid->enrolid));
		 	$coursedetail = $DB->get_record('course',array('id'=>$usercourses->courseid),'fullname,category');
			$cat_name = $catlist[$coursedetail->category];
		 	$username = $user->firstname.' '.$user->lastname;
		 	$startdate = userdate($enrolid->timestart);
			if(($enrolid->timeend) > '0'){
					$expirydate = userdate($enrolid->timeend);
			}else{$expirydate = 'Not set';}
			$table->data[] = array(
		            $i++,
		            $coursedetail->fullname,
		            $cat_name,
		            $username,
		            $startdate,
		            $expirydate
		    );
		}
}
$content .= html_writer::div('Course Delivery Report', 'lead');
$content .= html_writer::empty_tag('hr');
$content .= html_writer::div(html_writer::table($table));
$content .= html_writer::empty_tag('hr');

echo $OUTPUT->header();
echo $OUTPUT->skip_link_target();
echo $content;
echo '<script type="text/javascript">
        $(document).ready(function() {
            $("#example").DataTable( {
                scrollX:        true,
                dom: "Bfrtip",
                buttons: [
                          "copy", "csv", "excel", "pdf", "print"
               ]
            } );
        });
        </script>';
echo $OUTPUT->footer();
