<?php

require_once(dirname(__FILE__) . '/header.php');

?>

<div id="breadwrap" class="clearfix"><div class="container">
 <?php echo $OUTPUT->page_heading(); ?>
 <div id="page-navbar" class="clearfix">
            <nav class="breadcrumb-button"><?php echo $OUTPUT->page_heading_button(); ?></nav>
            <?php echo $OUTPUT->navbar(); ?>
 </div>
</div></div>

<?php
function last_scorm_course(){
     global $DB,$CFG, $COURSE, $USER;

     $now = time();

     $statatq28s = "SELECT contextinstanceid from {logstore_standard_log} where userid = $USER->id and timecreated = (SELECT max(timecreated) from {logstore_standard_log} 
     where userid = $USER->id and courseid = $COURSE->id and courseid != 0 and action='viewed' and target ='course_module' and component='mod_scorm' and objecttable='scorm')";
          //var_dump($lastrateq);
     $statatqrs = $DB->get_field_sql($statatq28s);
         
          if ($statatqrs) {
          $rs = $statatqrs;
          } else {
          $rs = NULL;
          }

          return $rs;
    
    }
?>

<?php
    // this is to redirect normal student to last accessed scorm

    global $DB,$CFG, $COURSE, $USER;
    // addded by arjun for category display for useradmin for redstone learning  --starts.
    $flag = 0;
    $linkurl ='';
    if (!(is_siteadmin())) {
      $roleids = $DB->get_records_sql('SELECT * FROM {role} WHERE shortname = ? OR shortname = ?', array( 'catadmin' , 'useradmin'));
      foreach ($roleids as $key => $roles) {
          $assignedroles = $DB->get_record('role_assignments',array('userid' => $USER->id,'roleid' => $roles->id),'id,contextid');
          if(!empty($assignedroles)){
            $flag = 1;
          }
      }
      if($flag == 1)
      {

      }else{
        $checklastscorm = last_scorm_course();
        if ($checklastscorm) {
            redirect(new moodle_url('/mod/scorm/view.php?id='.$checklastscorm));
        }else {
            // else we will redirect them to the first scorm becasue this is the first time they are coming
            $course=$COURSE->id;
            $modinfo = get_fast_modinfo($course);
            $cm = $modinfo->get_cms();
                //print_r($cm);
                foreach($cm as $cm2) {
                    if ($cm2->modname == 'scorm' or $cm2->modname=='quiz' or $cm2->modname=='resource') {
                        $linkurl = '/mod/'.$cm2->modname.'/view.php?id='.$cm2->id;
                        break;
                    }
                }
            redirect(new moodle_url($linkurl));
        }
      }
    }
    // addded by arjun for category display for useradmin for redstone learning  --ends.
?>


   <div id="page-content" class="row-fluid">
    	<div id="padder" class="clearfix">
        <section id="region-main" class="span12">
            <?php
            echo $OUTPUT->course_content_header();


            echo $OUTPUT->main_content();
            echo $OUTPUT->course_content_footer();
            ?>
        </section>
        </div>
    </div>

</div>


<?php require_once(dirname(__FILE__) . '/footer.php'); ?>
