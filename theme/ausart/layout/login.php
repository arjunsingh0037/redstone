<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * ausart theme with the underlying Bootstrap theme.
 *
 * @package    theme
 * @subpackage ausart
 * @copyright  &copy; 2014-onwards G J Barnard in respect to modifications of the Bootstrap theme.
 * @author     G J Barnard - gjbarnard at gmail dot com and {@link http://moodle.org/user/profile.php?id=442195}
 * @author     Based on code originally written by Bas Brands, David Scotson and many other contributors.
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/**
 * This layout file is designed maintenance related tasks such as upgrade and installation of plugins.
 *
 * It's ultra important that this layout file makes no use of API's unless it absolutely needs to.
 * Under no circumstances should it use API calls that result in database or cache interaction.
 *
 * If you are modifying this file please be extremely careful, one wrong API call and you could end up
 * breaking installation or upgrade unwittingly.
 */

$loggedin = isloggedin();
if (isloggedin() && !isguestuser()) {
   // redirect($CFG->wwwroot);
}
if (empty($CFG->authloginviaemail)) {
    $strusername = get_string('username');
} else {
    $strusername = get_string('usernameemail');
}
echo $OUTPUT->standard_head_html();
$PAGE->requires->js('/theme/ausart/javascript/bootstrap.min.js');

$settingshtml->additionalbodyclasses[] = 'loginpageimages';
echo $OUTPUT->doctype() 
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <style>
        body { 
        background: url("<?php echo $CFG->wwwroot.'/theme/ausart/pix/banner.jpg' ;?>") 70% 13%;   
        padding-top:0px!important;
        height:100%;
       /* background-repeat: no-repeat;*/
        }
        .title {
           
            color: #FFF;
            font-size: 320%;
            font-weight: 900;
            position: relative;
            text-transform: uppercase;
            width: 100%;
            opacity:0.7;
        }
        .logdivpd{
            padding-bottom: 25px;
            padding-top: 25px;
        }
        .logheader{
            text-align: center;
            background-color: #00a65a;
            margin-bottom: 0px;
            padding-top: 25px;
            padding-bottom: 25px;
           
            color:#fff;
        }
        .formwrap{
            background-color: #fafafa;
           
            padding: 20px;
            
          box-shadow: 0px 0px 6px rgba(0, 0, 0, 0.3);
			border-radius: 3px;

        }
		#loginbtn1 {
			 margin-top: 5px;
		}
        .loginbox{
            /* width:350px;
            margin-left: 19%;
			margin-top:3%; */
			opacity:0.9
            
        }
        .chkcsslg{
          padding-left: 0px;
        }
        .login-form {
            width: 300px;
            padding: 10px 30px 40px;
            background: #eee;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            border-radius: 4px;
            margin: auto;
            position: absolute;
            left: 0;
            right: 0;
            top: 50%;
            -moz-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%);
        }
        .login-form .logo {
            padding: 20px 10px 40px;
        }
        .center-block {
            display: block;
            margin-right: auto;
            margin-left: auto;
        }
        .form-control {
            width: 100%;
            margin: 0 0 5px 2px;
            height: 50px!important;
            border-radius: 0px!important;
            padding: 5px 7px 5px 15px!important;
            background: #fff;
            border: 2px solid #ddd!important;
        }
        .checkbox-inline {
            font-weight: normal;
            position: relative;
            display: inline-block;
            margin-bottom: 0;
            padding-left: 20px;
            cursor: pointer;
            vertical-align: middle;
        }
        .form-group, .mform .fitem, .form-item {
            margin-bottom: 15px;
        }
        .link {
            text-decoration: none;
            color: #a1a1a1;
            float: right;
            font-size: 12px;
            margin-bottom: 15px;
        }
        #loginbtn1 {
            margin-top: 5px;
            background: #50ac5a!important;
            width: 100%;
            height: 50px;
            border-radius: 4px;
        }
        #loginbtn1:hover{
            color: #fff!important;
            border: none!important;
            background-color: #fa803e!important;
        }
        .login-form .cllogo {
            margin: 30px 0px 0px;
            border-top: 1px solid #d7d7d7;
            margin-left: auto;
            margin-right: auto;
        }
    </style>   
    <body <?php echo $OUTPUT->body_attributes($settingshtml->additionalbodyclasses); ?> >

        <?php echo $OUTPUT->standard_top_of_body_html() ?>

        <?php $logo = $this->page->theme->setting_file_url('logo', 'logo'); ?>

        <div class="title">
            <div class="title-image"></div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center logdivpd">
                        <div class="col-sm-4 chkcsslg">
                            <!--<a  href="<?php echo $CFG->wwwroot; ?>"><img src="<?php echo $CFG->wwwroot.'/theme/ausart/pix/EZ_logo_v2_White.png'; ?>" class= "logo img-responsive" /> </a> -->
                        </div>
                        <div class="col-sm-8">

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!--<div id="page" class="<?php //echo $settingshtml->containerclass; ?>">-->
        <div id="page" class="container">
            

			<div id="page-content-dummy" class="row">
			<br>
			</div>
            <div id="page-content-dummy2" class="row">
                <?php echo "<div style='display: none;'>" . $OUTPUT->main_content() . "</div>"; ?>
                <div class="login-form">
				  <form action="<?php echo $CFG->httpswwwroot; ?>/login/index.php" method="post" id="login1" >
					 <img src="<?php echo $CFG->wwwroot.'/theme/ausart/pix/logoo.png'; ?>" class="img-responsive center-block logo" />
					 <div class="form-group ">
					   <input type="text" class="form-control" name="username" placeholder="Username " id="UserName">
					   <i class="fa fa-user"></i>
					 </div>
					 <div class="form-group log-status">
					   <input type="password" class="form-control" name ="password" placeholder="Password" id="Passwod">
					   <i class="fa fa-lock"></i>
					 </div>
					 <label class="checkbox-inline"> <input type="checkbox"> Remember username</label>
				 
					  <a class="link" href="<?php echo new moodle_url("/login/forgot_password.php"); ?>">Lost your password?</a>
					  <div class="alert alert-error" id="lemsg" style="display:none;margin-bottom:0px !important;padding-top: 0px; padding-bottom: 5px;">
                      &nbsp;
                    </div>
					 <input type="submit" class="log-btn" id="loginbtn1" value="Log in">
					 
					</form>
                    <?php if(($PAGE->url == 'http://lms.compliance.world/login/index.php') || ($PAGE->url == 'http://localhost/redstone/login/index.php')){ ?>
                        <img src="<?php echo $CFG->wwwroot.'/theme/ausart/pix/CLlogo.png'; ?>" class="img-responsive center-block cllogo "/>
                    <?php }elseif($PAGE->url == 'http://lms.ezcertifications.com/login/index.php'){ ?>
                        <img src="<?php echo $CFG->wwwroot.'/theme/ausart/pix/F_Logo.png'; ?>" class="img-responsive center-block cllogo "/>
                    <?php } ?>
				</div>

		</div>

            <script src="<?php echo $CFG->wwwroot . '/theme/ausart/jquery/jquery-min1.js' ?>"></script>
            
            <script>
                $(function () {
                    var e1 = $("#loginerrormessage").text();
                    if (e1.length > 0)
                    {
                        $("#lemsg").html(e1);
                        $("#lemsg").show();
						setTimeout( "$('.alert').fadeOut(1500);",3000 );
                    }
                    $("#loginbtn").click(function () {
                        var uname = $("#login1 input[name=username]").val();
                        $("#login input[name=username]").val(uname);
						
                        var pwd = $("#login1 input[name=password]").val();
                        $("#login input[name=password]").val(pwd);
                        $("#login").submit();
                    });
                });

            </script>



        </div>
         <?php echo $OUTPUT->standard_end_of_body_html() ?>
    </body>
</html>