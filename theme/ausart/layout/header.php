<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

// Get the HTML for the settings bits.
//$html = theme_allyou_get_html_for_settings($OUTPUT, $PAGE);

$left = (!right_to_left());  // To know if to add 'pull-right' and 'desktop-first-column' classes in the layout for LTR.

$hasfootnote = (!empty($PAGE->theme->settings->footnote));
$haslogo = (!empty($PAGE->theme->settings->logo));


//headeralignment
if (empty($PAGE->theme->settings->headeralign)) {
	$headeralign = "0";
} else {
$headeralign = $PAGE->theme->settings->headeralign;
}

if ($headeralign == 1) {
	$headerclass = "lalign";
} else {
	$headerclass = " ";
}

global $DB,$USER;

function Truncate($string, $length, $stopanywhere=false) {
    //truncates a string to a certain char length, stopping on a word if not specified otherwise.
    $string = strip_tags($string);
    if (strlen($string) > $length) {
        //limit hit!
        $string = substr($string,0,($length -3));
        if ($stopanywhere) {
            //stop anywhere
            $string .= '...';
        } else{
            //stop on a word.
            $string = substr($string,0,strrpos($string,' ')).'...';
        }
    }
    return $string;
}

//set hide blocks user pref
theme_ausart_initialise_zoom($PAGE);
$setzoom = theme_ausart_get_zoom();

if ($setzoom == "") {
	$setzoom = "zoomin";
}

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
    <?php echo $OUTPUT->standard_head_html() ?>
    
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Lato:100,300,regular,700,900%7COpen+Sans:300,regular%7CIndie+Flower:regular%7COswald:300,regular,700&amp;subset=latin%2Clatin-ext' type='text/css' media='all' />
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php p($CFG->wwwroot) ?>/theme/ausart/style/vector-icons.css" rel="stylesheet">
    <style>
    @font-face {
	font-family: 'moon';
	src:url('<?php p($CFG->wwwroot) ?>/theme/ausart/font/moon.eot');
	src:url('<?php p($CFG->wwwroot) ?>/theme/ausart/font/moon.eot?#iefix') format('embedded-opentype'),
		url('<?php p($CFG->wwwroot) ?>/theme/ausart/font/moon.svg#moon') format('svg'),
		url('<?php p($CFG->wwwroot) ?>/theme/ausart/font/moon.woff') format('woff'),
		url('<?php p($CFG->wwwroot) ?>/theme/ausart/font/moon.ttf') format('truetype');
	font-weight: normal;
	font-style: normal;
}
	</style>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body <?php echo $OUTPUT->body_attributes("$setzoom page-template-default header_1_body fullwidth_slider_page with_slider_page wpb-js-composer vc_responsive"); ?>>
<?php echo $OUTPUT->standard_top_of_body_html() ?>

<div id="page" class="container-fluid">

 <!-- Start Top Navigation -->
    <div class="top_nav">
        <div class="container">
            <div class="row-fluid">
                <div class="span9">
                    <div class="pull-left">
                        <div id="widget_topnav-2" class="widget widget_topnav">
                            <div class="login small_widget">
                                <div class="widget_activation">
                                
                                <?php echo $OUTPUT->user_menu(); ?>
                                
                                </div>
                               
                            </div>
                        </div>
                        <!--   added by Arjun Singh for header menus    >>starts-->
                        <?php if(isloggedin()){ ?>
                        <div class="widget widget_topinfo">
                            <div class="topinfo">
                                <?php 
                                    echo '<a href="'.$CFG->wwwroot.'/my/" style="text-decoration: none;">Dashboard</a>';
                                ?>
                            </div>
                        </div>
                        <?php
                        $catmenu = '<div class="widget widget_topinfo">
                                        <div class="topinfo">
                                            <ul class="nav-tabs tab-menu" id="nav-tab">
                                                    <li class="nav-item dropdown">
                                                        <a class="tab-a" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                                            Course Management
                                                            <i class="fa fa-caret-down" aria-hidden="true"></i>
                                                        </a>
                                                        <div class="dropdown-menu" id="custom-dropdown">
                                                            <a class="tab-a dropdown-item" href="'.$CFG->wwwroot.'/course/management.php" style="text-decoration: none;margin-left:25px">
                                                                <i class="fa fa-caret-right" aria-hidden="true"></i> Manage Courses
                                                            </a><div class="dropdown-divider"></div>
                                                            <a class="tab-a dropdown-item" href="'.$CFG->wwwroot.'/course/index.php" style="text-decoration: none;margin-left:25px">
                                                                <i class="fa fa-caret-right" aria-hidden="true"></i> Deliver Courses 
                                                            </a><div class="dropdown-divider"></div>
                                                            <a class="tab-a dropdown-item" href="'.$CFG->wwwroot.'/course/coursereportsview.php" style="text-decoration: none;margin-left:25px">
                                                                <i class="fa fa-caret-right" aria-hidden="true"></i> Course Report
                                                            </a>
                                                        </div>
                                                    </li>
                                            </ul>
                                        </div>
                                    </div>';
                        $usermenu = '<div class="widget widget_topinfo">
                                        <div class="topinfo">
                                            <ul class="nav-tabs tab-menu" id="nav-tab">
                                                    <li class="nav-item dropdown">
                                                        <a class="tab-a" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                                            User Management
                                                            <i class="fa fa-caret-down" aria-hidden="true"></i>
                                                        </a>
                                                        <div class="dropdown-menu" id="custom-dropdown2">
                                                            <a class="tab-a dropdown-item" href="'.$CFG->wwwroot.'/course/index.php" style="text-decoration: none;margin-left:25px">
                                                                <i class="fa fa-caret-right" aria-hidden="true"></i> Deliver Courses 
                                                            </a><div class="dropdown-divider"></div>
                                                            <a class="tab-a dropdown-item" href="'.$CFG->wwwroot.'//user/editadvanced.php?id=-1" style="text-decoration: none;margin-left:25px">
                                                                <i class="fa fa-caret-right" aria-hidden="true"></i> Create User
                                                            </a><div class="dropdown-divider"></div>
                                                            <a class="tab-a dropdown-item" href="'.$CFG->wwwroot.'/course/coursereportsview.php" style="text-decoration: none;margin-left:25px">
                                                                <i class="fa fa-caret-right" aria-hidden="true"></i> User Report
                                                            </a>
                                                        </div>
                                                    </li>
                                            </ul>
                                        </div>
                                    </div>';
                        $useradminrole = $DB->get_records_sql("SELECT * FROM {role} r 
                                                            JOIN {role_assignments} ra ON ra.roleid = r.id 
                                                            WHERE r.shortname = 'useradmin' AND ra.userid = {$USER->id}");
                        $catadminrole = $DB->get_records_sql("SELECT * FROM {role} r 
                                                            JOIN {role_assignments} ra ON ra.roleid = r.id 
                                                            WHERE r.shortname = 'catadmin' AND ra.userid = {$USER->id}");
                        
                            if(!(is_siteadmin())){ 
                            if (!empty($useradminrole)){
                                echo $usermenu;
                            } elseif(!empty($catadminrole)){
                                echo $catmenu;
                            }
                            }
                            else{
                                echo $catmenu;
                                echo $usermenu;
                            }
                         
                        ?>
                        <div class="widget widget_topinfo">
                            <div class="topinfo">
                                <?php 
                                    echo '<a href="'.$CFG->wwwroot.'/login/logout.php" style="text-decoration: none;">Logout</a>';
                                ?>
                            </div>
                        </div>
                        <?php } ?>
                        <!--   added by Arjun Singh for header menus    >>ends-->
                        <!--
                        <div id="widget_topinfo-2" class="widget widget_topinfo">
                            <div class="topinfo">
                            <?php if (!empty($PAGE->theme->settings->headcontact)) { ?>
                            <?php echo $PAGE->theme->settings->headcontact; ?>
                            <?php } ?>
                            
                             <?php if (!empty($PAGE->theme->settings->headcontact2)) { ?>
                            <?php echo $PAGE->theme->settings->headcontact2; ?>
                            <?php } ?>
                            </div>
                        </div>
                        -->
                    </div>
                </div>
                
                <div class="span3">
                    <div class="pull-right">
                        <div id="social_widget-2" class="widget social_widget">
                            <div class="row-fluid social_row">
                                <div class="span12">
                                    <ul class="footer_social_icons">
                                        <?php if (!empty($PAGE->theme->settings->socialthree)) { ?>
                                        <li class="google_plus"><a href="<?php echo $PAGE->theme->settings->socialthree; ?>"><i class="moon-google_plus"></i></a></li>
                                        <?php } ?>
                                        <?php if (!empty($PAGE->theme->settings->socialfour)) { ?>
                                        <li class="linkedin"><a href="<?php echo $PAGE->theme->settings->socialfour; ?>"><i class="moon-linkedin"></i></a></li>
                                        <?php } ?>
                                        <?php if (!empty($PAGE->theme->settings->socialtwo)) { ?>
                                        <li class="twitter"><a href="<?php echo $PAGE->theme->settings->socialtwo; ?>"><i class="moon-twitter"></i></a></li>
                                        <?php } ?>
                                        <?php if (!empty($PAGE->theme->settings->socialone)) { ?>
                                        <li class="facebook"><a href="<?php echo $PAGE->theme->settings->socialone; ?>"><i class="moon-facebook"></i></a></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <!-- End of Top Navigation -->

<div class="header_wrapper header_1 no-transparent ">
<header id="header" class="sticky_header">
    <div id="page-header" class="clearfix <?php echo "$headerclass"; ?> lalign">
    	<a href="<?php p($CFG->wwwroot) ?>">
    	<?php if ($haslogo) {
                        //echo "<img src='".$PAGE->theme->settings->logo."' alt='logo' id='logo' />";
                        echo "<img src='".$PAGE->theme->setting_file_url('logo', 'logo')."' alt='logo' id='logo' />";
                    } else { ?>
			<img src="<?php echo $OUTPUT->pix_url('logo', 'theme')?>" id="logo" alt="logo">
			<?php } ?>
		</a>
         
         
<div id="navwrap">
<div class="navbar">
    <nav role="navigation" class="navbar-inner">
        <div class="container-fluid">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <div class="nav-collapse collapse">
            
             <ul class="nav pull-right">
                 <li class="hbl"><a href="#" class="moodlezoom" title="hide blocks"><i class="fa fa-indent"></i></a></li>
                 <li class="sbl"><a href="#" class="moodlezoom" title="show blocks"><i class="fa fa-outdent"></i></a></li>
			 </ul>
           
            	 <div class="pull-left">
                <?php //echo $OUTPUT->custom_menu(); ?>
                </div>
                
            </div>
        </div>
    </nav>
</div>
</div>
         
       
              
         <div id="course-header">
            <?php echo $OUTPUT->course_header(); ?>
        </div>            
        
    </div>
    <div class="header_shadow"><span class=""></span></div>
</header>  
	<div class="header_shadow"><span class=""></span></div>
</div>  
