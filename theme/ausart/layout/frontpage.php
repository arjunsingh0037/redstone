<?php

require_once(dirname(__FILE__) . '/header.php');

if(!empty($PAGE->theme->settings->p1)) { ?>

    <link rel='stylesheet' href='<?php p($CFG->wwwroot) ?>/theme/ausart/plugins/LayerSlider/static/css/layerslider.css' type='text/css' media='all' property="stylesheet" />
    <link rel='stylesheet' href='<?php p($CFG->wwwroot) ?>/theme/ausart/plugins/revslider/rs-plugin/css/settings.css' type='text/css' media='all' property="stylesheet" />

    
    <script type='text/javascript' src='<?php p($CFG->wwwroot) ?>/theme/ausart/plugins/LayerSlider/static/js/layerslider.kreaturamedia.jquery.js'></script>
    <script type='text/javascript' src='<?php p($CFG->wwwroot) ?>/theme/ausart/plugins/LayerSlider/static/js/layerslider.transitions.js'></script>
    <script type='text/javascript' src='<?php p($CFG->wwwroot) ?>/theme/ausart/plugins/revslider/rs-plugin/js/jquery.themepunch.tools.min.js'></script>
    <script type='text/javascript' src='<?php p($CFG->wwwroot) ?>/theme/ausart/plugins/revslider/rs-plugin/js/jquery.themepunch.revolution.min.js'></script>
    
<script>
$( document ).ready(function() {
  $('#rev_slider_1_1').show().revolution({
            dottedOverlay: "none",
            delay: 3000,
            startwidth: 1100,
            startheight: 500,
            hideThumbs: 200,

            thumbWidth: 100,
            thumbHeight: 50,
            thumbAmount: 5,


            simplifyAll: "off",

            navigationType: "bullet",
            navigationArrows: "solo",
            navigationStyle: "round",

            touchenabled: "on",
            onHoverStop: "on",
            nextSlideOnWindowFocus: "off",

            swipe_threshold: 75,
            swipe_min_touches: 1,
            drag_block_vertical: false,



            keyboardNavigation: "off",

            navigationHAlign: "center",
            navigationVAlign: "bottom",
            navigationHOffset: 0,
            navigationVOffset: 20,

            soloArrowLeftHalign: "left",
            soloArrowLeftValign: "center",
            soloArrowLeftHOffset: 20,
            soloArrowLeftVOffset: 0,

            soloArrowRightHalign: "right",
            soloArrowRightValign: "center",
            soloArrowRightHOffset: 20,
            soloArrowRightVOffset: 0,

            shadow: 0,
            fullWidth: "on",
            fullScreen: "off",

            spinner: "spinner0",

            stopLoop: "off",
            stopAfterLoops: -1,
            stopAtSlide: -1,

            shuffle: "off",

            autoHeight: "off",
            forceFullWidth: "off",


            hideTimerBar: "on",
            hideThumbsOnMobile: "off",
            hideNavDelayOnMobile: 1500,
            hideBulletsOnMobile: "off",
            hideArrowsOnMobile: "off",
            hideThumbsUnderResolution: 0,

            hideSliderAtLimit: 0,
            hideCaptionAtLimit: 0,
            hideAllCaptionAtLilmit: 0,
            startWithSlide: 0
        });
        });

</script>
 <div class="top_wrapper   no-transparent">
        <span class="slider-img"></span>   
 <section id="slider-fullwidth" class="slider">
            <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin:0px auto;background-color:#E9E9E9;padding:0px;margin-top:0px;margin-bottom:0px;max-height:500px;">
                <!-- START REVOLUTION SLIDER 4.6.3 fullwidth mode -->
                <div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;max-height:500px;height:500px;">
                    <ul>
                    
<?php if(!empty($PAGE->theme->settings->p1)) { ?>
                        <!-- SLIDE  -->
                        <li data-transition="random" data-slotamount="7" data-masterspeed="300" data-fstransition="fadetoleftfadefromright" data-fsmasterspeed="300" data-fsslotamount="7" data-saveperformance="off">
                            <!-- MAIN IMAGE -->
                            
                            <?php echo "<img src='".$PAGE->theme->setting_file_url('p1', 'p1')."' alt='slideerr' data-bgposition='center center' data-bgfit='cover' data-bgrepeat='no-repeat' />";?>
                            <!-- LAYERS -->
                             <?php if(!empty($PAGE->theme->settings->p1cap)) { ?>
                        	<?php echo $PAGE->theme->settings->p1cap; ?>                     
                            <?php } ?>
 </li>
 <?php } ?>
 
<?php if(!empty($PAGE->theme->settings->p2)) { ?>
                        <!-- SLIDE  -->
                      <li data-transition="fade,boxfade,slotfade-horizontal,slotfade-vertical,fadefromright,fadefromleft,fadefromtop,fadefrombottom,fadetoleftfadefromright,fadetorightfadefromleft,fadetotopfadefrombottom,fadetobottomfadefromtop" data-slotamount="7" data-masterspeed="300" data-saveperformance="off">
                            <!-- MAIN IMAGE -->
                                                        <?php echo "<img src='".$PAGE->theme->setting_file_url('p2', 'p2')."' alt='slideerr' data-bgposition='center center' data-bgfit='cover' data-bgrepeat='no-repeat' />";?>
                            <!-- LAYERS -->
                             <?php if(!empty($PAGE->theme->settings->p2cap)) { ?>
                        	<?php echo $PAGE->theme->settings->p2cap; ?>                     
                            <?php } ?>
 </li>
 <?php } ?>
                        
                        
<?php if(!empty($PAGE->theme->settings->p3)) { ?>
                        <!-- SLIDE  -->
                      <li data-transition="fade,boxfade,slotfade-horizontal,slotfade-vertical,fadefromright,fadefromleft,fadefromtop,fadefrombottom,fadetoleftfadefromright,fadetorightfadefromleft,fadetotopfadefrombottom,fadetobottomfadefromtop" data-slotamount="7" data-masterspeed="300" data-saveperformance="off">
                            <!-- MAIN IMAGE -->
                            
                            <?php echo "<img src='".$PAGE->theme->setting_file_url('p3', 'p3')."' alt='slideerr' data-bgposition='center center' data-bgfit='cover' data-bgrepeat='no-repeat' />";?>
                            <!-- LAYERS -->
                             <?php if(!empty($PAGE->theme->settings->p3cap)) { ?>
                        	<?php echo $PAGE->theme->settings->p3cap; ?>                     
                            <?php } ?>
 </li>
 <?php } ?>
                        
</ul>
                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                </div>
               
               
            </div>
            <!-- END REVOLUTION SLIDER -->
        </section>
</div>


<?php } else { ?>

<?php } ?>


<section id="content" style="" class="composer_content">

<?php if(!empty($PAGE->theme->settings->callout)) { ?>
            <div id="fws_5559b27a6e0af" class="wpb_row animate_onoffset  vc_row-fluid  animate_onoffset row-dynamic-el section-style    " style="padding-top: 80px !important; padding-bottom: 0px !important; margin-bottom: 0px;">
                <div class="container  dark">
                    <div class="section_clear">
                        <div class="vc_col-sm-12 wpb_column column_container" style="" data-animation="" data-delay="0">
                            <div class="wpb_wrapper">
                                <div class="wpb_content_element dynamic_page_header style_3">
                                
                                    
                                     <?php echo $PAGE->theme->settings->callout; ?>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php } ?>
            
            <?php if(!empty($PAGE->theme->settings->coursetab1)) { ?>
            <div id="fws_5559b27a6ecce" class="wpb_row animate_onoffset  vc_row-fluid  animate_onoffset row-dynamic-el section-style    " style="padding-top: 0px !important; padding-bottom: 60px !important; margin-bottom: 30px;">
                <div class="container  dark">
                    <div class="section_clear row-fluid">
                        <div class="vc_col-sm-3 span3 wpb_column column_container" style="" data-animation="" data-delay="0">
                            <div class="wpb_wrapper">
                                <div class=" services_medium wpb_content_element ">
                                    <div class="icon_wrapper">
                                        
                                           <?php echo $PAGE->theme->settings->coursetab1; ?>
                                        
                                </div>
                            </div>
                        </div>
                        
                        <?php if(!empty($PAGE->theme->settings->coursetab2)) { ?>
                        <div class="vc_col-sm-3 span3 wpb_column column_container" style="" data-animation="" data-delay="0">
                            <div class="wpb_wrapper">
                                <div class=" services_medium wpb_content_element ">
                                    <div class="icon_wrapper">
                                        <?php echo $PAGE->theme->settings->coursetab2; ?>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        
                        <?php if(!empty($PAGE->theme->settings->coursetab3)) { ?>
                        <div class="vc_col-sm-3 span3 wpb_column column_container" style="" data-animation="" data-delay="0">
                            <div class="wpb_wrapper">
                                <div class=" services_medium wpb_content_element ">
                                    <div class="icon_wrapper">
                                       <?php echo $PAGE->theme->settings->coursetab3; ?>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        
                        <?php if(!empty($PAGE->theme->settings->coursetab4)) { ?>
                        <div class="vc_col-sm-3 span3 wpb_column column_container" style="" data-animation="" data-delay="0">
                            <div class="wpb_wrapper">
                                <div class=" services_medium wpb_content_element ">
                                    <div class="icon_wrapper">
                                       <?php echo $PAGE->theme->settings->coursetab4; ?>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php } ?>
            
            
             <?php if(!empty($PAGE->theme->settings->statstext)) { ?>
            <link rel='stylesheet' href='<?php p($CFG->wwwroot) ?>/theme/ausart/style/odometer-theme-minimal.css' type='text/css' media='all' property="stylesheet" />
            <script type='text/javascript' src='<?php p($CFG->wwwroot) ?>/theme/ausart/javascript/jquery.appear.js'></script>
            <script type='text/javascript' src='<?php p($CFG->wwwroot) ?>/theme/ausart/javascript/odometer.min.js'></script>
            <script type='text/javascript' src='<?php p($CFG->wwwroot) ?>/theme/ausart/javascript/animations.js'></script>
            <div id="fws_5559b27a70671" class="wpb_row animate_onoffset  vc_row-fluid   row-dynamic-el section-style parallax_section   " style="background-repeat: no-repeat; padding-top: 90px !important; padding-bottom: 90px !important; ">
                <div class="parallax_bg" style="background-image: url(<?php echo $PAGE->theme->setting_file_url('statspic', 'statspic'); ?>); background-position: 50% 0px; background-attachment:fixed !important"></div>
                <div class="container animate_onoffset light">
                    <div class="section_clear row-fluid">
                     
                      <?php echo $PAGE->theme->settings->statstext; ?>
                     
                     </div>
                </div>
            </div>
            <?php } ?>
            
</section>

 

    <div id="page-content" class="row-fluid">
    <div id="padder" class="clearfix">
         <section id="region-main" class="span9 desktop-first-column">
            <?php
            echo $OUTPUT->course_content_header();
            echo $OUTPUT->main_content();
            echo $OUTPUT->course_content_footer();
            ?>
        </section>
        <?php
        $classextra = '';
        if ($left) {
            $classextra = ' 2desktop';
        }
        echo $OUTPUT->blocks('side-pre', 'span3'.$classextra);
        ?>
    </div>
        
    </div>

</div>


<?php if(!empty($PAGE->theme->settings->callout2)) { ?>
<div id="fws_5559b27a83c70" class="wpb_row animate_onoffset  vc_row-fluid  animate_onoffset row-dynamic-el section-style    " style="background-color: #f6f6f6; padding-top: 20px !important; padding-bottom: 20px !important; ">
                <div class="container  dark">
                    <div class="section_clear">
                        <div class="vc_col-sm-12 wpb_column column_container container" style="" data-animation="" data-delay="0">
                            <div class="wpb_wrapper">
                                <div class="textbar-container wpb_content_element  style_1 " style="background:; border:none;">
                                   <?php echo $PAGE->theme->settings->callout2; ?>
                                   </div>
                            </div>
                        </div>
                    </div>
                </div>
</div>
<?php } ?>

<?php if(!empty($PAGE->theme->settings->popcoursetext1)) { ?>
 <div id="fws_5559b27a84678" class="wpb_row animate_onoffset  vc_row-fluid  animate_onoffset row-dynamic-el section-style    " style="padding-top: 120px !important; padding-bottom: 90px !important; margin: 0 0; float: none;">
                <div class="container  dark">
                    <div class="section_clear row-fluid">
                        <div class="vc_col-sm-6 span6 wpb_column column_container" style="" data-animation="" data-delay="0">
                            <div class="wpb_wrapper">
                                <div class="wpb_content_element media media_el animate_onoffset" style='text-align: center;'>
                               
                                <?php echo "<img src='".$PAGE->theme->setting_file_url('popcoursepic1', 'popcoursepic1')."' alt='slideerr' class='type_image media_animation animation_left alignment_left' style='float: none;' />";?>
                                </div>
                            </div>
                        </div>
                       
                       <?php echo $PAGE->theme->settings->popcoursetext1; ?>
                       
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php } ?>            
            

<?php if(!empty($PAGE->theme->settings->coursetab1text)) { ?>            
            <div id="fws_5559b27a9a709" class="wpb_row animate_onoffset  vc_row-fluid  animate_onoffset row-dynamic-el section-style    " style="background-color: #f6f6f6; padding-top: 60px !important; padding-bottom: 30px !important; margin: 0 0; ">
                <div class="container  dark">
                    <div class="section_clear2">
                        <div class="vc_col-sm-12 wpb_column column_container" style="" data-animation="" data-delay="0">
                            <div class="wpb_wrapper">
                                <div class="header " style="">
                                    <?php if(!empty($PAGE->theme->settings->fteach)) { ?>  
                                    <h2><?php echo $PAGE->theme->settings->fteach; ?></h2>
                                    <?php } ?>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="fws_5559b27a9af50" class="wpb_row animate_onoffset  vc_row-fluid  animate_onoffset row-dynamic-el section-style    " style="background-color: #f6f6f6; padding-top: 0px !important; padding-bottom: 90px !important; float: none; margin: 0 0; ">
                <div class="container  dark">
                    <div class="row-fluid">
                        <div class="vc_col-sm-3 span3 wpb_column column_container" style="" data-animation="" data-delay="0">
                            <div class="wpb_wrapper">
                                <div class="wpb_content_element">
                                    <div class="one-staff">
                                        <div class="img_staff">
                                       
                                        <?php echo "<img alt='teacher' src='".$PAGE->theme->setting_file_url('coursetab1image', 'coursetab1image')."'  />";?>
                                        </div>
                                        <?php echo $PAGE->theme->settings->coursetab1text ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <?php if(!empty($PAGE->theme->settings->coursetab2text)) { ?>  
                        <div class="vc_col-sm-3 span3 wpb_column column_container" style="" data-animation="" data-delay="0">
                            <div class="wpb_wrapper">
                                <div class="wpb_content_element">
                                    <div class="one-staff">
                                        <div class="img_staff">
                                        <?php echo "<img alt='teacher' src='".$PAGE->theme->setting_file_url('coursetab2image', 'coursetab2image')."'  />";?>
                                        </div>
                                        
                                        
                                          <?php echo $PAGE->theme->settings->coursetab2text ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        
                        <?php if(!empty($PAGE->theme->settings->coursetab3text)) { ?>  
                        <div class="vc_col-sm-3 span3 wpb_column column_container" style="" data-animation="" data-delay="0">
                            <div class="wpb_wrapper">
                                <div class="wpb_content_element">
                                    <div class="one-staff">
                                        <div class="img_staff">
                                       <?php echo "<img alt='teacher' src='".$PAGE->theme->setting_file_url('coursetab3image', 'coursetab3image')."'  />";?>
                                        </div>
                                          <?php echo $PAGE->theme->settings->coursetab3text ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        
                        <?php if(!empty($PAGE->theme->settings->coursetab4text)) { ?>  
                        <div class="vc_col-sm-3 span3 wpb_column column_container" style="" data-animation="" data-delay="0">
                            <div class="wpb_wrapper">
                                <div class="wpb_content_element">
                                    <div class="one-staff">
                                        <div class="img_staff">
                                        <?php echo "<img alt='teacher' src='".$PAGE->theme->setting_file_url('coursetab4image', 'coursetab4image')."'  />";?>
                                        </div>
                                         <?php echo $PAGE->theme->settings->coursetab4text ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
      <?php } ?>      

<?php require_once(dirname(__FILE__) . '/footer.php'); ?>