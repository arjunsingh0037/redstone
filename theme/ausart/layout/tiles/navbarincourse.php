<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * redstonev1 theme with the underlying Bootstrap theme.
 *
 * @package    theme
 * @subpackage redstonev1
 * @copyright  &copy; 2014-onwards G J Barnard in respect to modifications of the Bootstrap theme.
 * @author     G J Barnard - gjbarnard at gmail dot com and {@link http://moodle.org/user/profile.php?id=442195}
 * @author     Based on code originally written by Bas Brands, David Scotson and many other contributors.
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

global $PAGE, $DB;
$courseid = optional_param('id','', PARAM_INT);
if (!empty($courseid)) {
$coursename = $DB->get_field_sql("SELECT fullname from {course} WHERE id = $courseid");
} else {
$coursename = '';
}

?>
<?php
$data = array();
function category_tree($parent = 0, $optional = true) {
    global $DB;
    $data = array();
    $resultset = $DB->get_records('course_categories', array('parent' => (int) $parent));
    if ($resultset) {
        foreach ($resultset as $id => $row) {
            if ($optional || $id != 1) {
                $data[$id] = array(
                    'id' => $row->id,
                    'name' => $row->name,
                    'coursecount'=> $row->coursecount,
                    'courses' => get_courses($row->id, '', 'c.id,c.fullname'),
                    'flag' => category_tree($row->id)
                );
               
            }
        }
    }
    return $data;
}

$csdata = category_tree();
//print_object($csdata);
global $tabs;
global $content;
global $courselist;

//category_list($csdata);
?>
<script type="text/javascript">
var currentsubcatid = 0;
function show_course(id){

    if (currentsubcatid != 0) {
        document.getElementById('subcategory'+id).style.display = 'block'; 
        var d = document.getElementById('listclass'+id);
        d.classList.add("my-class");
         document.getElementById('subcategory'+currentsubcatid).style.display = 'none'; 
        var e = document.getElementById('listclass'+currentsubcatid);
        e.classList.remove("my-class");

        currentsubcatid = id;
    }else {
        currentsubcatid = id;
        document.getElementById('subcategory'+id).style.display = 'block'; 
        var d = document.getElementById('listclass'+id);
        d.classList.add("my-class");
       
        
       
   }
   
     // var d = document.getElementById('subactive'+id);
     // if(d.className==''){
     //    d.classList.add("my-class");
         
     // }else{
     //     d.classList.remove("my-class");
     // }
}
jQuery(document).ready(function() {

   var setHeight = $('#page-content').outerHeight(); 
 
  $("#block-region-side-pre").height(setHeight);
  $("#block-region-side-post").height(setHeight);

    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
  
    $("#rscse").click(function(){
        $("#redstnmenu").toggle();
    });
    $(".mainh2none").click(function(){
        $(".cscfndiv").hide();
    });
    $(".crossaddcss").click(function(){
        $("#redstnmenu").hide();
    });
  
});
</script>
<script>   
    
var coursesearch = {pageurl:"<?php echo $CFG->wwwroot.'/course/view.php?id=' ?>", coursename:"<?php  echo $coursename ?>"}; 

 function get_data(){
    //document.getElementById('main-panel').innerHTML ='';
    var coursename = document.getElementById('coursename').value; 
    var id = $('#coursesearch option').filter(function() {
        return this.value === coursename;
    }).data('xyz');
    if (id) {
    var searchurl = coursesearch.pageurl +id;

    window.location.assign(searchurl);
  }
}
</script>
