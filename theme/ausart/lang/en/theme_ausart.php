<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'theme_clean', language 'en'
 *
 * @package   theme_clean
 * @copyright 2013 Moodle, moodle.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['choosereadme'] = '
<div class="clearfix">
<div class="well">
<h2>ausart</h2>
<p><img class=img-polaroid src="ausart/pix/screenshot.png" /></p>
</div>
<div class="well">
<h3>About</h3>
<p>ausart is a modified Moodle bootstrap theme. For more information, additional themes and support or customization questions, visit <a href="http://newschoolthemes.com" target="_blank">newschoolthemes.com</a></p>

</div></div>';

$string['configtitle'] = 'ausart';

$string['customcss'] = 'Custom CSS';
$string['customcssdesc'] = 'Whatever CSS rules you add to this textarea will be reflected in every page, making for easier customization of this theme.';



$string['pluginname'] = 'ausart';

$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Left';

$string['fsize'] = 'Global Font Size';
$string['fsizedesc'] = 'Adjust the global font size (in percent, use a number only, default is 85).';

$string['enablefix'] = 'Enable the fixed header?';
$string['enablefixdesc'] = 'Uncheck to stop the header from being fixed.';

$string['enableshow'] = 'Enable the slideshow?';
$string['enableshowdesc'] = 'Uncheck to hide the slideshow.';

$string['enableshowbk'] = 'Enable the slideshow background image?';
$string['enableshowbkdesc'] = 'Check to enable the slideshow global background image';

$string['enableticker'] = 'Enable the news ticker on the homepage?';
$string['enabletickerdesc'] = 'Check to enable the homepage ticker.';

$string['enabletickerc'] = 'Enable the news ticker on internal pages?';
$string['enabletickercdesc'] = 'Check to enable the ticker on internal pages.';

$string['tickertext'] = 'News Ticker Text';
$string['tickertextdesc'] = 'Add news ticker text in list format. See the read me for more info.';

$string['slidep1'] = 'set a new slideshow background image';
$string['slidep1desc'] = 'add a url to an image to use as the slideshow background.';

$string['p1'] = 'Picture1';
$string['p1desc'] = 'Add picture to the frontpage slideshow. Type in a URL to an image.';

$string['p2'] = 'Picture2';
$string['p2desc'] = 'Add picture to the frontpage slideshow. Type in a URL to an image.';

$string['p3'] = 'Picture3';
$string['p3desc'] = 'Add picture to the frontpage slideshow. Type in a URL to an image.';

$string['p4'] = 'Picture4';
$string['p4desc'] = 'Add picture to the frontpage slideshow. Type in a URL to an image.';

$string['p5'] = 'Picture5';
$string['p5desc'] = 'Add picture to the frontpage slideshow. Type in a URL to an image.';

$string['p11'] = 'Slide 1 Icon';
$string['p11desc'] = 'Add the URL to the icon for slide 1';

$string['p22'] = 'Slide 2 Icon';
$string['p22desc'] = 'Add the URL to the icon for slide 2.';

$string['p33'] = 'Slide 3 Icon';
$string['p33desc'] = 'Add the URL to the icon for slide 3.';


$string['p1cap'] = 'Slide 1 caption 1';
$string['p1capdesc'] = 'Add a short heading for slide 1';

$string['p2cap'] = 'Slide 2 caption 1';
$string['p2capdesc'] = 'Add a short heading for slide 2';

$string['p3cap'] = 'Slide 3 caption 1';
$string['p3capdesc'] = 'Add a short heading for slide 3';


$string['p11cap'] = 'Slide 1 caption 2';
$string['p11capdesc'] = 'Add a second short heading for slide 1';

$string['p22cap'] = 'Slide 2 caption 2';
$string['p22capdesc'] = 'Add a second short heading for slide 2';

$string['p33cap'] = 'Slide 3 caption 2';
$string['p33capdesc'] = 'Add a second short heading for slide 3';

$string['servicebk'] = 'Set a background image for the marketing and 2nd callout spot';
$string['servicebkdesc'] = 'Use a background image of this size: 1920px x 1080px.';

$string['market1'] = 'Marketing Block 1';
$string['market1desc'] = 'Add html for marketing block 1 (see the readme file for additional info and hints).';

$string['market2'] = 'Marketing Block 2';
$string['market2desc'] = 'Add html for marketing block 2 (see the readme file for additional info and hints).';

$string['market3'] = 'Marketing Block 3';
$string['market3desc'] = 'Add html for marketing block 3 (see the readme file for additional info and hints).';

$string['market4'] = 'Marketing Block 4';
$string['market4desc'] = 'Add html for marketing block 4 (see the readme file for additional info and hints).';

$string['market5'] = 'Marketing Block 5';
$string['market5desc'] = 'Add html for marketing block 5 (see the readme file for additional info and hints).';

$string['logo'] = 'Logo';
$string['logodesc'] = 'Change the logo of this theme by entering the URL to a new one (i.e., http://www.somesite/animage.png). A transparent .png will work best.';

$string['logotop'] = 'Logo margin padding top';
$string['logotopdesc'] = 'Logo margin padding top (to align logo if needed, default is 0) Use a number like 0 or 10 or 5.';

$string['linkcolor'] = 'Link Color';
$string['linkcolordesc'] = 'Set the color of links in the theme, use html hex code.';

$string['linkhover'] = 'Link Hover Color';
$string['linkhoverdesc'] = 'Set the color of links (on hover) in the theme, use html hex code.';

$string['maincolor'] = 'Main color';
$string['maincolordesc'] = 'Main color for general font color and many other places.';

$string['callout'] = 'Callout for spot above marketing blocks';
$string['calloutdesc'] = 'Callout spot above marketing blocks!';

$string['callout2'] = '2nd callout area';
$string['callout2desc'] = 'Add the second callout block and text to the front page.';

$string['callout2show'] = 'Show the above the footer contact area on all pages or just the front page';

$string['footcolor'] = 'Footer Background Color';
$string['footcolordesc'] = 'Set the footer background color.';


$string['footnote'] = 'Footnote';
$string['footnotedesc'] = 'Add text to the footer.';

$string['socialone'] = 'Facebook';
$string['socialonedesc'] = 'Add URL to facebook for social icon';

$string['socialtwo'] = 'Twitter';
$string['socialtwodesc'] = 'Add URL to twitter account for social icon';

$string['socialthree'] = 'Google';
$string['socialthreedesc'] = 'Add URL to youtube channel for social icon';

$string['socialfour'] = 'Linkedin';
$string['socialfourdesc'] = 'Add URL to Linkedin for social icon';

$string['headeralign'] = 'Header alignment';
$string['headeraligndesc'] = 'Change the header alignment from center to left (logo and menu)';

$string['fpheight'] = 'Frontpage Banner Height';
$string['fpheightdesc'] = 'Set the height of the frontpage banner (default is 200px) Set in pixels with a number';

$string['intheight'] = 'Internal Banner Height';
$string['intheightdesc'] = 'Set the height of the internal banner (default is 200px). Set in pixels with a number';

$string['secondcolor'] = 'Secondary Color';
$string['secondcolordesc'] = 'secondary color for buttons hover states and more.';

$string['noenrolments'] = 'No enrolled courses';

$string['myc'] = 'My Courses';

$string['homelang'] = 'Home';

$string['ticker'] = 'Announcements';

$string['headcontact'] = 'Header Contact Area';
$string['headcontactdesc'] = 'add a phone number and email address in the header contact area.';

$string['headcontact2'] = 'Header Contact Area on the right';
$string['headcontact2desc'] = 'Add contact links in the right header contact area.';


$string['intbk'] = 'Add bacground image for internal page banner';
$string['intbkdesc'] = 'background for internal page banners, 1920px x 210px recomennded.';


$string['frontpagerenderer'] = 'Set which frontpage course listing style you want to use.';

$string['intbkyes'] = 'Use course image for internal banner';
$string['intbkyesdesc'] = 'Use the course image for internal banners where available?';

$string['coursehighlightbox'] = 'Highlight your courses tab box';
$string['coursetab1'] = 'Marketing Block 1';
$string['coursetab2'] = 'Marketing Block 2';
$string['coursetab3'] = 'Marketing Block 3';
$string['coursetab4'] = 'Marketing Block 4';

$string['fteach'] = 'Your Text for the heading of the teachers block.';

$string['coursetab1image'] = 'URL for the staff or user image';
$string['coursetab2image'] = 'URL for the staff or user image';
$string['coursetab3image'] = 'URL for the staff or user image';
$string['coursetab4image'] = 'URL for the staff or user image';

$string['coursetab1text'] = 'Text for the user text';
$string['coursetab2text'] = 'Text for the user text';
$string['coursetab3text'] = 'Text for the user text';
$string['coursetab4text'] = 'Text for the user text';

$string['statspic'] = 'A background picture for the stats section on the Frontpage.';

$string['statstext'] = 'Text for the stats section. See docs for examples and help.';

$string['testpic'] = 'A student or user image for the testimonials section.';
$string['testtitle'] = 'The large heading for the testimonials section. See docs for examples and help.';
$string['testtext'] = 'Text for the testimonials section. See docs for examples and help.';
$string['testname'] = 'Text for the testimonials section user or students name. See docs for examples and help.';

$string['boxed'] = 'Use full width layout or boxed layout.';

$string['preload'] = 'Use the proloader animation.';

$string['popcourse'] = 'Popular Courses';

$string['popcoursetext1'] = 'Site News block text';
$string['popcoursetext2'] = 'popular courses text and link 2';
$string['popcoursetext3'] = 'popular courses text and link 3';

$string['popcoursepic1'] = 'Site news pic';
$string['popcoursepic2'] = 'popular courses pic 2';
$string['popcoursepic3'] = 'popular courses pic 3';

$string['customlogin'] = 'Use the custom login layout with a background image';
$string['customloginpic'] = 'Upload your custom login background pic.';

$string['credit'] = 'Show or Hide the footer credit link.';

$string['intopacity'] = 'Opacity of the internal overlay. Add a number between .1 and 1 (like .5 or .7, etc.).';

$string['showlogin'] = 'Show the frontpage slideshow after user login?';

$string['headingcolor'] = 'Heading color';

$string['showblocks'] = 'Show blocks?';
$string['showblocksdesc'] = 'Force show blocks on all pages where possible?';