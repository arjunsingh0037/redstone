 <?php

require_once('../config.php');
require_once('lib.php');

global $USER,$CFG;
require_once($CFG->dirroot."/calendar/lib.php");
// $eventid = $DB->get_record('event',array('instance'=> 28));
// //print_r($meetingevent);
// $updateevent = array();
// $updateevent['id'] = $eventid->id;
// $updateevent['userid'] = $USER->id;
// $updateevent['name'] = 'meetme07';
// $updateevent['modulename'] = '';
// $updateevent['subscriptionid'] = null;
// $updateevent['uuid']= '';
// $updateevent['format'] = 1;
// $updateevent['timestart'] = time();
// $updateevent['repeats'] = 0;

// $var = $DB->update_record('event', $updateevent);
// print_object($var);
$obj = new block_calendar_upcoming();
$filtercourse    = array();
if (empty($obj->instance)) { // Overrides: use no course at all.
    $courseshown = false;
    $obj->content->footer = '';

} else {
    $courseshown = $obj->page->course->id;
    $obj->content->footer = '<div class="gotocal"><a href="'.$CFG->wwwroot.
                             '/calendar/view.php?view=upcoming&amp;course='.$courseshown.'">'.
                              get_string('gotocalendar', 'calendar').'</a>...</div>';
    $context = context_course::instance($courseshown);
    if (has_any_capability(array('moodle/calendar:manageentries', 'moodle/calendar:manageownentries'), $context)) {
        $obj->content->footer .= '<div class="newevent"><a href="'.$CFG->wwwroot.
                                  '/calendar/event.php?action=new&amp;course='.$courseshown.'">'.
                                   get_string('newevent', 'calendar').'</a>...</div>';
    }
    if ($courseshown == SITEID) {
        // Being displayed at site level. This will cause the filter to fall back to auto-detecting
        // the list of courses it will be grabbing events from.
        $filtercourse = calendar_get_default_courses();
    } else {
        // Forcibly filter events to include only those from the particular course we are in.
        $filtercourse = array($courseshown => $obj->page->course);
    }
}

list($courses, $group, $user) = calendar_set_filters($filtercourse);

$defaultlookahead = CALENDAR_DEFAULT_UPCOMING_LOOKAHEAD;
if (isset($CFG->calendar_lookahead)) {
    $defaultlookahead = intval($CFG->calendar_lookahead);
}
$lookahead = get_user_preferences('calendar_lookahead', $defaultlookahead);

$defaultmaxevents = CALENDAR_DEFAULT_UPCOMING_MAXEVENTS;
if (isset($CFG->calendar_maxevents)) {
    $defaultmaxevents = intval($CFG->calendar_maxevents);
}
$maxevents = get_user_preferences('calendar_maxevents', $defaultmaxevents);
$events = calendar_get_upcoming($courses, $group, $user, $lookahead, $maxevents);   

        if (!empty($obj->instance)) {
            $link = 'view.php?view=day&amp;course='.$courseshown.'&amp;';
            $showcourselink = ($obj->page->course->id == SITEID);
            $obj->content->text = calendar_get_block_upcoming($events, $link, $showcourselink);
        }